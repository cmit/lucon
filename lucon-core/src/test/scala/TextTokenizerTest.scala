import java.io.File
import java.io.FileFilter
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.FileWriter
import java.io.IOException
import java.io.InputStreamReader
import java.io.UnsupportedEncodingException

import org.junit.Assert
import org.junit.Test

import junit.framework.TestCase
import ro.cmit.lucon.analyzers.text.TextTokenizer

class TextTokenizerTest extends TestCase {

  private val inputDirPath = "tokenizer/input/"
  private val outputDirPath = "tokenizer/text/output/"
  private val expectedDirPath = "tokenizer/text/expected/"

  @Test
  @throws(classOf[UnsupportedEncodingException])
  @throws(classOf[FileNotFoundException])
  @throws(classOf[IOException])
  def testTokenizer: Unit = {
    val inputDir = new File(inputDirPath)

    Assert.assertTrue(inputDir != null && inputDir.isDirectory)

    val files = inputDir.listFiles(TextTokenizerTest.FILE_FILTER)

    for (inputFile <- files) {
      val textTokenizer = new TextTokenizer(new InputStreamReader(new FileInputStream(inputFile), "UTF8"))

      val fw = new FileWriter(new File(outputDirPath + inputFile.getName))
      TokenUtils.tokenDetails(textTokenizer, fw)
      fw.close
    }

    FileAssert.assertDirEquals(new File(expectedDirPath), new File(
      outputDirPath), TextTokenizerTest.FILE_FILTER)
  }

}

object TextTokenizerTest {

  val FILE_FILTER = new FileFilter {
    override def accept(pathname: File): Boolean = {
      return pathname.getName.toLowerCase.endsWith(".txt")
    }
  }
}
