import java.io.BufferedReader
import java.io.File
import java.io.FileFilter
import java.io.FileNotFoundException
import java.io.FileReader
import java.io.IOException

import org.junit.Assert
import scala.collection.mutable.TreeSet


object FileAssert extends Assert {
  
	def assertDirectory(file: File) = 
		Assert.assertTrue(file.getPath + " is not a directory.", file != null && file.isDirectory)

	def assertFile( file: File) = 
		Assert.assertTrue(file.getPath + " is not a file.", file != null && file.isFile)

	def assertExists( filePath: String):Unit = assertExists(new File(filePath))

	def assertExists( file: File): Unit = Assert.assertTrue(file + " file doesn't exist.", file.exists)

	@throws(classOf[FileNotFoundException])
	@throws(classOf[IOException])
	def assertFileEquals(expectedFile: File , actualFile:File ) = {
		assertFile(expectedFile)
		assertFile(actualFile)

		val r1 = new BufferedReader(new FileReader(expectedFile))
		val r2 = new BufferedReader(new FileReader(actualFile))

		var line1 = r1.readLine()
		var line2 = r2.readLine()
		while (line1 != null && line2 != null) {
			if (!line1.equals(line2))
				Assert.fail("Different lines!\n" + ">>>>" + line1 + "\n" + "<<<<"
						+ line2 + "\n")

			line1 = r1.readLine()
			line2 = r2.readLine()
		}

		r1.close()
		r2.close()

		if (line1 == null && line2 == null)
			Assert.assertTrue(true)
		else
			Assert.fail("Files with diferent sizes!")
	}

	@throws(classOf[FileNotFoundException])
	@throws(classOf[IOException])
	def assertDirEquals(expectedDir: File , actualDir:File ,fileFilter: FileFilter ) = {
		assertDirectory(expectedDir)
		assertDirectory(actualDir)

		// collect file names only from expected and actual
		val fileNames = new TreeSet[String]
		for (f <- expectedDir.listFiles(fileFilter))
			fileNames.add(f.getName)
		for (f <- actualDir.listFiles(fileFilter))
			fileNames.add(f.getName)

		for (fileName <- fileNames) {
			val expectedFile = new File(expectedDir.getPath() + File.separator
					+ fileName)
			assertExists(expectedFile)

			val actualFile = new File(actualDir.getPath() + File.separator
					+ fileName)
			assertExists(actualFile)

			assertFileEquals(expectedFile, actualFile)
		}

	}

}