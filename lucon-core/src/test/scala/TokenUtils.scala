import java.io.IOException
import java.io.Writer

import org.apache.lucene.analysis.Token
import org.apache.lucene.analysis.TokenStream

object TokenUtils {

  @throws(classOf[IOException])
  def tokenDetails(textTokenizer: TokenStream, writer: Writer) = {
    writer.write("term\ttype\tposIncr\tlength\toffsets\r\n")
    var token: Token = textTokenizer.next
    while (token != null) {
      writer.write(token.termBuffer, 0, token.termLength)
      writer.append('\t')
      writer.append(token.`type`)
      writer.append('\t')
      writer.append("" + token.getPositionIncrement)
      writer.append('\t')
      writer.append("" + token.termLength)
      writer.append('\t')
      writer.append("" + token.startOffset + "-" + token.endOffset)
      writer.append('\r')
      writer.append('\n')
      token = textTokenizer.next
    }
  }

}