import java.io.File
import java.io.FileFilter
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.FileWriter
import java.io.IOException
import java.io.InputStreamReader
import java.io.UnsupportedEncodingException

import org.junit.Assert
import org.junit.Test

import junit.framework.TestCase
import ro.cmit.lucon.analyzers.ConcAnalyzer
import ro.cmit.lucon.analyzers.xml.XmlTokenizer
import ro.cmit.lucon.configuration.IndexConfigurator

class XmlTokenizerTest extends TestCase {

  private val inputDirPath = "tokenizer/input/"

  @Test
  @throws(classOf[UnsupportedEncodingException])
  @throws(classOf[FileNotFoundException])
  @throws(classOf[IOException])
  def testTokenizer: Unit = {

    val outputDirPath = "tokenizer/xml/tokenizer/output/"
    val expectedDirPath = "tokenizer/xml/tokenizer/expected/"

    val inputDir = new File(inputDirPath)

    Assert.assertTrue(inputDir != null && inputDir.isDirectory())

    val files = inputDir.listFiles(XmlTokenizerTest.FILE_FILTER)

    for (inputFile <- files) {
      val textTokenizer = new XmlTokenizer(new InputStreamReader(new FileInputStream(inputFile), "UTF8"))

      val fw = new FileWriter(new File(outputDirPath + inputFile.getName()))
      TokenUtils.tokenDetails(textTokenizer, fw)
      fw.close
    }

    FileAssert.assertDirEquals(new File(expectedDirPath), new File(outputDirPath), XmlTokenizerTest.FILE_FILTER)
  }

  @Test
  @throws(classOf[UnsupportedEncodingException])
  @throws(classOf[FileNotFoundException])
  @throws(classOf[IOException])
  def testFilters = {
    val outputDirPath = "tokenizer/xml/filter/output/"
    val expectedDirPath = "tokenizer/xml/filter/expected/"

    val inputDir = new File(inputDirPath)

    Assert.assertTrue(inputDir != null && inputDir.isDirectory())

    val files = inputDir.listFiles(XmlTokenizerTest.FILE_FILTER)

    for (inputFile <- files) {
      val analyzer = new ConcAnalyzer(new IndexConfigurator)
      analyzer.forFile(inputFile)
      val textTokenizer = analyzer.tokenStream("text",
        new InputStreamReader(new FileInputStream(inputFile),
          "UTF8"))

      val fw = new FileWriter(new File(outputDirPath + inputFile.getName))
      TokenUtils.tokenDetails(textTokenizer, fw)
      fw.close

    }

    FileAssert.assertDirEquals(new File(expectedDirPath), new File(
      outputDirPath), XmlTokenizerTest.FILE_FILTER)
  }

}

object XmlTokenizerTest {
  val FILE_FILTER = new FileFilter {
    override def accept(pathname: File) = pathname.getName.toLowerCase.endsWith(".xml")
  }
}
