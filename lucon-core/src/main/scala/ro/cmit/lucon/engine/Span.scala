package ro.cmit.lucon.engine

class Span(val start: Int, val end: Int, val middle: Span) {

  private val _left = if (middle != null) Span(start, middle.start) else this
  private val _right = if (middle != null) Span(middle.end, end) else this
  def left = _left
  def right = _right

  def isSimple = middle == null
  def isCompound = !isSimple

  def translateWith(delta: Int): Span = {
    if (isSimple)
      Span(start + delta, end + delta)
    else
      Span(start + delta, end + delta, middle.translateWith(delta))
  }
}

object Span {
  def apply(start: Int, end: Int, middle: Span) = new Span(start, end, middle)
  def apply(main: Span, middle: Span) = new Span(main.start, main.end, middle)
  def apply(start: Int, end: Int) = new Span(start, end, null)
}