package ro.cmit.lucon.engine

class Context(val source: Source, val spanPositions: Span, val leftSmallSize: Int, val rightSmallSize: Int) {
  import Context._

  private var middleSpan: Span = null // offsets and simple

  private var smallSpan: Span = null // offsets and compound 
  private var largeSpan: Span = null // offsets and compound

  def this(source: Source, startSpan: Int, endSpan: Int, leftSmallSize: Int, rightSmallSize: Int) = this(source, Span(startSpan, endSpan), leftSmallSize, rightSmallSize)

  override def toString = smallContext

  def smallContext: String = {
    updateSmallContext
    source.getContext(smallSpan).map(SMALL_LAYOUT).getOrElse(EMPTY_STRING)
  }

  def largeContext: String = {
    updateLargeContext
    source.getContext(largeSpan).map(LARGE_LAYOUT).getOrElse(EMPTY_STRING)
  }

  def xpath = source.getXPath(spanPositions.start).getOrElse(EMPTY_STRING)

  private def updateLargeContext: Unit = {
    if (largeSpan == null) {
      updateMiddleSpan

      val newSpan = source.spanOffsetsForPositions(spanPositions, LEFT_LARGE_SIZE, RIGHT_LARGE_SIZE)
      largeSpan = Span(newSpan, middleSpan)
    }
  }

  private def updateSmallContext: Unit = {
    if (smallSpan == null) {
      updateMiddleSpan

      val newSpan = source.spanOffsetsForPositions(spanPositions, leftSmallSize, rightSmallSize)
      smallSpan = Span(newSpan, middleSpan)
    }
  }

  private def updateMiddleSpan: Unit =
    if (middleSpan == null)
      middleSpan = source.spanOffsetsForPositions(spanPositions, 0, 0)

}

object Context {
  private val LEFT_LARGE_SIZE = 200
  private val RIGHT_LARGE_SIZE = 200

  private val LEFT_SMALL_SEP = "<"
  private val RIGHT_SMALL_SEP = ">"

  private val LEFT_LARGE_SEP = "<span style=\"color:red;font-weight:bold;\">"
  private val RIGHT_LARGE_SEP = "</span>"

  private val SMALL_LAYOUT = new SmallLayout(LEFT_SMALL_SEP, RIGHT_SMALL_SEP)
  private val LARGE_LAYOUT = new LargeLayout(LEFT_LARGE_SEP, RIGHT_LARGE_SEP)
  
  private val EMPTY_STRING = ""
}

private trait Layout[R] extends (Tuple3[String, String, String] => R) {
  protected val TAGS = "<[^>]+>".r

  protected val SPACE = " "
}

private class StringLayout(val leftSep: String, val rightSep: String) extends Layout[String] {
  import StringLayout._

  override def apply(context: Tuple3[String, String, String]) =
    process(context._1) + leftSep + process(context._2) + rightSep + process(context._3)

  protected def process(s: String): String = s // dummy

}

private object StringLayout {
  val WHITESPACES = "[\\t ]{2,}".r
  val NEWLINES = "[\\s\\r\\n]{2,}".r
}

private class SmallLayout(leftSep: String, rightSep: String) extends StringLayout(leftSep, rightSep) {
  import StringLayout._

  override protected def process(s: String): String =
    NEWLINES.replaceAllIn(TAGS.replaceAllIn(s, SPACE), SPACE)
}

private class LargeLayout(leftSep: String, rightSep: String) extends StringLayout(leftSep, rightSep) {
  import StringLayout._

  override protected def process(s: String): String =
    WHITESPACES.replaceAllIn(TAGS.replaceAllIn(s, SPACE), SPACE)
}

