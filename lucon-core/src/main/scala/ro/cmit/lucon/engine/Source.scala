package ro.cmit.lucon.engine

import java.io.BufferedReader
import java.io.File
import java.io.FileInputStream
import java.io.InputStreamReader
import java.io.Reader
import java.util.Arrays

import scala.util.Failure
import scala.util.Try

import org.apache.log4j.Logger
import org.apache.lucene.index.IndexReader
import org.apache.lucene.index.TermPositionVector
import org.apache.lucene.index.TermVectorOffsetInfo

class Source(indexReader: IndexReader, val docId: Int, val filePath: String) {

  private var stpv: TermPositionVector = null

  private var fis: FileInputStream = null
  private var isr: InputStreamReader = null
  private var reader: Reader = null

  private val logger: Logger = Logger.getLogger(this.getClass)

  def getContext(context: Span): Try[Tuple3[String, String, String]] = {
    Try {
      if (reader == null) {
        val file = new File(filePath)
        val fileSize = file.length()
        fis = new FileInputStream(file)
        isr = new InputStreamReader(fis, "UTF-8")
        reader = new BufferedReader(isr)
        reader.mark(fileSize.toInt)
      }
      reader.reset()
      reader.skip(context.start)
      val cbuf = new Array[Char](context.end - context.start)
      reader.read(cbuf)

      val transContext = context.translateWith(-context.start)
      (
        new String(slice(cbuf, transContext.left)),
        new String(slice(cbuf, transContext.middle)),
        new String(slice(cbuf, transContext.right)))
    }
  }

  private def slice(buffer: Array[Char], span: Span) = buffer.slice(span.start, span.end)

  private[this] def getStpv: Try[TermPositionVector] = {
    Try {
      if (stpv == null) {
        val tfv = indexReader.getTermFreqVector(docId, "text")
        if (tfv.isInstanceOf[TermPositionVector])
          stpv = tfv.asInstanceOf[TermPositionVector]
        else
          return Failure(new Exception("Not able to take TermPositionVector."))
      }
      stpv
    }
  }

  override def finalize = {
    super.finalize()

    if (stpv != null)
      stpv = null
    if (fis != null) {
      fis.close()
      fis = null
    }
    if (isr != null) {
      isr.close()
      isr = null
    }
  }

  /* (non-Javadoc)
     * @see ro.cmit.lucon.engine.Source#getStartOffsetForPosition(int)
     */
  def getStartOffsetForPosition(start: Int): Int = {
    val position = if (start < 0) 0 else start

    val s = getStpv
    if (s.isFailure)
      return Integer.MAX_VALUE

    val stpv = s.get
    var minOffset = Integer.MAX_VALUE
    var minDistance = Integer.MAX_VALUE
    for (i <- 0 until stpv.size()) {
      val positions = stpv.getTermPositions(i)
      val nr_positions = positions.length
      val offsets = stpv.getOffsets(i)
      val j = Arrays.binarySearch(positions, position)
      if (j > -1) {
        val new_distance = position - positions(j) // it should be 0
        if (minDistance == new_distance)
          minOffset = Math.min(minOffset, offsets(j).getStartOffset)
        else if (minDistance > new_distance) {
          minDistance = new_distance
          minOffset = offsets(j).getStartOffset
        }
      } else {
        val k = -j - 1 // insertion point
        if (k == 0) {
          if (minDistance != 0) {
            val new_distance = positions(k) - position
            if (minDistance == new_distance)
              minOffset = Math.min(minOffset, offsets(k).getStartOffset)
            else if (minDistance > new_distance) {
              minDistance = new_distance
              minOffset = offsets(k).getStartOffset
            }
          }
        } else if (k > nr_positions - 1) {
        } else {
          if (minDistance != 0) {
            val new_distance = positions(k) - position
            if (minDistance == new_distance)
              minOffset = Math.min(minOffset, offsets(k).getStartOffset)
            else if (minDistance > new_distance) {
              minDistance = new_distance
              minOffset = offsets(k).getStartOffset
            }
          }
        }
      }
    }

    return minOffset
  }

  def getEndOffsetForPosition(end: Int): Int = {
    val position = if (end < 0) 0 else end

    val s = getStpv
    if (s.isFailure)
      return Integer.MAX_VALUE

    val stpv = s.get
    var maxOffset = 0
    var minDistance = Integer.MAX_VALUE
    for (i <- 0 until stpv.size) {
      val positions = stpv.getTermPositions(i)
      val offsets = stpv.getOffsets(i)
      val j = Arrays.binarySearch(positions, position)
      if (j > -1) { // found
        val new_distance = position - positions(j) // it should be 0
        if (minDistance == new_distance)
          maxOffset = Math.max(maxOffset, offsets(j).getEndOffset)
        else if (minDistance > new_distance) {
          minDistance = new_distance
          maxOffset = offsets(j).getEndOffset
        }
      } else { // not found, but j is the insertion point
        val nr_positions = positions.length
        val k = -j - 1 // insertion point
        if (k == 0) {
        } else if (k > nr_positions - 1) {
          val k1 = nr_positions - 1
          val new_distance = position - positions(k1)
          if (minDistance == new_distance)
            maxOffset = Math.max(maxOffset, offsets(k1).getEndOffset)
          else if (minDistance > new_distance) {
            minDistance = new_distance
            maxOffset = offsets(k1).getEndOffset
          }
        } else {
          if (minDistance != 0) {
            val k1 = k - 1
            val new_distance = position - positions(k1)
            if (minDistance == new_distance)
              maxOffset = Math.max(maxOffset, offsets(k1).getEndOffset)
            else if (minDistance > new_distance) {
              minDistance = new_distance
              maxOffset = offsets(k1).getEndOffset
            }
          }
        }
      }
    }

    return maxOffset
  }

  def spanOffsetsForPositions(positions: Span, leftSize: Int, rightSize: Int): Span = {
    val startOff = getStartOffsetForPosition(positions.start - leftSize)
    val endOff = getEndOffsetForPosition(positions.end - 1 + rightSize)
    Span(startOff, endOff)
  }

  def getXPath(position: Int): Try[String] = {
    import Source._
    getStpv.map {
      stpv =>
        {
          val s = for {
            (term, i) <- stpv.getTerms().zipWithIndex if (term.startsWith(ROOT) && stpv.getTermPositions(i).contains(position))
          } yield term
          if (s.length > 0) s(0) else EMPTY_STRING
        }
    }
  }

}

object Source {
  private val ROOT = "/"
  private val EMPTY_STRING = ""
}