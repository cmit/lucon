package ro.cmit.lucon.engine

import java.util.ArrayList
import java.util.HashMap
import java.util.Map

import scala.collection.JavaConversions._

import org.apache.log4j.Logger
import org.apache.lucene.index.IndexReader
import org.apache.lucene.index.Term
import org.apache.lucene.search.regex.SpanRegexQuery
import org.apache.lucene.search.spans.SpanNearQuery
import org.apache.lucene.search.spans.SpanQuery

class TermConcordance(val contexts: java.util.List[Context]) {

  private val logger = Logger.getLogger(this.getClass)

  def getConcordanceContexts(startIndex: Int, endIndex: Int): java.util.List[Context] = {
    val contexts = new ArrayList[Context]
    for (i <- startIndex until Math.min(endIndex, this.contexts.size())) {
      contexts.add(this.contexts.get(i));
    }
    return contexts;
  }

}

object TermConcordance {
  def apply(indexReader: IndexReader, queries: List[SpanQuery], leftContextSize: Int, rightContextSize: Int): TermConcordance = {
    val concordanceDocuments = new HashMap[Int, Source]
    val concordanceContexts = new ArrayList[Context]
    for (query <- queries) {
      query.rewrite(indexReader);
      val spans = query.getSpans(indexReader);
      while (spans.next()) {
        val docId = spans.doc();
        val doc = indexReader.document(docId);
        val concordanceDocument = getInstanceOf(
          concordanceDocuments, indexReader, docId, doc.get("path"));

        val context = new Context(
          concordanceDocument, spans.start(), spans.end(),
          leftContextSize, rightContextSize);
        concordanceContexts.add(context);
      }
    }
    new TermConcordance(concordanceContexts)
  }

  private def apply(indexReader: IndexReader, query: SpanQuery, leftContextSize: Int, rightContextSize: Int): TermConcordance = {
    val concordanceDocuments = new HashMap[Int, Source]
    val concordanceContexts = new ArrayList[Context]
    query.rewrite(indexReader);
    val spans = query.getSpans(indexReader);
    while (spans.next()) {
      val docId = spans.doc();
      val doc = indexReader.document(docId);
      val concordanceDocument = getInstanceOf(
        concordanceDocuments, indexReader, docId, doc.get("path"));

      val context = new Context(
        concordanceDocument, spans.start(), spans.end(),
        leftContextSize, rightContextSize);
      concordanceContexts.add(context);
    }
    new TermConcordance(concordanceContexts)
  }

  def apply(indexReader: IndexReader, regexTerms: List[String], distance: Int, leftContextSize: Int, rightContextSize: Int): TermConcordance = {
    val queries = regexTerms.map { t => new SpanRegexQuery(new Term(Text.name, t)) }
    TermConcordance(indexReader, new SpanNearQuery(queries.toArray, distance - queries.length, true), leftContextSize, rightContextSize)
  }

  private def getInstanceOf(concordanceDocuments: Map[Int, Source], indexReader: IndexReader, docId: Int, path: String): Source = {
    if (!concordanceDocuments.containsKey(Integer.valueOf(docId)))
      concordanceDocuments.put(Integer.valueOf(docId),
        new Source(indexReader, docId, path));
    concordanceDocuments.get(docId);
  }

}