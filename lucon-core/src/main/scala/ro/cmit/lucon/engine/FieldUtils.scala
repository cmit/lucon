package ro.cmit.lucon.engine

object FieldUtils {
  val PREFIX_ATTRIBUTE = "@";

  val PREFIX_SPECIAL = "!";

  val PREFIX_XPATH = "/";

}

sealed trait Field { def name: String }
case object Text extends Field { val name = "text" }
