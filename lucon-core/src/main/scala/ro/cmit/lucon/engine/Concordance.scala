package ro.cmit.lucon.engine

import java.util.regex.Pattern
import scala.annotation.tailrec
import scala.collection.JavaConversions._
import scala.collection.mutable.ListBuffer
import org.apache.lucene.index.IndexReader
import org.apache.lucene.index.Term
import org.apache.lucene.index.TermDocs
import org.apache.lucene.search.FuzzyTermEnum
import org.apache.lucene.search.spans.SpanTermQuery

class Concordance(val indexReader: IndexReader) {
  def terms: List[String] = termsBy(t => isTextTerm(t))

  def termsBy(pred: Term => Boolean): List[String] = mutableTerms(pred).toList

  def termsAsJava: java.util.List[String] = termsAsJavaBy(t => isTextTerm(t))

  def matchingTerms(regex: String): java.util.List[String] = {
    val pattern = Pattern.compile(regex, Pattern.UNICODE_CASE)
    mutableTerms(t => isTextTerm(t) && pattern.matcher(t.text()).find())
  }

  def matchingTermsWithFreq(regex: String): java.util.List[Tuple2[String, Int]] = {
    val pattern = Pattern.compile(regex, Pattern.UNICODE_CASE)
    mutableTermsWithFreq(t => isTextTerm(t) && pattern.matcher(t.text()).find())
  }

  def termsAsJavaBy(pred: Term => Boolean): java.util.List[String] = mutableTerms(pred)

  private def mutableTermsWithFreq(pred: Term => Boolean) = {
    val terms = ListBuffer[Tuple2[String, Int]]()
    val termEnum = indexReader.terms()
    while (termEnum.next) {
      val term = termEnum.term()
      if (pred(term))
        terms += Tuple2(term.text, freq(term))
    }
    termEnum.close
    terms
  }

  def freq(term: String): Int = freq(new Term(Text.name, term))

  private def freq(term: Term) = sumTermDocsFreq(indexReader.termDocs(term), 0)

  @tailrec
  private def sumTermDocsFreq(termDocs: TermDocs, accum: Int): Int = {
    if (termDocs.next()) sumTermDocsFreq(termDocs, accum + termDocs.freq)
    else accum
  }

  private def mutableTerms(pred: Term => Boolean) = {
    val terms = ListBuffer[String]()
    val termEnum = indexReader.terms()
    while (termEnum.next) {
      val term = termEnum.term()
      if (pred(term))
        terms += term.text
    }
    termEnum.close
    terms
  }

  def similarTerms(term: String): java.util.List[String] = {
    val terms = ListBuffer[String]()
    val termEnum = new FuzzyTermEnum(indexReader, new Term(Text.name, term))
    while (termEnum.next)
      terms += termEnum.term().text()
    termEnum.close()
    terms
  }

  private def isTextTerm(term: Term) = Text.name.equals(term.field) && !term.text.startsWith(FieldUtils.PREFIX_ATTRIBUTE) && !term.text.startsWith(FieldUtils.PREFIX_XPATH)

  def contexts(terms: List[String], left: Int, right: Int): TermConcordance = {
    val queries = terms.map (t => new SpanTermQuery(new Term(Text.name, t)))
    TermConcordance(indexReader, queries, left, right)
  }

  def contextsWithDistance(terms: List[String], distance: Int, left: Int, right: Int): TermConcordance = {
    TermConcordance.apply(indexReader, terms, distance, left, right)
  }
}
