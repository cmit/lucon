package ro.cmit.lucon.analyzers

import org.apache.lucene.analysis.TokenStream
import ro.cmit.lucon.analyzers.text.TextTokenizer
import ro.cmit.lucon.analyzers.xml.XmlTokenizer
import org.apache.lucene.analysis.LowerCaseFilter
import ro.cmit.lucon.configuration.IndexConfigurator
import ro.cmit.lucon.analyzers.xml.XmlMetaInfoFilter
import ro.cmit.lucon.analyzers.xml.AttrCollapseFilter
import java.io.Reader
import ro.cmit.lucon.analyzers.xml.XmlConcFilter

trait ConcTokenStream {

  def tokenStream: TokenStream

  def inputProgress: InputProgress
}

final class TextTokenStream(reader: Reader) extends ConcTokenStream {

  private val textTokenizer = new TextTokenizer(reader)
  private val lowerCaseFilter = new LowerCaseFilter(textTokenizer)

  def tokenStream = lowerCaseFilter
  def inputProgress = textTokenizer
}

final class XmlTokenStream(reader: Reader, configurator: IndexConfigurator) extends ConcTokenStream {
  private val xmlTokenizer = new XmlTokenizer(reader)
  private val xmlMetaInfoFilter = new XmlMetaInfoFilter(xmlTokenizer, configurator)
  private val xmlConcFilter = new XmlConcFilter(xmlMetaInfoFilter, configurator)
  private val attrTokenStream = new AttrCollapseFilter(xmlConcFilter)
  private val lowerCaseFilter = new LowerCaseFilter(attrTokenStream)

  def tokenStream = lowerCaseFilter

  def inputProgress = xmlTokenizer

}
