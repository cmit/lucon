package ro.cmit.lucon.analyzers

import java.io.File
import java.io.Reader

import org.apache.log4j.Logger
import org.apache.lucene.analysis.Tokenizer

import ro.cmit.libs.tsc.TypeSafeProperties
import ro.cmit.lucon.configuration.ConcKey

abstract class ConcTokenizer(r: Reader) extends Tokenizer(r) with InputProgress {

  private var inputOffset = 0

  protected val properties = new TypeSafeProperties(ConcKey.WORD_PATTERN, ConcKey.IGNORED_ATTRIBUTES, ConcKey.BUFFER_SIZE)
  properties.load(new File(System.getProperty("user.dir"), ConcTokenizer.propFileName))

  private val dim = if (properties.get(ConcKey.BUFFER_SIZE) != null) properties.get(ConcKey.BUFFER_SIZE).intValue else 8024

  private var finished = false
  protected val logger = Logger.getLogger(this.getClass)

  def getReaderPosition = synchronized { inputOffset }

  protected def getNextString: String = {
    val buffer = new Array[Char](dim)
    val n = input.read(buffer)
    if (n == -1) {
      setFinished(true)
      return null
    }
    val nextStr = new String(buffer, 0, n)
    inputOffset += nextStr.getBytes.length

    return nextStr
  }

  protected def isFinished = synchronized { finished }

  private def setFinished(finished: Boolean) = synchronized { this.finished = finished }

}

object ConcTokenizer {
  val propFileName = "lucon.properties";
}
