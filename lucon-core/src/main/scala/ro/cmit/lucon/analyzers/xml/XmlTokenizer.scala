package ro.cmit.lucon.analyzers.xml

import java.io.Reader
import ro.cmit.lucon.analyzers.ConcTokenizer
import java.io.IOException
import org.apache.lucene.analysis.Token
import ro.cmit.lucon.analyzers.TokenType
import ro.cmit.lucon.engine.Text

class XmlTokenizer(r: Reader) extends ConcTokenizer(r) {
  

    val buffer = new StringBuffer

    var offset = 0

    /*
     * Intoarce rand pe rand cate un token din xml. Nu se pune problema de
     * validitate sau well-formness decat ca inceput sau sfarsit de tag.
     * 
     * @see org.apache.lucene.analysis.TokenStream#next()
     */
    override def next(): Token = {
            if (buffer.length() == 0) {
                val nextStr = getNextString
                if (nextStr == null) {
                    offset = 0
                    return null
                }
                buffer.append(nextStr)
            }
            
            // TODO: cautarea elementului e simpla. Se presupune ca nu exista
            // comentarii sau PI=uri mai complicate: <!-- <element ...> -->
            if (buffer.charAt(0) == '<') {
                var end = buffer.indexOf(XmlTokenizer.END_TAG)
                while (end == -1) {
                    val nextStr = getNextString
                    if (nextStr == null)
                        throw new IOException(
                                "XML-ul nu e bine-format: asteptam un '>' dupa: "
                                        + buffer.substring(0, 20))
                    buffer.append(nextStr)
                    end = buffer.indexOf(XmlTokenizer.END_TAG)
                }
                val tokenString = stringForElement(end)
                val tokenTypeType = typeForElement(tokenString)
                offset += tokenString.length
                
                return new Token(tokenString, offset - tokenString.length, offset, tokenTypeType.`type`)
            } else { // inceput de text
                var end = buffer.indexOf(XmlTokenizer.START_TAG)
                while (end == -1) {
                    val nextStr = getNextString
                    if (nextStr == null) {
                        // se poate ca buffer sa fie sfarsit de xml (numai
                        // spatii)
                        if (XmlTokenizer.WHITESPACE_REGEXP.findFirstIn(buffer).isDefined) {
                            offset = 0
                            return null
                        } else
                            throw new IOException(
                                    "XML-ul nu e bine-format: asteptam un '<' dupa: "
                                            + buffer.substring(0))
                    }
                    buffer.append(nextStr)
                    end = buffer.indexOf(XmlTokenizer.START_TAG);
                }
                // sigur la end se afla '<'
                val tokenString = stringFromBuffer(end)
                offset += tokenString.length

                return new Token(tokenString, offset - tokenString.length(), offset, TokenType.Text.`type`)
            }

        return null
    }

    private def stringForElement(end: Int): String = {
        if (end >= buffer.length()) {
            val tokenString = buffer.substring(0)
            buffer.setLength(0)
            return tokenString
        }
        
        // sigur la end se afla '>'
        return stringFromBuffer(end + 1)
    }

    private def stringFromBuffer(end: Int): String = {
        val tokenString = buffer.substring(0, end)
        buffer.delete(0, end)
        return tokenString
    }

    private def typeForElement(tokenString: String): TokenType.MyVal = {
        if (tokenString.startsWith(XmlTokenizer.START_COMMENT))
            return TokenType.Comment
        else if (tokenString.startsWith(XmlTokenizer.START_PI))
            return TokenType.ProcessingInstruction
        else if (tokenString.startsWith(XmlTokenizer.START_END_TAG))
            return TokenType.EndElement
        else if (tokenString.endsWith(XmlTokenizer.END_EMPTY_TAG1)
                || tokenString.endsWith(XmlTokenizer.END_EMPTY_TAG2))
            return TokenType.EmptyElement
        TokenType.StartElement
    }
}

object XmlTokenizer {
  
    val START_TAG = "<"
    val END_TAG = ">"
    val START_END_TAG = "</"
    val END_EMPTY_TAG1 = "/>"
	  val END_EMPTY_TAG2 = "/ >"
    val START_COMMENT = "<!--"
    val START_PI = "<?"

    val WHITESPACE_REGEXP = "^[\\s\\r\\n]+$".r;
}
