package ro.cmit.lucon.analyzers

trait InputProgress {
  def getReaderPosition: Long
}