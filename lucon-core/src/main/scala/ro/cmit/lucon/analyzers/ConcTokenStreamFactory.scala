package ro.cmit.lucon.analyzers

import java.io.Reader

import ro.cmit.lucon.configuration.IndexConfigurator

trait ConcTokenStreamFactory {
  def createTokenStream(reader: Reader, configurator: IndexConfigurator): ConcTokenStream
}

object TextTokenStreamFactory extends ConcTokenStreamFactory {
  def createTokenStream(reader: Reader, configurator: IndexConfigurator) = new TextTokenStream(reader)
}

object XmlTokenStreamFactory extends ConcTokenStreamFactory {
  def createTokenStream(reader: Reader, configurator: IndexConfigurator) = new XmlTokenStream(reader, configurator)
}
