package ro.cmit.lucon.analyzers.text

import java.io.Reader
import java.util.regex.Matcher
import java.util.regex.Pattern

import org.apache.lucene.analysis.Token

import ro.cmit.lucon.analyzers.ConcTokenizer
import ro.cmit.lucon.analyzers.TokenType
import ro.cmit.lucon.configuration.ConcKey

class TextTokenizer(r: Reader) extends ConcTokenizer(r) {

  val buffer = new StringBuffer()
  var offset = 0

  private[this] val wordPattern = properties.get(ConcKey.WORD_PATTERN)
  private[this] val pattern =
    if (wordPattern != null)
      Pattern.compile(wordPattern, Pattern.UNICODE_CASE)
    else
      Pattern.compile("[\\p{L}]+", Pattern.UNICODE_CASE)

  /*
     * Intoarce rand pe rand cate un token din text.
     * 
     * @see org.apache.lucene.analysis.TokenStream#next()
     */
  override def next: Token = {
    var tokenString: String = null
    var wordMatcher: Matcher = null

    while (true) {
      wordMatcher = pattern.matcher(buffer)
      val matches = wordMatcher.find()
      if (matches
        && (wordMatcher.end() != buffer.length || isFinished)) {
        val start = wordMatcher.start
        val end = wordMatcher.end
        if (end < buffer.length) {
          tokenString = buffer.substring(start, end)
          buffer.delete(0, end)
        } else {
          tokenString = buffer.substring(start)
          buffer.setLength(0)
        }

        offset = offset + end
        return new Token(tokenString, offset - end + start, offset, TokenType.Text.`type`)
      }
      // citim mai departe din fisier
      val nextStr = getNextString
      if (nextStr == null) {
        if (!matches) {
          offset = 0
          return null
        }
      } else
        buffer.append(nextStr)
      wordMatcher = pattern.matcher(buffer)
    }

    return null
  }
}

object TextTokenizer {
  val WordPatternProp = "word.pattern"

}