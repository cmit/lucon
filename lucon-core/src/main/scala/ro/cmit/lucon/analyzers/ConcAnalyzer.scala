package ro.cmit.lucon.analyzers

import org.apache.lucene.analysis.Analyzer
import java.io.Reader
import org.apache.lucene.analysis.TokenStream
import ro.cmit.lucon.configuration.IndexConfigurator
import java.io.File

class ConcAnalyzer(configurator: IndexConfigurator) extends Analyzer with InputProgress {
  
    var tokenStream: ConcTokenStream = null

    var tokenStreamFactory: ConcTokenStreamFactory = TextTokenStreamFactory

    override def tokenStream(fieldName: String , reader: Reader ): TokenStream = {
        tokenStream = tokenStreamFactory.createTokenStream(reader, configurator)
        tokenStream.tokenStream
    }

    def forFile(file: File ): Unit = {
        if (file.getName.toLowerCase.endsWith("xml"))
            tokenStreamFactory = XmlTokenStreamFactory
        else
            tokenStreamFactory = TextTokenStreamFactory
    }

    override def getReaderPosition: Long = tokenStream.inputProgress.getReaderPosition

}