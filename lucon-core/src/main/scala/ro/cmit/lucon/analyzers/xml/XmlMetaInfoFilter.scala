package ro.cmit.lucon.analyzers.xml

import org.apache.lucene.analysis.TokenStream
import org.apache.lucene.analysis.Token
import ro.cmit.lucon.configuration.IndexConfigurator
import org.apache.lucene.analysis.TokenFilter
import ro.cmit.lucon.analyzers.TokenType
import scala.util.matching.Regex
import XmlMetaInfoFilter._

class XmlMetaInfoFilter(in: TokenStream, configurator: IndexConfigurator) extends TokenFilter(in) {

  override def next(): Token = {
    var nextToken = in.next()
    while (isStartMetaInfoTag(nextToken)) {
      {
        val elementName = firstGroup(startElementNameRegex, nextToken.termText)
        nextToken = input.next
        if (elementName.isDefined) {
          while (!isEndMetaInfoTag(nextToken, elementName.get)) {
            if (TokenType.Text.isTokenType(nextToken))
              configurator.addMetaInfoField(elementName.get, nextToken.termText)
            nextToken = input.next
          }
          nextToken = input.next
        }
      }
    }
    return nextToken;
  }

  private def isStartMetaInfoTag(token: Token): Boolean = {
    token != null &&
      TokenType.StartElement.isTokenType(token) &&
      firstGroup(startElementNameRegex, token.termText).filter { configurator.getXmlMetaElements().contains(_) }.isDefined
  }

  private def isEndMetaInfoTag(token: Token, startElement: String) = {
    token != null &&
      TokenType.EndElement.isTokenType(token) &&
      firstGroup(endElementNameRegex, token.termText).contains(startElement)
  }

  private def firstGroup(regex: Regex, text: String) = regex.findFirstMatchIn(text).map { (_.group(1)) }

}

object XmlMetaInfoFilter {
  val startElementNameRegex = "^<([^<>\\s\\/]+)\\b".r
  val endElementNameRegex = "^<\\/\\s*([^<>\\s\\/]+)\\b".r
}
