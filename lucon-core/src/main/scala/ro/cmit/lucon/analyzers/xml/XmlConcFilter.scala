package ro.cmit.lucon.analyzers.xml

import org.apache.lucene.analysis.TokenFilter
import org.apache.lucene.analysis.TokenStream
import ro.cmit.lucon.configuration.IndexConfigurator
import java.util.regex.Pattern
import ro.cmit.lucon.configuration.ConcKey
import ro.cmit.lucon.analyzers.TokenType
import java.io.IOException
import org.apache.lucene.analysis.Token
import ro.cmit.lucon.configuration.ConcProperties
import java.io.File
import java.util.ArrayList
import org.apache.log4j.Logger
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import java.util.Collections
import ro.cmit.lucon.engine.FieldUtils

class XmlConcFilter(input: TokenStream, configurator: IndexConfigurator) extends TokenFilter(input) {

    private val properties = new ConcProperties
    val crtDir = System.getProperty("user.dir")
    val propFile = new File(crtDir, XmlConcFilter.propFileName)
    properties.load(propFile);
    
    private val wordPattern = Pattern.compile(properties.get(ConcKey.WORD_PATTERN), Pattern.UNICODE_CASE)
    private val tokenQueue: java.util.List[Token] = new ArrayList[Token]
    private var openedElements: List[String] = List()
    private var crtXPath: String = ""
    
    // folosit la crearea tokenelor din atribute
    private val justOpenedElements: java.util.List[String] =  new ArrayList[String]
    
    private val ignoreAttributes: java.util.List[String] = new ArrayList[String](properties.get(ConcKey.IGNORED_ATTRIBUTES))
    private var prevTextToken: Token = null
    private var prevAttrTokens: java.util.List[Token] = null
    private val logger = Logger.getLogger(this.getClass)

    override def  next(): Token = {

        while (tokenQueue.isEmpty) {
            val t = input.next()
            if (t == null)
                return null
            if (TokenType.Text.isTokenType(t)) {
                val textBuffer = t.termText()
                var start = 0
                var end = 0
                var offset = t.startOffset()
                val wordMatcher = wordPattern.matcher(textBuffer)
                while (wordMatcher.find(start)) {
                    start = wordMatcher.start()
                    end = wordMatcher.end()
                    var tokenString = ""
                    if (end < textBuffer.length())
                        tokenString = textBuffer.substring(start, end)
                    else 
                        tokenString = textBuffer.substring(start)

                    val newToken = new Token(tokenString, offset + start, offset + end)
                    tokenQueue.add(newToken)
                    prevTextToken = newToken
                    if (prevAttrTokens == null)
                        prevAttrTokens = new ArrayList[Token]
                    else
                        while (prevAttrTokens.size() > 0)
                            prevAttrTokens.remove(0)

                    // TODO: teste de performanta cu sau fara secventa de mai
                    // jos
                    /*
                     * Se genereaza si un token de localizare reprezentand
                     * Xpath-ul rezultat din openedElements
                     */
                    val pathToken = new Token(crtXPath, offset + start, offset + end, TokenType.XPath.`type`)
                    pathToken.setPositionIncrement(0)
                    tokenQueue.add(pathToken)

                    /*
                     * in cazul in care exista elemente in array-ul
                     * justOpenedElements atunci se creeaza token-uri pe primul
                     * token rezultat din "acest sir de
                     * token-uri". Noile token-uri vor avea text ce incepe cu "
                     * 
                     * @" (FieldUtils.PREFIX_ATTRIBUTE) si se termina in
                     * "START", respectiv "END" si, daca elementul cu atribute e
                     * pe acelasi token, "FULL";
                     */
                    if (justOpenedElements.size() > 0) {
                        // genereaza token-uri pentru fiecare atribut al
                        // fiecarui element
                        while (justOpenedElements.size() > 0) {
                            val element = justOpenedElements.remove(0)
                            val attrTokens = generateAttrTokensFromElement(element, prevTextToken, "START")
                            tokenQueue.addAll(attrTokens)
                            prevAttrTokens.addAll(attrTokens)
                        }
                    }

                    start = end
                }
            } else if (TokenType.EmptyElement.isTokenType(t)) {
                // TODO: de construit contextul ce poate fi verificat
            } else if (TokenType.StartElement.isTokenType(t)
                    && !t.termText().toLowerCase().startsWith("<!")) {
                openedElements = openedElements :+ t.termText
                crtXPath = calculateCurrentXPath()
                justOpenedElements.add(t.termText())
            } else if (TokenType.EndElement.isTokenType(t)) {
                val endTag = t.termText().replaceAll("\\s+", "")
                if (openedElements.isEmpty)
                    throw new IOException(
                            "XML-ul nu e bine-format: nu am gasit element de deschidere pentru "
                                    + endTag)
                // inspectez valoarea elementului din openedElements
                val lastOpenedElement = openedElements.last
                openedElements = openedElements.dropRight(1)
                crtXPath = calculateCurrentXPath
                if (justOpenedElements.size() > 0)
                    justOpenedElements.remove(justOpenedElements.size() - 1)
                val matcher = XmlConcFilter.elementNamePattern.findFirstMatchIn(lastOpenedElement)
                if (matcher.isDefined) {
                    val elementName = matcher.get.group(1)
                    if (!endTag.equalsIgnoreCase("</" + elementName + ">"))
                        throw new IOException(
                                "XML-ul nu e bine-format: elementul " + endTag
                                        + " nu inchide elementul "
                                        + lastOpenedElement)
                    val attrTokens = generateAttrTokensFromElement(lastOpenedElement, prevTextToken, "END")
                    tokenQueue.addAll(attrTokens)
                } else
                    throw new IOException("Acesta nu e element de deschidere: "
                            + lastOpenedElement)
            }
        }

        return tokenQueue.remove(0)
    }

    private def calculateCurrentXPath(): String = {
        var xpath = ""
        for (element <- openedElements) {
            val m1 = XmlConcFilter.elementNamePattern.findFirstMatchIn(element);
            if (m1.isDefined)
                xpath += "/" + m1.get.group(1)
            xpath += "["
            val m2 = XmlConcFilter.attributePattern.findAllMatchIn(element)
            xpath += m2.map { x => x.group(1) + "=" + x.group(2) }.mkString(" ")
            xpath += "]";
        }
        xpath = xpath.replaceAll(" \\]", "]")
        xpath = xpath.replaceAll("\\[\\]", "")
        return xpath
    }

    private def generateAttrTokensFromElement(element: String, t: Token, suffix: String): java.util.List[Token] = {
        if (t == null)
            return Collections.emptyList()
        // logger.debug("token: " + t);
        // logger.debug("suffix: " + suffix);
        val attrTokens = new ArrayList[Token]

        val matches = XmlConcFilter.attributePattern.findAllMatchIn(element)
        for (matcher <- matches) {
            val attrName = matcher.group(1)
            if (!ignoreAttributes.contains(attrName.toLowerCase())) {
              configurator.addAttribute(attrName)
              val attrValue = matcher.group(2)
              // logger.debug(attrName + " = " + attrValue);
              val newToken = new Token(FieldUtils.PREFIX_ATTRIBUTE + attrName + "=" + attrValue + ";" + suffix, t.startOffset(), t.endOffset(), "attribute")
              newToken.setPositionIncrement(0)
              // logger.debug("new token: " + newToken);
              attrTokens.add(newToken)
            }
        }

        return attrTokens
    }


}

object XmlConcFilter {
    private val propFileName = "lucon.properties"
    private val attributePattern = "\\b([^\\s\"=<>\\/]+)\\s*=\\s*(\"[^\"]+\")".r
    private val elementNamePattern = "^<([^<>\\s\\/]+)\\b".r
}