package ro.cmit.lucon.analyzers

import org.apache.lucene.analysis.Token

object TokenType extends Enumeration {
  type TokenType = Value
  val Text = Value("text", "text")
  val EmptyElement = Value("emptyElement", "emptyElement")
  val StartElement = Value("startElement", "startElement")
  val EndElement = Value("endElement", "endElement")
  val ProcessingInstruction = Value("pi", "pi")
  val Comment = Value("comment", "comment")
  val XPath = Value("xpath", "xpath")

  class MyVal(name: String, val `type`: String) extends Val(nextId, name) {
    def isTokenType(token: Token) = token != null && `type`.equalsIgnoreCase(token.`type`)
  }

  protected final def Value(name: String, `type`: String): MyVal = new MyVal(name, `type`)
}
