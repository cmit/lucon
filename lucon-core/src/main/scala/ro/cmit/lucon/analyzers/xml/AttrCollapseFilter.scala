package ro.cmit.lucon.analyzers.xml

import org.apache.lucene.analysis._

class AttrCollapseFilter(in: TokenStream) extends TokenFilter(in) {

  var tokenQueue: List[Token] = List()

  override def next(): Token = {
    if (tokenQueue.size > 0) {
      val first = tokenQueue.head
      tokenQueue = tokenQueue.tail
      return first
    }
    var t = input.next()
    if (t == null) return null
    if (t.getPositionIncrement() == 0 && t.`type`().equalsIgnoreCase("attribute")) {
      while (t != null && t.getPositionIncrement() == 0 && t.`type`().equalsIgnoreCase("attribute")) {
        val tokenWithIndex = search(t, ";START", ";END").orElse(search(t, ";END", ";START"))
        if (tokenWithIndex.isDefined)
          tokenQueue = tokenQueue.patch(tokenWithIndex.get._2, Seq(tokenWithIndex.get._1), 1)
        else
          tokenQueue = tokenQueue :+ t
        t = input.next();
      }

      if (t != null)
        tokenQueue = tokenQueue :+ t

      t = tokenQueue.head
      tokenQueue = tokenQueue.tail
    }
    return t;
  }

  private def search(t: Token, oldFlag: String, newFlag: String): Option[Tuple2[Token, Int]] = {
    if (t.termText.endsWith(oldFlag)) {
      val searchTermText = t.termText.replaceFirst(oldFlag, newFlag)
      val tokenWithIndex = tokenQueue.zipWithIndex.find { _._1.termText.equalsIgnoreCase(searchTermText) }
      if (tokenWithIndex.isDefined) {
        val newTokenText = searchTermText.replaceFirst(newFlag, ";FULL")
        val tt = tokenWithIndex.get._1
        val newToken = new Token(newTokenText, tt.startOffset, tt.endOffset, "attribute")
        newToken.setPositionIncrement(0)
        return Some((newToken, tokenWithIndex.get._2))
      }
    }

    None
  }

}