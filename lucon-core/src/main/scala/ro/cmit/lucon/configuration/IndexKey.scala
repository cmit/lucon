package ro.cmit.lucon.configuration

import ro.cmit.libs.tsc.TypeSafeKey
import java.io.File
import ro.cmit.libs.tsc.DefaultKey
import ro.cmit.libs.tsc.key.property.PropertyKeyImpl
import ro.cmit.libs.formatters.impl.CollectionFormatter
import ro.cmit.libs.formatters.impl.StringFormatter
import ro.cmit.libs.tsc.key.property.PropertyKey
import java.util.Collection

object IndexKey {
  val INDEX_ROOT_DIR: TypeSafeKey[File] = new TypeSafeKey[File] {}
  val INDEX_FILE: TypeSafeKey[File] = new TypeSafeKey[File] {}
  val INDEX_CONTENT_DIR: TypeSafeKey[File] = new TypeSafeKey[File] {}
  val INDEX_METAINFO_DIR: TypeSafeKey[File] = new TypeSafeKey[File] {}

  val ATTRIBUTES: PropertyKey[Collection[String]] = PropertyKeyImpl.create("attributes", new CollectionFormatter[String](new StringFormatter, ","))

  val METAFIELDS: PropertyKey[Collection[String]] = PropertyKeyImpl.create("metafields", new CollectionFormatter[String](new StringFormatter, ","))

  val LEFT_CONTEXT: DefaultKey[Int] = new DefaultKey[Int](6)
  val RIGHT_CONTEXT: DefaultKey[Int] = new DefaultKey[Int](6)

}