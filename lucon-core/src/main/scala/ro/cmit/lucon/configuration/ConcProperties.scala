package ro.cmit.lucon.configuration

import ro.cmit.libs.tsc.TypeSafeProperties

class ConcProperties extends TypeSafeProperties(ConcKey.IGNORED_ATTRIBUTES, ConcKey.WORD_PATTERN)
