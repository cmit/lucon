package ro.cmit.lucon.configuration

import java.util.Collection
import ro.cmit.libs.formatters.impl.CollectionFormatter
import ro.cmit.libs.formatters.impl.StringFormatter
import ro.cmit.libs.tsc.key.property.PropertyKey
import ro.cmit.libs.tsc.key.property.PropertyKeyImpl
import ro.cmit.libs.formatters.impl.IntegerFormatter

object ConcKey {
  val IGNORED_ATTRIBUTES: PropertyKey[Collection[String]] = PropertyKeyImpl.create("ignore.attributes", new CollectionFormatter[String](new StringFormatter, ","))
  val WORD_PATTERN: PropertyKey[String] = PropertyKeyImpl.create("word.pattern", new StringFormatter)
  val BUFFER_SIZE: PropertyKey[Integer] = PropertyKeyImpl.create("file.buffer.size", new IntegerFormatter)
}