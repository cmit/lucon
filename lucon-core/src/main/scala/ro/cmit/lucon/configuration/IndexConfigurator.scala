package ro.cmit.lucon.configuration

import ro.cmit.libs.tsc.TypeSafeProperties
import java.io.File
import java.util.Map
import java.util.HashMap
import scala.collection.JavaConverters._
import java.util.Collections
import java.util.Collection
import java.util.TreeSet
import java.util.Set
import java.util.HashSet
import scala.collection.mutable.ListBuffer

class IndexConfigurator extends TypeSafeProperties(IndexKey.ATTRIBUTES, IndexKey.METAFIELDS) {

  var files: ListBuffer[File] = ListBuffer[File]()

  var xmlElements: Map[String, Integer] = new HashMap[String, Integer]

  /*
     * aici se colecteaza field-urile cu metadata care vor fi tratate cu
     * analizorul de tip text
     */
  var metaInfoFields: Map[String, String] = new HashMap[String, String]

  def init(): Unit = {
    files = ListBuffer[File]()
    xmlElements = new HashMap[String, Integer]
    metaInfoFields = new HashMap[String, String]
  }

  def addFile(f: File): Unit = files += f

  def addXmlElement(name: String) = {
    if (xmlElements.containsKey(name)) {
      val i = xmlElements.get(name)
      xmlElements.put(name, i + 1)
    } else
      xmlElements.put(name, 1)
  }

  def hasFiles = !files.isEmpty

  def getSizeOfFiles(): Long = {
    if (files == null)
      return 0
    files.map(_.length).foldLeft(0L)(_ + _)
  }

  def setIndexRootDir(indexRootDir: File): Unit = {
    set(IndexKey.INDEX_ROOT_DIR, indexRootDir)
    set(IndexKey.INDEX_CONTENT_DIR, new File(indexRootDir.getAbsolutePath()
      + File.separator + IndexConfigurator.CONTENT_NAME_DIR))
    set(IndexKey.INDEX_METAINFO_DIR,
      new File(indexRootDir.getAbsolutePath() + File.separator
        + IndexConfigurator.METAINFO_NAME_DIR))
    set(IndexKey.INDEX_FILE, new File(indexRootDir.getAbsolutePath()
      + File.separator + indexRootDir.getName() + ".idx"))
  }

  def getFiles = files

  def getXmlElements = xmlElements

  def getXmlMetaElements(): Collection[String] = {
    val collection = get(IndexKey.METAFIELDS)
    if (collection == null)
      return Collections.emptySet()
    return collection
  }

  def addXmlMetaElement(xmlMetaElement: String): Unit = {
    if (get(IndexKey.METAFIELDS) == null)
      set(IndexKey.METAFIELDS, new TreeSet[String])
    get(IndexKey.METAFIELDS).add(xmlMetaElement)
  }

  def indexExists: Boolean = {
    val contentDir = get(IndexKey.INDEX_CONTENT_DIR)
    val metainfoDir = get(IndexKey.INDEX_METAINFO_DIR)
    val indexFile = get(IndexKey.INDEX_FILE)
    return contentDir.exists && metainfoDir.exists && indexFile.exists
  }

  def getAttributes: Collection[String] = {
    val collection = get(IndexKey.ATTRIBUTES)
    if (collection == null)
      return Collections.emptySet()
    return collection
  }

  def addAttribute(attr: String): Unit = {
    if (get(IndexKey.ATTRIBUTES) == null)
      set(IndexKey.ATTRIBUTES, new TreeSet[String])
    get(IndexKey.ATTRIBUTES).add(attr)
  }

  def addMetaInfoField(elementName: String, tokenString: String) = {
    if (metaInfoFields.containsKey(elementName))
      metaInfoFields.put(elementName, metaInfoFields.get(elementName)
        + " " + tokenString)
    else
      metaInfoFields.put(elementName, tokenString)
  }

  def getMetaInfoFields = metaInfoFields
}

object IndexConfigurator {
  private val CONTENT_NAME_DIR = ".content";
  private val METAINFO_NAME_DIR = ".metainfo";
}