# Lucon

## Description
**Lucon** = acronym of **Lu**cene based **con**cordancer.

A _concordancer_ is a computer program that builds a _concordance_, i.e. the alphabetical list 
of the words used in a corpus, listing every instance of each word with its number of occurrences
and immediate, left and right context.

**Lucon** uses Lucene (http://lucene.apache.org/) which is an indexing and search
library.

## How to install

### Install Java
From here: https://www.java.com/en/download/.

### Install Lucon
**Lucon** is released as a zip archive in this location: https://gitlab.com/cmit/lucon/tags.
Steps to install:
- download the latest version,
- unzip it in any location,
- go to the newly created folder, `lucon-0.3.18`,
- run `lucon.bat` (for Windows) or `lucon.sh` (for Linux/MacOS). Sometimes it 
works by only double-clicking the `lucon-gui-0.3.18.jar` file.

From Linux command line:
```
$ unzip lucon-0.3.18.zip
$ cd lucon-0.3.18
$ ./lucon.sh
```

## How to use
**Lucon** uses Apache Lucene which is a full-featured text search engine library written in Java.
The library uses an inverted index (short _index_) for a faster retrieval of the content.
**Lucon** can create a such index from any `.xml` or `.txt` files encoded in UTF-8 format.

### Build index
To build the index:

- From the menu choose `Concordance > Build index` or press `Ctrl + B` to open the _Create index_ wizard.
- In the **Choose type and source for indexing** step:
  - Choose **File** if you want to index a single file, or **Directory** if you want to
    index files from a folder/directory.
  - **Process directory recursively** is used only when **Directory** is selected. If checked, **Lucon**
    will search for files in the current directory and in all subdirectories to be indexed.
  - Use the **Browse** button to choose the file(s) or directory to be indexed
  - **Deep search for metadata elements (very slow)** is used only if `.xml` files are indexed.
    Sometimes an `.xml` can contain elements whose content the user does not want to be indexed or at least
    it can be indexed but separately from the main content (such elements are
    called _metadata_ elements). As examples of metadata elements we can mention: the author of a book or
    the information about the publishing of the book (year, location, number of pages) etc. 
    If checked, **Lucon** will parse all `.xml` files and will build a list containing all found elements
    to let the user choose the metadata elements.
  - Use the **Next** button to advance to the next step.
- In the **Analysis report** step, **Lucon** reports the number of files found and the total size (in bytes) of
  what will be indexed. Use the **Next** button to advance to the next step.
- In the **Choose metadata elements** step the user should choose the XML elements that will be considered as
metadata. The content of the metadata elements will be indexed into a separate index to let the user
the possibility to use the information in searches, for example when (s)he wants to search for terms used
by a certain author. Choose the metadata elements by selecting one or more elements from the left side
using the **Add**, **Add all**, **Remove** or **Remove all** buttons. Use the **Next** button to advance to the
next step when finished.
- In the **Choose save location** step choose the location where the index will be created for further
uses. It is recommanded that the chosen location should be an empty folder not to overlap with other files.
  - If **Automatically open index after build** is checked, then the newly created index will be opened
automatically. Also provide the left and right sizes for the short contexts. Press the **Finish** button
and wait till **Lucon** builds the index.

### Open index
To open an already created index:

- From menu `Concordance > Open index` or press `Ctrl + O` to open the _Open index_ wizard.
- Search for the `.idx` file using the **Browse** button and then press **Finish**.

Another possibility to open an index is to use from the menu `Concordance > Open recent`.

You can open simultaneously as many indexes as you want. Each index will have its proper internal window
to be investigated. The same index can be opened in two or more internal windows (also called views).

### Index viewer (window)

An index viewer is split in 3 frames:

- The left frame is for term searching and it is composed of:
  - a text field, where the user enters the expression to search for terms. For more details about
    how the search can be done see section [Search](#search).
  - a table with 2 columns: one for terms and one for frequencies. This table can be sorted (ascending/descending)
    by clicking the table headers **Terms** and **Frequency**. By default, the terms are displayed
    in ascending order. You can select one or more terms (using <Shift> and/or <Ctrl> keys) to see the
    corresponding contexts.
  - a pagination area, if the number of found terms is too large to be loaded entirely. To navigate to a certain page
    you can use either the arrows or simply enter the number of the page and press <Enter>.
- The right-top frame is used to show the immediate context of the selected term(s). By default, the left and right context
  is 6, but it can be modified in the _Open index_ wizard. When an immediate context is selected, the user can view a larger
  context just below. Only one immediate context can be selected at a moment.
- The right-bottom frame is used to display a larger context of the context selected in the right-top frame. In case the selected
  context corresponds to a term coming from an XML structure, the XML path will be shown right under the large context.

### Search
Searching for term(s) can be done in the text field of the left frame of the index viewer. This search can be used in
2 ways:
- using regular expressions (https://www.regular-expressions.info/): enter the regular expression and wait for 3 seconds. The terms 
  matching the regular expression will be returned. The special characters '^' and '$' can be used.
- using a non-regular expression and pressing <Enter> returns the terms which are similar with the introduced term. The returned
  terms are calculated using the Levenshtein Distance 
  (https://people.cs.pitt.edu/~kirk/cs1501/Pruhs/Spring2006/assignments/editdistance/Levenshtein%20Distance.htm),
  having a factor of similarity of 0.75.

#### Advanced search
The **Advanced search** (from menu) can be used for more complex searches, esp. for terms being within a specific 
distance. For example, when searching for "I" and "know" within 3 words from each other, **Lucon** will return:
"I know", "I do know", "if you know", "is best known" etc. Please note the possible matching between "I" and "if"
or "is" (as result of the case-insensitive nature of the matching and of the fact that the found element
contains the searched for element), on the one hand, and between "know" and "known" (again as result of the fact
that the found element contains the searched for element), on the other hand.

In case of word level annotated corpora (XML files), the advanced search can be used to search not only for
text terms, but also for attribute values. For example, searching for a verb (in this case use the attribute
corresponding to the part of speech) followed by a certain word.

### Statistics
Shows the number of indexed words.

### Save index
Used to save the entire list of terms from the index and their corresponding frequencies.

### Save context
Used to save the immediate contexts of a selected term.
