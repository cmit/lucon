/*
 * Created on 14.05.2005
 * adaugat la comentariu pentru testul SVN 
 */
package ro.cmit.lucon;

import ro.cmit.lucon.gui.MainFrame;

/**
 * @author Catalin Mititelu
 */
object ConcMain {

    def main(args: Array[String]) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            override def run(): Unit = {
                MainFrame.createAndShowGUI();
            }
        });
    }
}
