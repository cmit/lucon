package ro.cmit.lucon.gui.wizard.step;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.apache.log4j.Logger;
import org.pietschy.wizard.InvalidStateException;
import org.pietschy.wizard.WizardModel;

import ro.cmit.lucon.configuration.IndexConfigurator;
import ro.cmit.lucon.configuration.IndexKey;
import ro.cmit.lucon.gui.tasks.ATask;
import ro.cmit.lucon.gui.tasks.IndexDeletingTask;
import ro.cmit.lucon.gui.tasks.IndexOpeningTask;
import ro.cmit.lucon.gui.tasks.IndexingTask;
import ro.cmit.lucon.gui.wizard.GuiKey;
import foxtrot.Worker;
import ChooseSaveLocationStep._

class ChooseSaveLocationStep(configuration: IndexConfigurator) extends AIndexStep("Choose save location.", "", configuration)
  with ActionListener with DocumentListener with ChangeListener {
  private var task: ATask = null;

  private var textField: JTextField = null;

  private var button: JButton = null;

  private var openIndexCheckBox: JCheckBox = null;

  private var lcsSpinner: JSpinner = null;

  private var rcsSpinner: JSpinner = null;

  private var progressBar: JProgressBar = null;

  private val userDir = new File(System.getProperty("user.dir"))
    .getAbsolutePath();

  private val fileChooser = new JFileChooser(new File(userDir));
  fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
  fileChooser.setMultiSelectionEnabled(false);
  setLayout(new BorderLayout(3, 3));
  add(createPanel(), BorderLayout.NORTH);

  private def createPanel(): Component = {
    val panel = new JPanel();
    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
    val label = createLabel("Save to directory");
    panel.add(label);
    val destinationChooserPanel = createDestinationChooserPanel();
    panel.add(destinationChooserPanel);
    openIndexCheckBox = createOpenIndexCheckBox();
    panel.add(openIndexCheckBox);
    panel.add(createContextSizePanel());
    panel.add(createLabel(" "));
    progressBar = createProgressBar();
    panel.add(progressBar);
    return panel;
  }

  private def createOpenIndexCheckBox(): JCheckBox = {
    val jCheckBox = new JCheckBox(
      "Automatically open index after build");
    jCheckBox.setSelected(true);
    jCheckBox.setAlignmentX(Component.LEFT_ALIGNMENT);
    jCheckBox.addActionListener(this);
    return jCheckBox;
  }

  private def createProgressBar(): JProgressBar = {
    val progressBar = new JProgressBar(0, 100);
    progressBar.setStringPainted(true);
    progressBar.setAlignmentX(Component.LEFT_ALIGNMENT);
    return progressBar;
  }

  private def createLabel(str: String): JLabel = {
    val label = new JLabel(str);
    label.setAlignmentX(Component.LEFT_ALIGNMENT);
    return label;
  }

  private def createDestinationChooserPanel(): JPanel = {
    val panel = new JPanel(new BorderLayout());
    // Directory path text field
    textField = new JTextField();
    textField.getDocument().addDocumentListener(this);
    panel.add(textField, BorderLayout.CENTER);
    button = new JButton("Browse...");
    panel.add(button, BorderLayout.EAST);
    button.addActionListener(this);
    panel.setAlignmentX(Component.LEFT_ALIGNMENT);
    return panel;
  }

  private def createContextSizePanel(): Component = {
    val panel = new JPanel(new GridLayout(2, 1));
    panel.add(createCSPanel("Left context size", true));
    panel.add(createCSPanel("Right context size", false));
    panel.setAlignmentX(Component.LEFT_ALIGNMENT);
    return panel;
  }

  private def createCSPanel(labelStr: String, isLeft: Boolean): Component = {
    val panel = new JPanel(new BorderLayout());
    val label = new JLabel(labelStr);
    panel.add(label, BorderLayout.WEST);
    var spinnerModel: SpinnerNumberModel = null;
    var spinner: JSpinner = null;
    if (isLeft) {
      spinnerModel = new SpinnerNumberModel(configurator.getDefault(IndexKey.LEFT_CONTEXT), 0, 200, 1);
      spinner = new JSpinner(spinnerModel);
      lcsSpinner = spinner;
    } else {
      spinnerModel = new SpinnerNumberModel(configurator.getDefault(IndexKey.RIGHT_CONTEXT).intValue(), 0, 200, 1);
      spinner = new JSpinner(spinnerModel);
      rcsSpinner = spinner
    }
    spinnerModel.addChangeListener(this);
    panel.add(spinner, BorderLayout.EAST);

    return panel;
  }

  override def actionPerformed(e: ActionEvent): Unit = {
    val source = e.getSource();
    if (source == button) {
      val returnVal = fileChooser.showOpenDialog(this);
      if (returnVal == JFileChooser.APPROVE_OPTION) {
        val toDir = fileChooser.getSelectedFile();
        configurator.setIndexRootDir(toDir);
        textField.setText(toDir.getAbsolutePath());
        setComplete(true);
      }
    } else if (source == openIndexCheckBox) {
      val value: Boolean = openIndexCheckBox.isSelected();
      configurator.set(GuiKey.INDEX_OPENER, value);
      lcsSpinner.setEnabled(value);
      rcsSpinner.setEnabled(value);
    }
  }

  override def isComplete(): Boolean = {
    val saveLocation = textField.getText().trim();
    if (saveLocation.length() == 0)
      return false;
    return true;
  }

  @throws[InvalidStateException]
  override def applyState(): Unit = {
    var ok = false;
    setBusy(true);
    try {
      val saveLocation = textField.getText().trim();
      val location = new File(saveLocation);

      configurator.setIndexRootDir(
        new File(textField.getText().trim()));
      if (!location.exists())
        ok = true;
      else {
        if (configurator.indexExists) {
          val ret = JOptionPane
            .showConfirmDialog(
              this,
              "This operation will delete the existing index. Are you sure you want to continue?",
              "Existing index.",
              JOptionPane.YES_NO_OPTION,
              JOptionPane.WARNING_MESSAGE);
          if (ret == JOptionPane.YES_OPTION)
            ok = true;
          else
            ok = false;
        } else {
          ok = true;
        }
      }
      if (ok) {
        // stergerea directoarelor existente
        if (configurator.indexExists) {
          task = new IndexDeletingTask(configurator);
          task.setProgressBar(progressBar);
          Worker.post(task);
        }
        if (configurator.indexExists) {
          JOptionPane.showMessageDialog(
            this,
            "The index could not be saved in this location beacause the existing index could not be deleted.\nPlease use another save location.",
            "Warning.", JOptionPane.WARNING_MESSAGE);
        } else {
          // indexarea propriu-zisa
          // do some work on another thread.. see Foxtrot
          task = new IndexingTask(configurator);
          task.setProgressBar(progressBar);
          Worker.post(task);

          if (configurator.getDefault(GuiKey.INDEX_OPENER)) {
            task = new IndexOpeningTask(configurator);
            task.setProgressBar(progressBar);
            Worker.post(task);
          }
        }
      }
    } catch {
      case e: Exception =>
        logger.error("Error occurred.", e);
    } finally {
      setBusy(false);
    }
    if (!ok) {
      throw new InvalidStateException("Unexpected error.\nPlease review the settings, especially save location.");
    }
  }

  override def abortBusy(): Unit = {
    super.abortBusy();
    if (task != null) {
      task.setTaskInterrupted(true);
    }

  }

  override def init(model: WizardModel): Unit = {
    // TODO: de implementat
  }

  override def prepare(): Unit = { // read the model and configure the panel
    // TODO: de implementat
  }

  override def changedUpdate(e: DocumentEvent): Unit = {
    if (e.getDocument().getLength() > 0)
      this.setComplete(true);
    else
      this.setComplete(false);
  }

  override def insertUpdate(e: DocumentEvent): Unit = {
    if (e.getDocument().getLength() > 0)
      this.setComplete(true);
    else
      this.setComplete(false);
  }

  override def removeUpdate(e: DocumentEvent): Unit = {
    if (e.getDocument().getLength() > 0)
      this.setComplete(true);
    else
      this.setComplete(false);
  }

  override def stateChanged(e: ChangeEvent): Unit = {
    val eventSource = e.getSource();
    if (eventSource.isInstanceOf[SpinnerNumberModel]) {
      if (eventSource == lcsSpinner.getModel()) {
        configurator.set(IndexKey.LEFT_CONTEXT, eventSource.asInstanceOf[SpinnerNumberModel].getValue().asInstanceOf[Int]);
      } else if (eventSource == rcsSpinner.getModel()) {
        configurator.set(IndexKey.RIGHT_CONTEXT, eventSource.asInstanceOf[SpinnerNumberModel].getValue().asInstanceOf[Int]);
      }
    }
  }

}

object ChooseSaveLocationStep {
  val logger = Logger.getLogger(ChooseSaveLocationStep.getClass);
}