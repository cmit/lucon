package ro.cmit.lucon.gui;

import java.util.Observable;

import ro.cmit.lucon.gui.exception.LuconException;

class ErrorObservable extends Observable {

  def reportError(e: LuconException, userMessage: String) = {
    // Notify observers of change
    setChanged();
    notifyObservers(userMessage);
  }
}
