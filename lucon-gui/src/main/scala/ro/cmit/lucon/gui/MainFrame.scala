package ro.cmit.lucon.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.InternalFrameEvent;

import org.apache.log4j.Logger;
import org.apache.lucene.LucenePackage;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermDocs;
import org.apache.lucene.index.TermEnum;

import ro.cmit.lucon.LuconPackage;
import ro.cmit.lucon.configuration.IndexConfigurator;
import ro.cmit.lucon.configuration.IndexKey;
import ro.cmit.lucon.engine.Context;
import ro.cmit.lucon.engine.FieldUtils;
import ro.cmit.lucon.gui.tasks.IndexOpeningTask;
import ro.cmit.lucon.gui.viewers.ALuconViewer;
import ro.cmit.lucon.gui.viewers.ConcViewer;
import ro.cmit.lucon.gui.viewers.ConcViewer2;
import ro.cmit.lucon.gui.wizard.GuiKey;
import ro.cmit.lucon.gui.wizard.IndexWizard;
import ro.cmit.lucon.gui.wizard.OpenIndexWizard;
import ro.cmit.lucon.gui.wizard.SaveContextsWizard;
import ro.cmit.lucon.gui.wizard.SaveWordListWizard;
import MainFrame._;
import java.awt.Font
import scala.collection.JavaConverters._
import javax.swing.plaf.metal.MetalLookAndFeel.FontActiveValue

import ro.cmit.libs.tsc.TypeSafeProperties

/*
 * InternalFrameDemo.java is a 1.4 application that requires:
 *   MyInternalFrame.java
 */
class MainFrame extends AbstractMainFrame("Lucon") {
    private var _width = 440;
    private var _height = 280;
    private var iconData: Array[Byte] = null;
    private var menuFileOpenRecent: JMenu = null;
    private var menuSearch: JMenu = null;
    private var saveIndexMenuItem: JMenuItem = null;
    private var saveContextMenuItem: JMenuItem = null;

    try {
        this.iconData = ImageUtils.getIconData("/icons/lc.jpg");
        super.setIconImage(new ImageIcon(iconData).getImage());

        // Make the big window be indented 50 pixels from each edge
        // of the screen.
        val inset = 100;
        val screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        _width = screenSize.width - inset * 2;
        _height = screenSize.height - inset * 2;
        setBounds(inset, inset, _width, _height);

        setLayout(new BorderLayout());

        // create the menu
        add(createMenuBar(), BorderLayout.NORTH);

        // Create the desktop
        add(desktop, BorderLayout.CENTER);

        // TODO: alte lucruri legate de aspectul ferestrei
    } catch {
      case e: HeadlessException =>
        logger.error("Error creating main frame", e);
      case e: IOException =>
        logger.error("Error creating main frame", e);
    }

    protected def createMenuBar(): JMenuBar = {
        val menuBar = new JMenuBar();

        val menuFile = createMenuFile();
        menuBar.add(menuFile);

        menuSearch = createMenuSearch();
        menuSearch.setEnabled(false);
        menuBar.add(menuSearch);

        val menuTools = createMenuTools();
        menuBar.add(menuTools);

        val menuAbout = createMenuAbout();
        menuBar.add(menuAbout);

        return menuBar;
    }

    private def createMenuFile(): JMenu = {
        val menuFile = new JMenu("Concordance");
        menuFile.setMnemonic(KeyEvent.VK_C);

        val buildMenuItem = new JMenuItem("Build index");
        buildMenuItem.setMnemonic(KeyEvent.VK_B);
        buildMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B,
                ActionEvent.CTRL_MASK));
        buildMenuItem.setActionCommand("buildindex");
        buildMenuItem.addActionListener(this);
        menuFile.add(buildMenuItem);

        val openMenuItem = new JMenuItem("Open index");
        openMenuItem.setMnemonic(KeyEvent.VK_O);
        openMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,
                ActionEvent.CTRL_MASK));
        openMenuItem.setActionCommand("openindex");
        openMenuItem.addActionListener(this);
        menuFile.add(openMenuItem);

        menuFileOpenRecent = new JMenu("Open recent");
        menuFile.add(menuFileOpenRecent);
        updateMenuFileOpenRecent();

        saveIndexMenuItem = new JMenuItem("Save index");
        saveIndexMenuItem.setMnemonic(KeyEvent.VK_S);
        saveIndexMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,
                ActionEvent.CTRL_MASK));
        saveIndexMenuItem.setActionCommand("saveindex");
        saveIndexMenuItem.addActionListener(this);
        menuFile.add(saveIndexMenuItem);
        saveIndexMenuItem.setEnabled(false);

        saveContextMenuItem = new JMenuItem("Save contexts");
        // menuItem.setMnemonic(KeyEvent.VK_C);
        // menuItem.setAccelerator(KeyStroke.getKeyStroke(
        // KeyEvent.VK_C, ActionEvent.CTRL_MASK));
        saveContextMenuItem.setActionCommand("savecontext");
        saveContextMenuItem.addActionListener(this);
        menuFile.add(saveContextMenuItem);
        saveContextMenuItem.setEnabled(false);

        val quitMenuItem = new JMenuItem("Quit");
        quitMenuItem.setMnemonic(KeyEvent.VK_Q);
        quitMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q,
                ActionEvent.CTRL_MASK));
        quitMenuItem.setActionCommand("quit");
        quitMenuItem.addActionListener(this);
        menuFile.add(quitMenuItem);

        return menuFile;
    }

    override def updateMenuFileOpenRecent(): Unit = {
        val dir = new File(startDirectory, recentsDirectory);
        var success = true;
        if (!dir.exists())
            success = success && dir.mkdirs();
        menuFileOpenRecent.removeAll();
        val menuFiles = dir.listFiles();
        if (menuFiles == null || menuFiles.length == 0) {
            val menuItem = new JMenuItem("N/A");
            menuItem.setEnabled(false);
            menuFileOpenRecent.add(menuItem);
        } else {
            val alreadyOpenedFiles = new HashSet[String]();
            for (file <- menuFiles) {
                val props = new Properties();
                try {
                    val fis = new FileInputStream(file);
                    props.load(fis);
                    fis.close();

                    val indexPath = props.getProperty("index.path");
                    if (!alreadyOpenedFiles.contains(indexPath)) {
                        val leftContextSize = props
                                .getProperty("left.context.size");
                        val rightContextSize = props
                                .getProperty("right.context.size");
                        val menuItem = new JMenuItem(indexPath);
                        menuItem.setActionCommand("openindex:" + indexPath
                                + ";" + leftContextSize + ";"
                                + rightContextSize);
                        menuItem.addActionListener(this);
                        menuFileOpenRecent.add(menuItem);
                        alreadyOpenedFiles.add(indexPath);
                    }
                } catch {
                  case e: Exception =>
                    logger.error("Error occurred.", e);
                }
            }
        }
    }

    private def createMenuSearch(): JMenu = {
        val menuFile = new JMenu("Advanced search");
        menuFile.setMnemonic(KeyEvent.VK_D);

        val menuItem = new JMenuItem("Search ...");
        menuItem.setMnemonic(KeyEvent.VK_S);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,
                ActionEvent.ALT_MASK));
        menuItem.setActionCommand("search");
        menuItem.addActionListener(this);
        menuFile.add(menuItem);

        return menuFile;
    }

    private def createMenuTools(): JMenu = {
        val menuFile = new JMenu("Tools");
        menuFile.setMnemonic(KeyEvent.VK_T);

        val menuItem = new JMenuItem("Statistics");
        menuItem.setMnemonic(KeyEvent.VK_S);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,
                ActionEvent.ALT_MASK));
        menuItem.setActionCommand("statistics");
        menuItem.addActionListener(this);
        menuFile.add(menuItem);

        return menuFile;
    }

    private def createMenuAbout(): JMenu = {
        val menuFile = new JMenu("About");
        menuFile.setMnemonic(KeyEvent.VK_A);

        val menuItem = new JMenuItem("About this ...");
        menuItem.setMnemonic(KeyEvent.VK_B);
        menuItem.setActionCommand("about");
        menuItem.addActionListener(this);
        menuFile.add(menuItem);

        return menuFile;
    }

    // React to menu selections.
    override def actionPerformed(e: ActionEvent): Unit = {

        val command = e.getActionCommand();
        val configurator = new IndexConfigurator();
        configurator.set(GuiKey.MAIN_FRAME, this);
        if (command.equals("buildindex")) {
            val indexWizard = new IndexWizard(configurator);
            indexWizard.show("Create index", this);
        } else if (command.equals("openindex")) {
            val openIndexWizard = new OpenIndexWizard(configurator);
            openIndexWizard.show("Open index", this);
        } else if (command.startsWith("openindex:")) {
            val params = command.substring("openindex:".length());
            val paramList = params.split(";");
            configurator
                    .setIndexRootDir(new File(paramList(0)).getParentFile());
            configurator.set(IndexKey.LEFT_CONTEXT, paramList(1).toInt);
            configurator.set(IndexKey.RIGHT_CONTEXT, paramList(2).toInt);
            try {
                val indexOpeningTask = new IndexOpeningTask(configurator);
                indexOpeningTask.run();
            } catch {
              case e1: InterruptedException =>
                logger.error("Error occurred.", e1);
            }
        } else if (command.equals("saveindex")) {
            var configuration: IndexConfigurator = null;
            val internalFrame = desktop.getSelectedFrame();
            if (internalFrame == null) {
                JOptionPane.showMessageDialog(this,
                        "There is no window active.");
                return;
            }
            if (internalFrame.isInstanceOf[ConcViewer]) {
                val cv = internalFrame.asInstanceOf[ConcViewer];
                configuration = cv.configurator;
            } else if (internalFrame.isInstanceOf[ConcViewer2]) {
                val cv = internalFrame.asInstanceOf[ConcViewer2];
                configuration = cv.configurator;
            }
            val saveWordListWizard = new SaveWordListWizard(
                    configuration);
            saveWordListWizard.show("Save word list", this);
        } else if (command.equals("savecontext")) {
            var topTree: TopTree = null;
            val internalFrame = desktop.getSelectedFrame();
            if (internalFrame == null) {
                JOptionPane.showMessageDialog(this,
                        "There is no window active.");
                return;
            }
            if (internalFrame.isInstanceOf[ConcViewer]) {
                val cv = internalFrame.asInstanceOf[ConcViewer];
                topTree = cv.getTopTree();
            } else if (internalFrame.isInstanceOf[ConcViewer2]) {
                val cv = internalFrame.asInstanceOf[ConcViewer2];
                topTree = cv.getTopTree();
            }

            if (topTree != null) {
                val contexts = topTree.getContexts();
                if (contexts == null || contexts.size() == 0) {
                    JOptionPane.showMessageDialog(this,
                            "There is no context to save.");
                    return;
                }
                val saveContextsWizard = new SaveContextsWizard(
                        contexts, configurator);
                saveContextsWizard.show("Save contexts", this);
            }
        } else if (command.equals("search")) {
            advancedSearch();
        } else if (command.equals("statistics")) {
            val statisticInfo = statistics();
            if (statisticInfo != null && !"".equals(statisticInfo))
                JOptionPane.showMessageDialog(this, statisticInfo,
                        "Statistics", JOptionPane.INFORMATION_MESSAGE);
        } else if (command.equals("quit")) {
            quit();
        } else if (command.equals("about")) {
            val lucenePackage = LucenePackage.get();
            val luconPackage = LuconPackage.get();
            JOptionPane.showMessageDialog(
                    this,
                    luconPackage.getImplementationTitle() + " " + luconPackage.getImplementationVersion() + "\nbased on Lucene " + lucenePackage.getSpecificationVersion() + "\nDeveloped by " + luconPackage.getImplementationVendor(),
                    "About Lucon", JOptionPane.INFORMATION_MESSAGE,
                    new ImageIcon(iconData));
        } else {
            JOptionPane.showMessageDialog(this,
                    "This functionality is not implemented yet.");
        }
    }

    private def statistics(): String = {
        var statisticInfo = "";
        var wordCount = 0;
        val internalFrame = desktop.getSelectedFrame();
        var contentIndex: IndexReader = null;
        var activeViewer: ALuconViewer = null;
        if (internalFrame.isInstanceOf[ConcViewer]) {
            val cv = internalFrame.asInstanceOf[ConcViewer];
            contentIndex = cv.getConcordance().indexReader;
            activeViewer = cv;
        } else if (internalFrame.isInstanceOf[ConcViewer2]) {
            val cv = internalFrame.asInstanceOf[ConcViewer2];
            contentIndex = cv.getConcordance().indexReader;
            activeViewer = cv;
        }

        if (contentIndex != null) {
            try {
                if (activeViewer != null)
                    activeViewer.blockMouseAndKeyboard();
                val te = contentIndex.terms();
                while (te.next()) {
                    val t = te.term();
                    if ("text".equals(t.field())) {
                      val termString = t.text();
                      if (!termString.startsWith(FieldUtils.PREFIX_ATTRIBUTE)) {
                        if (!termString.startsWith(FieldUtils.PREFIX_XPATH)) {
                          var fr = 0;
                          val td = contentIndex.termDocs(t);
                          while (td.next())
                              fr = fr + td.freq();
                          wordCount = wordCount + fr;
                        }
                      }
                    }
                }
                te.close();
                statisticInfo += "Word count: " + wordCount + "\n";
                if (activeViewer != null)
                    activeViewer.freeMouseAndKeyboard();
            } catch {
              case e: IOException =>
                statisticInfo = "Unable to get statistics. Error occured: " + e.getMessage();
                if (activeViewer != null)
                    activeViewer.freeMouseAndKeyboard();
            }
        }

        return statisticInfo;
    }

    // Quit the application.
    protected def quit(): Unit = {
        System.exit(0);
    }

    private def advancedSearch(): Unit = {
        val asd = new AdvancedSearchDialog(this);
        asd.pack();
        asd.setVisible(true);
    }

    override def internalFrameActivated(e: InternalFrameEvent): Unit = {
        menuSearch.setEnabled(true);
        saveIndexMenuItem.setEnabled(true);
        saveContextMenuItem.setEnabled(true);
    }

    override def internalFrameClosed(e: InternalFrameEvent): Unit = {
    }

    override def internalFrameClosing(e: InternalFrameEvent): Unit = {
        menuSearch.setEnabled(false);
        saveIndexMenuItem.setEnabled(false);
        saveContextMenuItem.setEnabled(false);
    }

    override def internalFrameDeactivated(e: InternalFrameEvent): Unit = {
        menuSearch.setEnabled(false);
        saveIndexMenuItem.setEnabled(false);
        saveContextMenuItem.setEnabled(false);
    }

    override def internalFrameDeiconified(e: InternalFrameEvent): Unit = {
        menuSearch.setEnabled(true);
        saveIndexMenuItem.setEnabled(true);
        saveContextMenuItem.setEnabled(true);
    }

    override def internalFrameIconified(e: InternalFrameEvent): Unit = {
        menuSearch.setEnabled(false);
        saveIndexMenuItem.setEnabled(false);
        saveContextMenuItem.setEnabled(false);
    }

    override def internalFrameOpened(e: InternalFrameEvent): Unit = {
        menuSearch.setEnabled(true);
        saveIndexMenuItem.setEnabled(true);
        saveContextMenuItem.setEnabled(true);
    }

    override def getWidth() = _width

    override def getHeight() = _height

}
object MainFrame {
    val startDirectory = System.getProperty("user.dir");
    val propFileName = "lucon.properties"
    val recentsDirectory = "recents"
    val properties = new TypeSafeProperties(GuiKey.FONT_SCALE)
    properties.load(new File(startDirectory, propFileName))

    val logger = Logger.getLogger(MainFrame.getClass);  

    /**
     * Create the GUI and show it. For thread safety, this method should be
     * invoked from the event-dispatching thread.
     */
    def createAndShowGUI(): Unit = {
        logger.info("Starting GUI.");
        var nativeLF = UIManager.getSystemLookAndFeelClassName();
        if (nativeLF == null || !nativeLF.toLowerCase().contains("windows"))
            nativeLF = UIManager.getCrossPlatformLookAndFeelClassName();
        try {
            UIManager.setLookAndFeel(nativeLF);
            val fontScale = properties.get(GuiKey.FONT_SCALE)
            UIManager.getDefaults().entrySet().asScala.foreach {
              entry =>
                if (entry.getKey.toString().contains("font")) {
                  val font = UIManager.get(entry.getKey).asInstanceOf[Font]
                  UIManager.put(entry.getKey, font.deriveFont(font.getSize + fontScale))
                }
            }
        } catch {
          case e: Exception =>
            logger.error("Error occurred.", e);
        }

        // Make sure we have nice window decorations.
        JFrame.setDefaultLookAndFeelDecorated(true);

        // Create and set up the window.
        val frame = new MainFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Display the window.
        frame.setVisible(true);
        logger.info("GUI ready.");
    }

}
