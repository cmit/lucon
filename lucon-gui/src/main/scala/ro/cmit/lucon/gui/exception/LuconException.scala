package ro.cmit.lucon.gui.exception

final case class LuconException(
  private val message: String    = "",
  private val cause:   Throwable = None.orNull)
  extends Exception(message, cause) {

  def this(cause: Throwable) = this("", cause)
}

