package ro.cmit.lucon.gui

import java.util.Observer

import javax.swing.JTree
import javax.swing.tree.{DefaultMutableTreeNode, TreePath, TreeSelectionModel}
import ro.cmit.lucon.engine.TermConcordance
import ro.cmit.lucon.gui.exception.LuconException
import ro.cmit.lucon.gui.viewers.ALuconViewer

import scala.collection.JavaConversions._

class TopTree(val viewer: ALuconViewer) extends JTree {

  private val model = new TopTreeModel()

  private val errorObservable = new ErrorObservable()

  setModel(model)
  model.setViewer(viewer)
  getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION)

  def getContexts() = model.getContexts

  def expandAll(expand: Boolean): Unit = {
    // Traverse tree from root
    try {
      if (viewer != null)
        viewer.blockMouseAndKeyboard()
      expandAll(new TreePath(model.getRoot()), expand)
      if (!expand)
        expandPath(new TreePath(model.getRoot()))
      if (viewer != null)
        viewer.freeMouseAndKeyboard()
    } catch {
      case e1: RuntimeException =>
        // catch any runtime exception as OutOfMemory and notify for
        // error
        if (viewer != null)
          viewer.freeMouseAndKeyboard()
        errorObservable.reportError(new LuconException(e1),
          "Sorry, an error occured. Not all contexts may have been "
            + (if (expand) "expanded" else "collapsed") + ".")
      case e2: Throwable =>
        // catch any runtime exception as OutOfMemory and notify for
        // error
        if (viewer != null)
          viewer.freeMouseAndKeyboard()
        errorObservable.reportError(new LuconException(e2),
          "Sorry, an error occured. Not all contexts may have been "
            + (if (expand) "expanded" else "collapsed") + ".")
    }
  }

  private def expandAll(parent: TreePath, expand: Boolean): Unit = {
    // Traverse children
    val node = parent.getLastPathComponent().asInstanceOf[DefaultMutableTreeNode]
    if (node.getChildCount() >= 0) {
      for (n <- node.children()) {
        val path = parent.pathByAddingChild(n)
        expandAll(path, expand)
      }
    }

    // Expansion or collapse must be done bottom-up
    if (expand) {
      expandPath(parent)
    } else {
      collapsePath(parent)
    }
  }

  def addErrorObserver(o: Observer) = errorObservable.addObserver(o)

  def updateModel(termConcordance: TermConcordance) = model.updateModel(termConcordance)
}
