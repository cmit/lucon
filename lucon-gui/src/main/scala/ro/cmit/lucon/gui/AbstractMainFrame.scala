package ro.cmit.lucon.gui

import javax.swing.JFrame
import java.awt.event.ActionListener
import javax.swing.event.InternalFrameListener
import javax.swing.JDesktopPane
import java.awt.Color

abstract class AbstractMainFrame(name:String) extends JFrame(name) with ActionListener with InternalFrameListener {
  val desktop: JDesktopPane = {
    val desktop = new JDesktopPane(); // a specialized layered pane
    desktop.setBackground(Color.WHITE);
    // Make dragging a little faster but perhaps uglier.
    desktop.setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);
    desktop
  }
  
  def updateMenuFileOpenRecent(): Unit
}