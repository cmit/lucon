package ro.cmit.lucon.gui.tasks;

import java.beans.PropertyVetoException
import java.io.FileNotFoundException
import java.io.IOException

import org.apache.log4j.Logger
import org.apache.lucene.index.IndexReader

import IndexOpeningTask.logger
import ro.cmit.lucon.configuration.IndexConfigurator
import ro.cmit.lucon.configuration.IndexKey
import ro.cmit.lucon.gui.viewers.ConcViewer
import ro.cmit.lucon.gui.wizard.GuiKey

/** Uses a SwingWorker to perform a time-consuming (and utterly fake) task. */

class IndexOpeningTask(configuration: IndexConfigurator) extends ATask(configuration) {

  @throws[InterruptedException]
  override def run(): Object = {
    val progressBar = getProgressBar;
    if (progressBar != null) {
      progressBar.setIndeterminate(true);
      updateProgressbar(0, 100, "Opening...");
    }
    val cv = new ConcViewer(configurator);
    cv.setVisible(true); // necessary as of 1.3
    val mainFrame = configurator.get(GuiKey.MAIN_FRAME);
    mainFrame.desktop.add(cv);
    cv.addInternalFrameListener(mainFrame);
    try {
      cv.setSelected(true);
    } catch {
      case pve: java.beans.PropertyVetoException =>
        logger.error("Error occurred.", pve);
    }

    try {
      val indexContentDir = configurator.get(IndexKey.INDEX_CONTENT_DIR);
      val indexReader = IndexReader.open(indexContentDir);
      cv.openIndex(indexReader);
    } catch {
      case e1: FileNotFoundException =>
        logger.error("Error occurred.", e1);
      case e11: IOException =>
        logger.error("Error occurred.", e11);
    }
    if (progressBar != null) {
      getProgressBar.setIndeterminate(false);
      updateProgressbar(100, 100, "Finished");
    }
    try {
      cv.setMaximum(true);
    } catch {
      case e: PropertyVetoException =>
        logger.error("Error occurred.", e);
    }
    return null;
  }

  override def rollback(): Unit = {
    // nothing todo
  }

}
object IndexOpeningTask {
  val logger = Logger.getLogger(IndexOpeningTask.getClass);
}
