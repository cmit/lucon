package ro.cmit.lucon.gui.wizard.step;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.pietschy.wizard.InvalidStateException;
import org.pietschy.wizard.WizardModel;

import ro.cmit.lucon.configuration.IndexConfigurator;
import ro.cmit.lucon.gui.components.PickListModel;
import ro.cmit.lucon.gui.components.PickListPanel;
import ro.cmit.lucon.gui.tasks.ATask;
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import ChooseXmlFieldsStep._

class ChooseXmlFieldsStep(configuration: IndexConfigurator) 
  extends AIndexStep("Choose metadata xml elements.", "The metadata elements are indexed separately from the rest of the elements, but they can be searched for.", configuration) {
	private var task: ATask = null;

	private var pickList: PickListPanel[String] = null;

	override def isComplete() = true

	@throws[InvalidStateException] 
	override def applyState(): Unit = {
		setBusy(true);
		try {
			// conversie explicita de la List<Object> la List<String>
      for(xmlMetaElement <- pickList.getSelectedValues)
          configurator.addXmlMetaElement(xmlMetaElement);
		} catch {
		  case e: Exception =>
			  logger.error("Error occurred.", e);
		} finally {
			setBusy(false);
		}
	}

	override def abortBusy(): Unit = {
		super.abortBusy();
		if (task != null) {
			task.setTaskInterrupted(true);
		}

	}

	override def init(model: WizardModel): Unit = {
		// TODO: de implementat
	}

	override def prepare(): Unit = { // read the model and configure the panel
		val layout = new BorderLayout();
		this.setLayout(layout);
		// this.setPreferredSize(new Dimension(300, 200));
		val leftList = new ArrayList[String]();
		val xmlElements = configurator.getXmlElements;
		leftList.addAll(xmlElements.keySet());
		pickList = new PickListPanel[String]("Text elements", "Metadata elements", new PickListModel[String](leftList));
		this.add(pickList.peer, BorderLayout.NORTH);
	}

}

object ChooseXmlFieldsStep {
  val logger = Logger.getLogger(ChooseXmlFieldsStep.getClass);
}