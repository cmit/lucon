package ro.cmit.lucon.gui.wizard;

import java.awt.Component
import java.awt.Dimension

import scala.collection.JavaConversions.asScalaBuffer

import org.pietschy.wizard.Wizard
import org.pietschy.wizard.WizardModel
import org.pietschy.wizard.models.BranchingPath
import org.pietschy.wizard.models.Condition
import org.pietschy.wizard.models.MultiPathModel
import org.pietschy.wizard.models.SimplePath

import ro.cmit.lucon.configuration.IndexConfigurator
import ro.cmit.lucon.gui.wizard.step.AnalyseStep
import ro.cmit.lucon.gui.wizard.step.ChooseSaveLocationStep
import ro.cmit.lucon.gui.wizard.step.ChooseSourceStep
import ro.cmit.lucon.gui.wizard.step.ChooseXmlFieldsStep

class IndexWizard(configuration: IndexConfigurator) {

  // Construct each of the paths involved in the wizard.
  val analysePath = createAnalysePath();
  val xmlConfigPath = createXmlConfigPath();
  val lastPath = createLastPath();

  // Now bind all the paths together, first the branching path then the
  // optional path.

  // add the optional path and the condition that determines when it
  // should be followed
  analysePath.addBranch(xmlConfigPath, new Condition() {
    override def evaluate(model: WizardModel) = hasXmlFiles(configuration)
  });

  // add the end path and the condition that determines when it should be
  // followed
  analysePath.addBranch(lastPath, new Condition() {
    override def evaluate(model: WizardModel) = !hasXmlFiles(configuration)
  });

  // the optional path proceeds directly to the lastPath
  xmlConfigPath.setNextPath(lastPath);
  val model = new MultiPathModel(analysePath);
  private val wizard = new Wizard(model);
  wizard.setPreferredSize(new Dimension(410, 280));
  wizard.setDefaultExitMode(Wizard.EXIT_ON_FINISH);

  private def hasXmlFiles(configurator: IndexConfigurator): Boolean = {
    for (f <- configurator.getFiles)
      if (f.getName().toLowerCase().endsWith(".xml"))
        return true;
    return false;
  }

  private def createAnalysePath(): BranchingPath = {
    val analysePath = new BranchingPath();
    val chooseSourceStep = new ChooseSourceStep(configuration);
    analysePath.addStep(chooseSourceStep);
    val analyseStep = new AnalyseStep(configuration);
    analysePath.addStep(analyseStep);
    return analysePath;
  }

  private def createXmlConfigPath(): SimplePath = {
    val xmlConfigPath = new SimplePath();
    val xmlFieldsStep = new ChooseXmlFieldsStep(configuration);
    xmlConfigPath.addStep(xmlFieldsStep);
    return xmlConfigPath;
  }

  private def createLastPath(): SimplePath = {
    val lastPath = new SimplePath();
    val indexLocationStep = new ChooseSaveLocationStep(configuration);
    lastPath.addStep(indexLocationStep);
    return lastPath;
  }

  def show(title: String, parent: Component): Unit = {
    wizard.showInDialog(title, parent, true);
  }
}
