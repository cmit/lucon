package ro.cmit.lucon.gui.wizard;

import java.awt.Component
import java.awt.Dimension

import org.pietschy.wizard.Wizard
import org.pietschy.wizard.models.MultiPathModel
import org.pietschy.wizard.models.SimplePath

import ro.cmit.lucon.configuration.IndexConfigurator
import ro.cmit.lucon.gui.wizard.step.ChooseIndexStep;

class OpenIndexWizard(configuration: IndexConfigurator) {

  // Construct each of the paths involved in the wizard.
  private val openIndexPath = createOpenIndexPath();

  // the optional path proceeds directly to the lastPath
  private val model = new MultiPathModel(openIndexPath);
  private val wizard = new Wizard(model);
  wizard.setDefaultExitMode(Wizard.EXIT_ON_FINISH);
  wizard.setPreferredSize(new Dimension(410, 250));

  private def createOpenIndexPath(): SimplePath = {
    val xmlConfigPath = new SimplePath();
    xmlConfigPath.addStep(new ChooseIndexStep(configuration));
    return xmlConfigPath;
  }

  def show(title: String, parent: Component): Unit = {
    wizard.showInDialog(title, parent, true);
  }
}
