package ro.cmit.lucon.gui;

import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;

import ro.cmit.lucon.engine.Concordance;

import org.apache.log4j.Logger;

class LeftTablePaginator(_model: LeftTableSorter) extends AbstractTableModel 
  with TableModelListener with ChangeListener {
	private val nrRecordsOnPage = 200;

	private var nrRecordsTotal: Int = 0;

	private var nrPagesTotal: Int = 0;

	protected var model: LeftTableSorter = _model;
	model.addTableModelListener(this);

	private var spinnerModel: SpinnerNumberModel = null;
	private var label: JLabel = null;


	recalculateParameters();

	private def recalculateParameters() = {
		nrRecordsTotal = if (model == null) 0 else model.getRowCount();
		if (nrRecordsTotal == 0)
			nrPagesTotal = 1;
		else
			nrPagesTotal = (nrRecordsTotal / nrRecordsOnPage) + (if (nrRecordsTotal % nrRecordsOnPage == 0) 0 else 1);
		
		if (spinnerModel != null) {
			spinnerModel.setMaximum(nrPagesTotal);
		}
		if (label != null)
			label.setText(" of " + nrPagesTotal);
	}

	def getModel() = model

	// By default, implement TableModel by forwarding all messages
	// to the model.

	override def getRowCount(): Int = {
		if (model == null)
			return 0;
		if (nrRecordsTotal == 0)
			return 0;
		if (getNrCurrentPage() == nrPagesTotal)
			return nrRecordsTotal % nrRecordsOnPage;
		return nrRecordsOnPage;
	}
	
	private def getNrCurrentPage(): Int = spinnerModel.getNumber.intValue()

	override def getColumnCount(): Int = if(model == null) 0 else model.getColumnCount();


	override def getColumnName(aColumn: Int): String = model.getColumnName(aColumn)

	override def getColumnClass(aColumn: Int) = model.getColumnClass(aColumn)

	override def isCellEditable(row: Int, column: Int): Boolean = model.isCellEditable(row, column)

	override def tableChanged(e: TableModelEvent) = {
		recalculateParameters();
		fireTableDataChanged();
	}

	// The mapping only affects the contents of the data rows.
	// Pass all requests to these rows through the mapping array: "indexes".

	override def getValueAt(aRow: Int, aColumn: Int): Object = 
	  model.getValueAt((getNrCurrentPage() -1) * nrRecordsOnPage + aRow, aColumn)

	override def setValueAt(aValue: Object, aRow: Int, aColumn: Int) =
		model.setValueAt(aValue, (getNrCurrentPage() - 1) * nrRecordsOnPage + aRow, aColumn)

	def updateModel(concordance: Concordance) = model.updateModel(concordance)

	def get(row: Int): String = model.get((getNrCurrentPage() - 1) * nrRecordsOnPage + row)

	override def stateChanged(e: ChangeEvent) = {
		val source = e.getSource();
		if (source.isInstanceOf[JSpinner]) {
			fireTableDataChanged();
		}
	}

	def getNrPagesTotal() = nrPagesTotal

	def setSpinnerModel(spinnerModel: SpinnerNumberModel) = {
		this.spinnerModel = spinnerModel;
	}

	def setLabel(label: JLabel) = {
		this.label = label;
	}

}
