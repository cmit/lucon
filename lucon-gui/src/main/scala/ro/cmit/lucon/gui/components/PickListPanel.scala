package ro.cmit.lucon.gui.components

import scala.swing.ListView
import scala.swing.GridBagPanel
import scala.swing.GridPanel
import scala.swing.BorderPanel
import scala.swing.BorderPanel.Position._
import scala.swing.Label
import scala.swing.Button
import java.awt.Insets
import java.awt.Font
import java.awt.Dimension
import scala.swing.event.ButtonClicked
import scala.swing.event.Event
import scala.collection.JavaConverters._
import scala.swing.ScrollPane

case class PickListModelChanged[T](val pickListModel: PickListModel[T]) extends Event

protected class ListPanel[T](val label: Label, val list: List[T]) extends BorderPanel {
  val listView = new ListView[T]() {
    listData = list

    selection.intervalMode = ListView.IntervalMode.MultiInterval
    visibleRowCount = 7
    fixedCellHeight = 16
    fixedCellWidth = 136;
  }

  layout(label) = North
  layout(new ScrollPane(listView)) = Center
}

protected class ListButton(val l: String, val n: String) extends Button {
  text = l
  name = n
  margin = new Insets(1, 1, 1, 1)
  font = font.deriveFont(Font.BOLD, 9.0f)
  preferredSize = new Dimension(80, 20)
}

class PickListPanel[T](val leftLabel: String, val rightLabel: String, var pickListModel: PickListModel[T]) extends GridBagPanel {

  val leftPanel = new ListPanel[T](new Label(leftLabel), pickListModel.leftList)
  val rightPanel = new ListPanel[T](new Label(rightLabel), pickListModel.rightList)

  val middlePanel = new GridPanel(4, 1) {
    vGap = 3
    val l2rButton = new ListButton("Add >", "moveSelectedToRight")
    val lAll2rButton = new ListButton("Add all >>", "moveAllToRight")
    val r2lButton = new ListButton("< Remove", "moveSelectedToLeft")
    val rAll2lButton = new ListButton("<< Remove all", "moveAllToLeft")

    contents += l2rButton
    contents += lAll2rButton
    contents += r2lButton
    contents += rAll2lButton

    listenTo(l2rButton)
    listenTo(lAll2rButton)
    listenTo(r2lButton)
    listenTo(rAll2lButton)

    reactions += {
      case ButtonClicked(`l2rButton`)    => leftToRight
      case ButtonClicked(`lAll2rButton`) => allToRight
      case ButtonClicked(`r2lButton`)    => rightToLeft
      case ButtonClicked(`rAll2lButton`) => allToLeft
    }

    private def leftToRight = {
      val indexes = leftPanel.listView.selection.indices.toList
      publish(PickListModelChanged(pickListModel.moveLeftToRight(indexes)))
    }

    private def allToRight = publish(PickListModelChanged(pickListModel.moveAllToRight))

    private def rightToLeft = {
      val indexes = rightPanel.listView.selection.indices.toList
      publish(PickListModelChanged(pickListModel.moveRightToLeft(indexes)))
    }

    private def allToLeft = publish(PickListModelChanged(pickListModel.moveAllToLeft))
    
  }

  add(leftPanel, new Constraints { gridx = 0; gridy = 0; gridwidth = 2 })
  add(middlePanel, new Constraints { gridx = 2; gridy = 0; gridwidth = 1 })
  add(rightPanel, new Constraints { gridx = 3; gridy = 0; gridwidth = 2 })

  listenTo(middlePanel)

  reactions += {
    case e: PickListModelChanged[T] => {
      pickListModel = e.pickListModel
      leftPanel.listView.listData = pickListModel.leftList
      rightPanel.listView.listData = pickListModel.rightList
    }
  }

  def getSelectedValues: java.util.List[T] = pickListModel.rightList.asJava

}