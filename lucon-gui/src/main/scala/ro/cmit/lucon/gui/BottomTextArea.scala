/*
 * Created on 19.06.2005 by Catalin Mititelu
 */
package ro.cmit.lucon.gui

import java.awt.Font

import javax.swing.{JEditorPane, UIManager}
import javax.swing.event.{TreeSelectionEvent, TreeSelectionListener}
import javax.swing.tree.DefaultMutableTreeNode
import org.apache.log4j.Logger
import ro.cmit.lucon.engine.Context

import scala.util.Try

class BottomTextArea extends JEditorPane with TreeSelectionListener {
  private[this] val fontSize = UIManager.get("TextArea.font").asInstanceOf[Font].getSize

  setEditable(false)
  setContentType("text/html")
  setText(BottomTextArea.EmptyInnerHtml)

  def refresh(context: String): Unit = {
    resetContent

    var innerHtml = "<html><head></head><body style=\"font-size:" + fontSize + "pt\"><p>"
    innerHtml += context.replaceAll("\\n", "</p><p>")
    innerHtml += "</p></body></html>"

    setText(innerHtml)

    setCaretPosition(0)
  }

  private[this] def resetContent =
    Try {
      val doc = getDocument
      doc.remove(0, doc.getLength)
    }.recover {
      case ble =>
        BottomTextArea.logger.error("Error occurred.", ble)
    }

  override def valueChanged(e: TreeSelectionEvent): Unit = {
    resetContent

    val component = e.getPath.getLastPathComponent
    if (component != null) {
      val node = component.asInstanceOf[DefaultMutableTreeNode]
      if (node.isLeaf) {
        node.getUserObject match {
          case context: Context =>
            refresh(context.largeContext)
        }
      }
    }
  }

}

object BottomTextArea {
  val EmptyInnerHtml = "<html><head></head><body><p></p></body></html>"
  private val logger = Logger.getLogger(BottomTextArea.getClass)
}
