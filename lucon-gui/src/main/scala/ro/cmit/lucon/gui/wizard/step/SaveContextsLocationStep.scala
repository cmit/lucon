package ro.cmit.lucon.gui.wizard.step;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.apache.log4j.Logger;
import org.pietschy.wizard.InvalidStateException;
import org.pietschy.wizard.WizardModel;

import ro.cmit.lucon.configuration.IndexConfigurator;
import ro.cmit.lucon.engine.Context;
import ro.cmit.lucon.gui.tasks.ATask;
import ro.cmit.lucon.gui.tasks.SaveContextsTask;
import foxtrot.Worker;
import SaveContextsLocationStep._;

class SaveContextsLocationStep(configuration: IndexConfigurator) extends AIndexStep("Choose save location.", "", configuration)
  with ActionListener with DocumentListener {
    private var task: ATask = null;

    private var textField: JTextField = null;

    private var button: JButton = null;

    private var progressBar: JProgressBar = null;

    private val userDir = new File(System.getProperty("user.dir")).getAbsolutePath();

    private var contexts: List[Context] = null;

    private val fileChooser = new JFileChooser(new File(userDir));
    fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    fileChooser.setMultiSelectionEnabled(false);
    setLayout(new BorderLayout(3, 3));
    add(createPanel(), BorderLayout.NORTH);

    private def createPanel(): Component = {
        val panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        val label = createLabel("Save to file");
        panel.add(label);
        val destinationChooserPanel = createDestinationChooserPanel();
        panel.add(destinationChooserPanel);
        panel.add(createLabel(" "));
        progressBar = createProgressBar();
        panel.add(progressBar);
        return panel;
    }

    private def createProgressBar(): JProgressBar = {
        val progressBar = new JProgressBar(0, 100);
        progressBar.setStringPainted(true);
        progressBar.setAlignmentX(Component.LEFT_ALIGNMENT);
        return progressBar;
    }

    private def createLabel(str: String): JLabel = {
        val label = new JLabel(str);
        label.setAlignmentX(Component.LEFT_ALIGNMENT);
        return label;
    }

    private def createDestinationChooserPanel(): JPanel = {
        val panel = new JPanel(new BorderLayout());
        // Directory path text field
        textField = new JTextField();
        textField.getDocument().addDocumentListener(this);
        panel.add(textField, BorderLayout.CENTER);
        button = new JButton("Browse...");
        panel.add(button, BorderLayout.EAST);
        button.addActionListener(this);
        panel.setAlignmentX(Component.LEFT_ALIGNMENT);
        return panel;
    }

    override def actionPerformed(e: ActionEvent): Unit = {
        val source = e.getSource();
        if (source == button) {
            val returnVal = fileChooser.showOpenDialog(this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                val toDir = fileChooser.getSelectedFile();
                textField.setText(toDir.getAbsolutePath());
                setComplete(true);
            }
        }
    }

    override def isComplete(): Boolean = {
        val saveLocation = textField.getText().trim();
        if (saveLocation.length() == 0)
            return false;
        return true;
    }

    @throws[InvalidStateException] 
    override def applyState(): Unit = {
        var ok = false;
        setBusy(true);
        try {
            val saveLocation = textField.getText().trim();
            val location = new File(saveLocation);

            if (!location.exists())
                ok = true;
            else {
                val ret = JOptionPane
                        .showConfirmDialog(
                                this,
                                "This operation will overwrite the existing file. Are you sure you want to continue?",
                                "Existing file.", JOptionPane.YES_NO_OPTION,
                                JOptionPane.WARNING_MESSAGE);
                if (ret == JOptionPane.YES_OPTION)
                    ok = true;
                else
                    ok = false;
            }

            if (ok) {
                // do some work on another thread.. see Foxtrot
                task = new SaveContextsTask(contexts, location, configurator);
                task.setProgressBar(progressBar);
                Worker.post(task);
            }
        } catch {
          case e: Exception =>
            logger.error("Error occurred.", e);
        } finally {
            setBusy(false);
        }
        if (!ok) {
            throw new InvalidStateException(
                    "Unexpected error.\nPlease review the settings, especially save location.");
        }
    }

    override def abortBusy(): Unit = {
        super.abortBusy();
        if (task != null) {
            task.setTaskInterrupted(true);
        }

    }

    override def init(model: WizardModel): Unit = {
        // TODO: de implementat
    }

    override def prepare(): Unit = { // read the model and configure the panel
        // TODO: de implementat
    }

    override def changedUpdate(e: DocumentEvent): Unit = {
        if (e.getDocument().getLength() > 0)
            this.setComplete(true);
        else
            this.setComplete(false);
    }

    override def insertUpdate(e: DocumentEvent): Unit = {
        if (e.getDocument().getLength() > 0)
            this.setComplete(true);
        else
            this.setComplete(false);
    }

    override def removeUpdate(e: DocumentEvent): Unit = {
        if (e.getDocument().getLength() > 0)
            this.setComplete(true);
        else
            this.setComplete(false);
    }

    def setContexts(contexts: List[Context]): Unit = {
        this.contexts = contexts;
    }

}
object SaveContextsLocationStep {
  val logger = Logger.getLogger(SaveContextsLocationStep.getClass);
}