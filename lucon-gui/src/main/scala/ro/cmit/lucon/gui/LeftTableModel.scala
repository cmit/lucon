package ro.cmit.lucon.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import org.apache.log4j.Logger;
import org.apache.lucene.index.IndexReader;

import ro.cmit.lucon.engine.Concordance;
import ro.cmit.lucon.gui.viewers.ALuconViewer;
import scala.collection.JavaConversions._

class LeftTableModel(_concordance: Concordance) extends AbstractTableModel
  with TableModelListener with DocumentListener with ActionListener {

  private var concordance = _concordance

  private val columnNames = new Array[String](2)
  columnNames(0) = "Terms"
  columnNames(1) = "Frequency"

  private var terms = new ArrayList[String](1000)

  private var frequencies = new ArrayList[Int](1000)

  private var filterExpression = ""

  private val wordPattern = Pattern.compile("[\\p{L}]+", Pattern.UNICODE_CASE)

  private val timer = new Timer(2000, this);
  timer.setRepeats(false);
  updateModel(concordance);

  private var internalFrame: ALuconViewer = null;

  override def getColumnCount = columnNames.length

  override def getRowCount =
    if (terms == null) 0
    else terms.size

  override def getColumnName(col: Int) = columnNames(col)

  override def getValueAt(row: Int, col: Int): Object = {
    if (col == 0) terms.get(row)
    else if (col == 1) frequencies.get(row).asInstanceOf[Object]
    else null
  }

  /*
     * JTable uses this method to determine the default renderer/ editor for
     * each cell. If we didn't implement this method, then the last column would
     * contain text ("true"/"false"), rather than a check box.
     */
  override def getColumnClass(c: Int): Class[_] = getValueAt(0, c).getClass()

  def get(row: Int): String = {
    if (row < terms.size)
      terms.get(row)
    else null
  }

  def updateModel(concordance: Concordance) = {
    if (concordance != null) {
      this.concordance = concordance;
      reset();
    }
  }

  private def reset() = {
    terms = new ArrayList[String](1000)
    frequencies = new ArrayList[Int](1000)
  }

  // By default forward all events to all the listeners.
  override def tableChanged(e: TableModelEvent) = {
    fireTableChanged(e);
  }

  def setInternalFrame(jif: ALuconViewer) = {
    internalFrame = jif;
  }

  override def insertUpdate(e: DocumentEvent) = {
    if (timer.isRunning())
      timer.restart();
    else
      timer.start();
    updateFilter(e);
  }

  override def removeUpdate(e: DocumentEvent) = {
    if (timer.isRunning())
      timer.restart();
    else
      timer.start();
    updateFilter(e);
  }

  override def changedUpdate(e: DocumentEvent) = {
    if (timer.isRunning())
      timer.restart();
    else
      timer.start();
    updateFilter(e);
  }

  private def updateFilter(e: DocumentEvent) = {
    try {
      val d = e.getDocument();
      filterExpression = d.getText(0, d.getLength());
    } catch {
      case e: BadLocationException =>
        LeftTableModel.logger.error("Error occurred.", e);
    }
  }

  override def actionPerformed(evt: ActionEvent) = {
    val source = evt.getSource();
    if (source.isInstanceOf[Timer]) {
      if (timer.isRunning())
        timer.stop();
      if (internalFrame != null)
        internalFrame.blockMouseAndKeyboard();
      matchingSearch();
      if (internalFrame != null)
        internalFrame.freeMouseAndKeyboard();
    } else if (source.isInstanceOf[JTextField]) {
      if (timer.isRunning())
        timer.stop();
      if ("" != filterExpression) {
        val wordMatcher = wordPattern.matcher(filterExpression);
        if (wordMatcher.matches()) { // daca este cuvant
          // similaritati pe siruri
          if (internalFrame != null)
            internalFrame.blockMouseAndKeyboard();
          similaritySearch();
          if (internalFrame != null)
            internalFrame.freeMouseAndKeyboard();
        } else { // poate fi expresie regulata
          JOptionPane
            .showInternalMessageDialog(
              internalFrame,
              "Ati incercat cautarea aproximativa.\nPentru acest lucru sirul de caractere cautat nu trebuie sa fie expresie regulata.\nAdica NU trebuie sa contina meta-caractere: * , . [ ] { } etc.");
        }
      }
    }
  }

  private def matchingSearch() = {
    reset();
    fireTableRowsInserted(0, terms.size - 1);
    if ("" != filterExpression) {
      try {
        for (term <- concordance.matchingTerms(filterExpression)) {
          terms.add(term);
          frequencies.add(concordance.freq(term));
        }
      } catch {
        case e: Exception =>
          LeftTableModel.logger.error("Error occurred.", e);
      }
      fireTableRowsInserted(0, terms.size() - 1);
    }
  }

  private def similaritySearch() = {
    reset();
    if ("" != filterExpression) {
      try {
        for (term <- concordance.similarTerms(filterExpression)) {
          terms.add(term);
          frequencies.add(concordance.freq(term));
        }
      } catch {
        case e: Exception =>
          LeftTableModel.logger.error("Error occurred.", e);
      }
      fireTableRowsInserted(0, terms.length - 1);
    }
  }

  def getConcordance = concordance

}

object LeftTableModel {
  val logger = Logger.getLogger(LeftTableModel.getClass);
}