package ro.cmit.lucon.gui.wizard;

import java.awt.Component;
import java.awt.Dimension;
import java.util.List;

import org.pietschy.wizard.Wizard;
import org.pietschy.wizard.models.MultiPathModel;
import org.pietschy.wizard.models.SimplePath;

import ro.cmit.lucon.configuration.IndexConfigurator;
import ro.cmit.lucon.engine.Context;
import ro.cmit.lucon.gui.wizard.step.SaveContextsLocationStep;

class SaveContextsWizard(contexts: List[Context], val configuration: IndexConfigurator) {
  private var wizard: Wizard = null;

  // Construct each of the paths involved in the wizard.
  private val lastPath = createLastPath();

  // the optional path proceeds directly to the lastPath
  val model = new MultiPathModel(lastPath);
  wizard = new Wizard(model);
  wizard.setDefaultExitMode(Wizard.EXIT_ON_FINISH);
  wizard.setPreferredSize(new Dimension(410, 250));

  private def createLastPath(): SimplePath = {
    val lastPath = new SimplePath();
    val indexLocationStep = new SaveContextsLocationStep(configuration);
    indexLocationStep.setContexts(contexts);
    lastPath.addStep(indexLocationStep);
    return lastPath;
  }

  def show(title: String, parent: Component): Unit = {
    wizard.showInDialog(title, parent, true);
  }
}
