package ro.cmit.lucon.gui.wizard.step;

import java.awt.BorderLayout
import java.awt.Component
import java.awt.GridLayout
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.io.File

import org.apache.log4j.Logger
import org.pietschy.wizard.InvalidStateException
import org.pietschy.wizard.WizardModel

import ChooseSourceStep.logger
import foxtrot.Worker
import javax.swing.JButton
import javax.swing.JCheckBox
import javax.swing.JComboBox
import javax.swing.JFileChooser
import javax.swing.JPanel
import javax.swing.JProgressBar
import javax.swing.JTextField
import javax.swing.event.DocumentEvent
import javax.swing.event.DocumentListener
import ro.cmit.lucon.configuration.IndexConfigurator
import ro.cmit.lucon.gui.tasks.ComplexAnalizeTask

class ChooseSourceStep(configuration: IndexConfigurator) extends AIndexStep("Choose type and source for indexing", "", configuration)
  with ActionListener with DocumentListener {
  private var sourceCombo: JComboBox[String] = null;

  private var processDirectoryRecursively: JCheckBox = null;

  private var textField: JTextField = null;

  private var button: JButton = null;

  private var progressBar: JProgressBar = null;

  private var metadataDeepSearchJCheckBox: JCheckBox = null;

  private var files: Array[File] = null;

  private var task: ComplexAnalizeTask = null;

  private val fileChooser = new JFileChooser(new File(System.getProperty("user.dir")));
  setLayout(new BorderLayout(3, 3));
  add(createPanel(), BorderLayout.NORTH);

  private def createPanel(): Component = {
    val panel = new JPanel(new GridLayout(3, 1, 5, 5));
    panel.add(createSourceTypePanel());
    panel.add(createSourceChooserPanel());
    panel.add(createOptionPanel());
    return panel;
  }

  private def createSourceTypePanel(): Component = {
    val panel = new JPanel(new BorderLayout());
    sourceCombo = new JComboBox[String](Array[String]("Directory", "File"));
    sourceCombo.addActionListener(this);
    panel.add(sourceCombo, BorderLayout.NORTH);
    processDirectoryRecursively = createProcessDirectoryRecusivelyCheckBox();
    panel.add(processDirectoryRecursively, BorderLayout.WEST);
    fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    fileChooser.setMultiSelectionEnabled(false);
    return panel;
  }

  private def createSourceChooserPanel(): Component = {
    val panel = new JPanel(new BorderLayout());
    textField = new JTextField();
    textField.getDocument().addDocumentListener(this);
    textField.setEnabled(false);
    button = new JButton("Browse...");
    progressBar = new JProgressBar(0, 100);
    progressBar.setStringPainted(true);
    panel.add(textField, BorderLayout.CENTER);
    panel.add(button, BorderLayout.EAST);
    panel.add(progressBar, BorderLayout.SOUTH);
    button.addActionListener(this);
    return panel;
  }

  private def createOptionPanel(): Component = {
    val panel = new JPanel(new BorderLayout());
    metadataDeepSearchJCheckBox = new JCheckBox("Deep search for metadata elements (very slow)");
    metadataDeepSearchJCheckBox.setSelected(false);
    panel.add(metadataDeepSearchJCheckBox, BorderLayout.CENTER);
    return panel;
  }

  private def createProcessDirectoryRecusivelyCheckBox(): JCheckBox = {
    val jCheckBox = new JCheckBox(
      "Process directory recursively");
    jCheckBox.setSelected(true);
    jCheckBox.setAlignmentX(Component.LEFT_ALIGNMENT);
    jCheckBox.addActionListener(this);
    return jCheckBox;
  }

  override def actionPerformed(e: ActionEvent): Unit = {
    if (e.getSource().isInstanceOf[JComboBox[String]]) {
      val cb: JComboBox[String] = e.getSource().asInstanceOf[JComboBox[String]];
      val sourceType = cb.getSelectedItem().asInstanceOf[String];
      if (sourceType.equalsIgnoreCase("file")) {
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.setMultiSelectionEnabled(true);
        processDirectoryRecursively.setEnabled(false);
      } else if (sourceType.equalsIgnoreCase("directory")) {
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fileChooser.setMultiSelectionEnabled(false);
        processDirectoryRecursively.setEnabled(true);
      }
      reset();
    } else if (e.getSource().isInstanceOf[JButton]) {
      val sourceType = sourceCombo.getSelectedItem().asInstanceOf[String];
      val returnVal = fileChooser.showOpenDialog(this);
      if (returnVal == JFileChooser.APPROVE_OPTION) {
        files = new Array[File](0);
        var str = "";
        if (sourceType.equalsIgnoreCase("file")) {
          files = fileChooser.getSelectedFiles();
          if (files == null || files.length == 0) {
            files = new Array[File](1);
            files(0) = fileChooser.getSelectedFile();
          }
        } else if (sourceType.equalsIgnoreCase("directory")) {
          files = new Array[File](1);
          files(0) = fileChooser.getSelectedFile();
        }
        for (file <- files)
          str = str + file.getAbsolutePath() + " ";
        textField.setText(str);
        setComplete(true);
      }
    }
  }

  override def isComplete(): Boolean = {
    if (textField.getText().trim().length() > 0
      && (files != null && files.length > 0))
      return true;
    return false;
  }

  @throws[InvalidStateException]
  override def applyState(): Unit = {
    configurator.init();
    setBusy(true);
    try {
      // do some work on another thread.. see Foxtrot
      task = new ComplexAnalizeTask(configurator);
      task.setProgressBar(progressBar);
      task.setStartingFiles(files);
      val processRecursively = processDirectoryRecursively.isEnabled() && processDirectoryRecursively.isSelected();
      task.setProcessRecursively(processRecursively);
      val metadataDeepSearch = metadataDeepSearchJCheckBox.isEnabled() && metadataDeepSearchJCheckBox.isSelected();
      task.setMetadataDeepSearch(metadataDeepSearch);
      Worker.post(task);
    } catch {
      case e: Exception =>
        files = null;
        logger.error("Error occurred.", e);
    } finally {
      setBusy(false);
    }
  }

  override def abortBusy(): Unit = {
    super.abortBusy();
    if (task != null) {
      task.setTaskInterrupted(true);
    }

  }

  def getFiles(): Array[File] = {
    return files;
  }

  private def reset(): Unit = {
    textField.setText("");
    files = null;
  }

  override def init(model: WizardModel): Unit = {
    // TODO: de implementat
  }

  override def prepare(): Unit = { // read the model and configure the panel
    // TODO: de implementat
  }

  override def changedUpdate(e: DocumentEvent): Unit = {
    if (isComplete())
      this.setComplete(true);
    else
      this.setComplete(false);
  }

  override def insertUpdate(e: DocumentEvent): Unit = {
    if (isComplete())
      this.setComplete(true);
    else
      this.setComplete(false);
  }

  override def removeUpdate(e: DocumentEvent): Unit = {
    if (isComplete())
      this.setComplete(true);
    else
      this.setComplete(false);
  }

}

object ChooseSourceStep {
  val logger = Logger.getLogger(ChooseSourceStep.getClass);
}
