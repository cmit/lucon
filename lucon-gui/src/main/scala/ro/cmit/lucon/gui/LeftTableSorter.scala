/**
 * A sorter for TableModels. The sorter has a model (conforming to TableModel) 
 * and itself implements TableModel. TableSorter does not store or copy 
 * the data in the TableModel, instead it maintains an array of 
 * integers which it keeps the same size as the number of rows in its 
 * model. When the model changes it notifies the sorter that something 
 * has changed eg. "rowsAdded" so that its internal array of integers 
 * can be reallocated. As requests are made of the sorter (like 
 * getValueAt(row, col) it redirects them to its model via the mapping 
 * array. That way the TableSorter appears to hold another copy of the table 
 * with the rows in a different order. The sorting algorthm used is stable 
 * which means that it does not move around rows when its comparison 
 * function returns 0 to denote that they are equivalent. 
 */
package ro.cmit.lucon.gui;

import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import org.apache.log4j.Logger;

import ro.cmit.lucon.engine.Concordance;

class LeftTableSorter(_model: LeftTableModel) extends AbstractTableModel
		with TableModelListener {
	var indexes = Array[Int]()

	var sortingColumns = List[Int]();

	var ascending = true;

	var compares: Int = 0;

	protected var model = _model;
	reallocateIndexes();
	model.addTableModelListener(this);

	def getModel() = model

	// By default, implement TableModel by forwarding all messages
	// to the model.

	override def getRowCount(): Int = if (model == null)  0 else model.getRowCount

	override def getColumnCount(): Int = if (model == null) 0 else model.getColumnCount

	override def getColumnName(aColumn: Int): String = model.getColumnName(aColumn)

	override def getColumnClass(aColumn: Int) = model.getColumnClass(aColumn)

	override def isCellEditable(row: Int, column: Int): Boolean = model.isCellEditable(row, column)

	def compareRowsByColumn(row1: Int, row2: Int, column: Int): Int = {
		val colClass = model.getColumnClass(column);
		val data = model;

		// Check for nulls.
		val o1 = data.getValueAt(row1, column);
		val o2 = data.getValueAt(row2, column);

		// If both values are null, return 0.
		if (o1 == null && o2 == null) {
			return 0;
		} else if (o1 == null) { // Define null less than everything.
			return -1;
		} else if (o2 == null) {
			return 1;
		}

		/*
		 * We copy all returned values from the getValue call in case an
		 * optimised model is reusing one object to return many values. The
		 * Number subclasses in the JDK are immutable and so will not be used in
		 * this way but other subclasses of Number might want to do this to save
		 * space and avoid unnecessary heap allocation.
		 */

		if (colClass.getSuperclass() == classOf[java.lang.Number]) {
			val n1 = data.getValueAt(row1, column).asInstanceOf[java.lang.Number];
			val d1 = n1.doubleValue();
			val n2 = data.getValueAt(row2, column).asInstanceOf[java.lang.Number];
			val d2 = n2.doubleValue();

			if (d1 < d2) {
				return -1;
			} else if (d1 > d2) {
				return 1;
			} else {
				return 0;
			}
		} else if (colClass == classOf[java.util.Date]) {
			val d1 = data.getValueAt(row1, column).asInstanceOf[java.util.Date];
			val n1 = d1.getTime();
			val d2 = data.getValueAt(row2, column).asInstanceOf[java.util.Date];
			val n2 = d2.getTime();

			if (n1 < n2) {
				return -1;
			} else if (n1 > n2) {
				return 1;
			} else {
				return 0;
			}
		} else if (colClass == classOf[String]) {
			val s1 = data.getValueAt(row1, column).asInstanceOf[String];
			val s2 = data.getValueAt(row2, column).asInstanceOf[String];
			val result = s1.compareTo(s2);

			if (result < 0) {
				return -1;
			} else if (result > 0) {
				return 1;
			} else {
				return 0;
			}
		} else if (colClass == classOf[Boolean]) {
			val bool1 = data.getValueAt(row1, column).asInstanceOf[Boolean];
			val b1 = bool1.booleanValue();
			val bool2 = data.getValueAt(row2, column).asInstanceOf[Boolean];
			val b2 = bool2.booleanValue();

			if (b1 == b2) {
				return 0;
			} else if (b1) { // Define false < true
				return 1;
			} else {
				return -1;
			}
		} else {
			val v1 = data.getValueAt(row1, column);
			val s1 = v1.toString();
			val v2 = data.getValueAt(row2, column);
			val s2 = v2.toString();
			val result = s1.compareTo(s2);

			if (result < 0) {
				return -1;
			} else if (result > 0) {
				return 1;
			} else {
				return 0;
			}
		}
	}

	def compare(row1: Int, row2: Int): Int = {
		compares = compares + 1;
		for (column <- sortingColumns) {
			val result = compareRowsByColumn(row1, row2, column);
			if (result != 0) {
				return if(ascending) result else -result;
			}
		}
		return 0;
	}

	def reallocateIndexes() = {
		val rowCount = model.getRowCount();

		// Set up a new array of indexes with the right number of elements
		// for the new data model.
		// Initialise with the identity mapping.
		indexes = (0 until rowCount).toArray;
	}

	override def tableChanged(e: TableModelEvent) = {
		reallocateIndexes();
		fireTableDataChanged();
	}

	def checkModel() = {
		if (indexes.length != model.getRowCount()) {
			LeftTableSorter.logger.error("Sorter not informed of a change in model.");
		}
	}

	def sort(sender: Object) = {
		checkModel();
		compares = 0;
		indexes = indexes.sortWith((row1: Int, row2: Int) => compare(row1, row2) > 0);
	}

	// The mapping only affects the contents of the data rows.
	// Pass all requests to these rows through the mapping array: "indexes".

	override def getValueAt(aRow: Int, aColumn: Int) = {
		checkModel();
		model.getValueAt(indexes(aRow), aColumn);
	}

	override def setValueAt(aValue: Object, aRow: Int, aColumn: Int) = {
		checkModel();
		model.setValueAt(aValue, indexes(aRow), aColumn);
	}

	def sortByColumn(column: Int): Unit = sortByColumn(column, true);

	def sortByColumn(column: Int, ascending: Boolean): Unit = {
		this.ascending = ascending;
		sortingColumns = List[Int](column);
		sort(this);
		fireTableDataChanged();
	}

	// There is no-where else to put this.
	// Add a mouse listener to the Table to trigger a table sort
	// when a column heading is clicked in the JTable.
	def addMouseListenerToHeaderInTable(table: JTable ) = {
		val sorter = this;
		val tableView = table;
		tableView.setColumnSelectionAllowed(false);
		val listMouseListener = new MouseAdapter() {
			override def mouseClicked(e: MouseEvent) = {
				val columnModel = tableView.getColumnModel();
				val viewColumn = columnModel.getColumnIndexAtX(e.getX());
				val column = tableView.convertColumnIndexToModel(viewColumn);
				if (e.getClickCount() == 1 && column != -1) {
					sorter.sortByColumn(column, !ascending);
				}
			}
		};
		val th = tableView.getTableHeader();
		th.addMouseListener(listMouseListener);
	}

	def updateModel(concordance: Concordance ) = {
		model.updateModel(concordance);
	}

	def get(row: Int): String = model.get(indexes(row));
}

object LeftTableSorter {
  	val logger = Logger.getLogger(LeftTableSorter.getClass);
}