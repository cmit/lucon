package ro.cmit.lucon.gui.components

import scala.collection.JavaConverters.asScalaBufferConverter
import scala.util.Try

class PickListModel[T](val leftList: List[T], val rightList: List[T]) {

  def this(list: List[T]) = this(list, Nil)

  /* Necessary for Java compatibility */
  def this(leftList: java.util.List[T], rightList: java.util.List[T]) = this(leftList.asScala.toList, rightList.asScala.toList)
  def this(list: java.util.List[T]) = this(list.asScala.toList)

  def moveLeftToRight(indexes: List[Int]) = {
    val l = leftList.zipWithIndex.partition(p => indexes.contains(p._2))
    new PickListModel(l._2.map(_._1), rightList ++ l._1.map(_._1))
  }

  def moveRightToLeft(indexes: List[Int]) = {
    val l = rightList.zipWithIndex.partition(p => indexes.contains(p._2))
    new PickListModel(leftList ++ l._1.map(_._1), l._2.map(_._1))
  }

  def moveAllToRight = new PickListModel(Nil, leftList ++ rightList)

  def moveAllToLeft = new PickListModel(leftList ++ rightList)

  override def toString = "L: " + leftList + "; R: " + rightList
}
