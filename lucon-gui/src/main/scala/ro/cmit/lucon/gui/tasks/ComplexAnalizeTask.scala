package ro.cmit.lucon.gui.tasks

import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.IOException
import java.io.InputStreamReader
import java.io.UnsupportedEncodingException
import java.util.LinkedList
import java.util.Vector
import java.util.regex.Pattern

import scala.collection.JavaConversions.seqAsJavaList

import ro.cmit.lucon.analyzers.TokenType
import ro.cmit.lucon.analyzers.xml.XmlTokenizer
import ro.cmit.lucon.configuration.IndexConfigurator

/** Uses a SwingWorker to perform a time-consuming (and utterly fake) task. */

class ComplexAnalizeTask(override val configurator: IndexConfigurator) extends ATask(configurator) {
  private var startingFiles: Array[File] = null

  private var processRecursively: Boolean = false

  private var metadataDeepSearch: Boolean = false

  private val endNamePattern = Pattern.compile("^<\\/\\s*([^<>\\s\\/]+)\\s*>", Pattern.UNICODE_CASE)

  @throws[InterruptedException]
  @throws[UnsupportedEncodingException]
  @throws[FileNotFoundException]
  @throws[IOException]
  override def run(): Object = {
    val queue = new LinkedList[File]
    queue.addAll(startingFiles.toSeq)

    val files = new Vector[File](1, 1)
    var taskLength = files.size() + queue.size()
    updateProgressbar(0, taskLength, "0/" + taskLength)
    var j = 0
    var justStarted = true
    while (!queue.isEmpty()) {
      val f = queue.poll()
      if (f.isFile()) {
        files.add(f)
        configurator.addFile(f)
        if (f.getName().toLowerCase().endsWith(".xml")) {
          if (metadataDeepSearch) {
            // parcurg fisierul folosind XMLTokenizer
            val fis = new FileInputStream(f)
            val isr = new InputStreamReader(fis, "UTF-8")
            val xmlTokenizer = new XmlTokenizer(isr)
            var t = xmlTokenizer.next()
            while (t != null) {
              if (TokenType.EndElement.isTokenType(t)) {
                val matcher = endNamePattern.matcher(t
                  .termText())
                if (matcher.find()) {
                  val elementName = matcher.group(1)
                  configurator.addXmlElement(elementName)
                }
              }
              t = xmlTokenizer.next()
            }
            isr.close()
            fis.close()
          }
        }
        j = j + 1
      } else {
        if (!".svn".equalsIgnoreCase(f.getName()) && (justStarted || processRecursively)) {
          justStarted = false
          val fileList = f.listFiles()
          queue.addAll(fileList.toSeq)
        }
      }
      taskLength = files.size() + queue.size()
      updateProgressbar(j, taskLength, j + "/" + taskLength)
      if (isTaskInterrupted) {
        updateProgressbar(j, taskLength, j + "Stopped")
        return null
      }
    }
    updateProgressbar(taskLength, taskLength, "100%")
    return null
  }

  def getStartingFiles() = startingFiles

  def setStartingFiles(startingFiles: Array[File]) = this.startingFiles = startingFiles

  def setProcessRecursively(processRecursively: Boolean) = this.processRecursively = processRecursively

  def setMetadataDeepSearch(metadataDeepSearch: Boolean) = this.metadataDeepSearch = metadataDeepSearch

  override def rollback() = {}

}
