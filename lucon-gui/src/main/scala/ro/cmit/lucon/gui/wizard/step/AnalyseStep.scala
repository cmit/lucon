package ro.cmit.lucon.gui.wizard.step;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.apache.log4j.Logger;
import org.pietschy.wizard.InvalidStateException;

import ro.cmit.lucon.configuration.IndexConfigurator;

class AnalyseStep(configurator: IndexConfigurator) extends AIndexStep("Analysis report", "", configurator) {
  private var label1: JLabel = null
  private var label2: JLabel = null

  private val logger = Logger.getLogger(this.getClass);

  setLayout(new BorderLayout());
  add(createPanel(), BorderLayout.CENTER);

  private def createPanel(): Component = {
    val panel = new JPanel();
    label1 = new JLabel("");
    label2 = new JLabel("");
    panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
    panel.add(label1);
    panel.add(label2);
    return panel;
  }

  override def isComplete() = true

  override def prepare() = {
    if (configurator.hasFiles) {
      label1.setText("There are " + configurator.getFiles.size + " files.")
      label2.setText("About " + configurator.getSizeOfFiles() + " bytes to process.")
    } else {
      label1.setText("There is no file to process.")
      label2.setText("")
    }
  }

  @throws[InvalidStateException]
  override def applyState(): Unit = {
    setBusy(true);
    try {
      // indexare propriu-zisa
    } catch {
      case e: Exception => logger.error("Error occurred.", e);
    } finally {
      setBusy(false);
    }
  }

}
