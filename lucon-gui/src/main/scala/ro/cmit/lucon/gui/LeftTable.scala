/*
 * Created on 29.05.2005 by Catalin Mititelu
 */
package ro.cmit.lucon.gui;

import java.util.ArrayList;
import java.util.List;
import java.util.Observer;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import ro.cmit.lucon.configuration.IndexKey;
import ro.cmit.lucon.engine.Concordance;
import ro.cmit.lucon.engine.TermConcordance;
import ro.cmit.lucon.gui.exception.LuconException;
import ro.cmit.lucon.gui.viewers.ALuconViewer;
import ro.cmit.lucon.gui.wizard.GuiKey

class LeftTable(concordance: Concordance) extends JTable {
    // table models
    private val tableModel = new LeftTableModel(concordance);
    private val tableSorter = new LeftTableSorter(tableModel);
    private val tablePaginator = new LeftTablePaginator(tableSorter);
    private val errorObservable = new ErrorObservable();
    private var viewer: ALuconViewer = null;

    // private TopTable topTable;
    private var topTree: TopTree = null;

    setModel(tablePaginator);
    setColumnSelectionAllowed(false);
    setRowSelectionAllowed(true);
    val fontScale = MainFrame.properties.get(GuiKey.FONT_SCALE)
    setRowHeight(getRowHeight + fontScale.toInt)
    val rowSM: ListSelectionModel = getSelectionModel();
    rowSM.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    rowSM.addListSelectionListener(new LeftTableSelectionListener());

    tableSorter.addMouseListenerToHeaderInTable(this);

    def updateModel(concordance: Concordance) = tablePaginator.updateModel(concordance)

    def sort() = tableSorter.sort(this)

    def getFilter() = tableModel

    def setTopTree(tt: TopTree ) = topTree = tt

    def setViewer(viewer: ALuconViewer ) = this.viewer = viewer

    class LeftTableSelectionListener extends ListSelectionListener {
        override def valueChanged(e: ListSelectionEvent): Unit = {
            if (e.getValueIsAdjusting())
                return;
            try {
                val lsm = e.getSource().asInstanceOf[ListSelectionModel];
                if (!lsm.isSelectionEmpty()) {
                    if (viewer != null)
                        viewer.blockMouseAndKeyboard();
                    val minSelectedRow = lsm.getMinSelectionIndex();
                    val maxSelectedRow = lsm.getMaxSelectionIndex();
                    val selectedTerms =
                        (for (i <- minSelectedRow to maxSelectedRow if (lsm.isSelectedIndex(i)))
                            yield tablePaginator.get(i))
                        .toList
                    val leftContextSize = viewer.configurator.getDefault(IndexKey.LEFT_CONTEXT);
                    val rightContextSize = viewer.configurator.getDefault(IndexKey.RIGHT_CONTEXT);
                    val termConcordance = tableModel.getConcordance.contexts(selectedTerms, leftContextSize, rightContextSize);
                    topTree.updateModel(termConcordance);
                }
            } catch {
              case e1: RuntimeException =>
                // catch any runtime exception as OutOfMemory and notify for
                // error
                errorObservable
                        .reportError(new LuconException(e1),
                                "Sorry, an error occured. Maybe, not all contexts are loaded.");
              case e2: Throwable =>
                // catch any runtime exception as OutOfMemory and notify for
                // error
                errorObservable
                        .reportError(new LuconException(e2),
                                "Sorry, an error occured. Maybe, not all contexts are loaded.");
            } finally {
                if (viewer != null)
                    viewer.freeMouseAndKeyboard();
            }
        }
    }

    def getTablePaginator() = tablePaginator

    def addErrorObserver(o: Observer) = errorObservable.addObserver(o)
}
