package ro.cmit.lucon.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.log4j.Logger;
import org.apache.lucene.search.BooleanQuery;

import ro.cmit.lucon.configuration.IndexConfigurator;
import ro.cmit.lucon.configuration.IndexKey;
import ro.cmit.lucon.engine.Concordance;
import ro.cmit.lucon.engine.FieldUtils;
import ro.cmit.lucon.gui.viewers.ConcViewer;
import ro.cmit.lucon.gui.viewers.ConcViewer2;
import AdvancedSearchDialog._;
import java.util.Map
import java.util.Set
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import javax.swing.WindowConstants._
import java.util.HashMap

class AdvancedSearchDialog(aFrame: AbstractMainFrame) extends JDialog(aFrame, true) with ActionListener {
    private var optionPane: JOptionPane = null;

    private var distanceField: JTextField = null;

    private var fields: Set[String] = null;

    private val frame: AbstractMainFrame = aFrame;

    private var westPanel: JPanel = null
    private var centerPanel: JPanel = null
    private var eastPanel: JPanel = null;

    private var concordance: Concordance = null;

    private var obj4Criterion: Map[Int, Map[JComboBox[_], JTextField]] = null;
    private var configurator: IndexConfigurator = null;

        val internalFrame = frame.desktop.getSelectedFrame();
        if (internalFrame.isInstanceOf[ConcViewer]) {
            val cv = internalFrame.asInstanceOf[ConcViewer];
            configurator = cv.configurator;
            concordance = cv.getConcordance();
        } else if (internalFrame.isInstanceOf[ConcViewer2]) {
            val cv = internalFrame.asInstanceOf[ConcViewer2];
            concordance = cv.getConcordance();
            configurator = cv.configurator;
        }

        try {
            val altConfigurator = new IndexConfigurator();
            altConfigurator.load(configurator.get(IndexKey.INDEX_FILE));
            fields = new HashSet[String]();
            /* Adaug atributele */
            for (attr <- altConfigurator.getAttributes)
                fields.add(FieldUtils.PREFIX_ATTRIBUTE + attr);

        } catch {
          case e1: IOException =>
            logger.error("Error occurred.", e1);
        }
        fields.add("text");

        setTitle("Advanced search");

        westPanel = new JPanel(createGridLayout(0, 1));
        centerPanel = new JPanel(createGridLayout(0, 2));
        eastPanel = new JPanel(createGridLayout(0, 1));

        val options = Array(btnString1, btnString2);

        optionPane = new JOptionPane(createGroupElements(),
                JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION, null,
                options.asInstanceOf[Array[Object]], options(0));
        setContentPane(optionPane);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            override def windowClosing(we: WindowEvent): Unit = {
                /*
                 * Instead of directly closing the window, we're going to change
                 * the JOptionPane's value property.
                 */
                optionPane.setValue(new Integer(JOptionPane.CLOSED_OPTION));
            }
        });
        setLocationRelativeTo(aFrame);

        optionPane.addPropertyChangeListener(new PropertyChangeListener() {
            override def propertyChange(e: PropertyChangeEvent): Unit = {
                val prop = e.getPropertyName();

                if (isVisible()
                        && (e.getSource() == optionPane)
                        && (prop.equals(JOptionPane.VALUE_PROPERTY) || prop
                                .equals(JOptionPane.INPUT_VALUE_PROPERTY))) {
                    val value = optionPane.getValue();

                    if (value == JOptionPane.UNINITIALIZED_VALUE) {
                        // ignore reset
                        return;
                    }

                    // Reset the JOptionPane's value.
                    // If you don't do this, then if the user
                    // presses the same button next time, no
                    // property change event will be fired.
                    optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

                    if (value.equals(btnString1)) {
                        /* Aici se poate face validare */

                        setVisible(false);

                        // thread nou pentru cautare
                        val worker = new SwingWorker() {
                            def construct(): Object = {
                                startSearch();
                                return null;
                                // return new ActualTask();
                            }
                        };
                        worker.start();

                    } else { // user closed dialog or clicked cancel
                        setVisible(false);
                    }
                }
            }
        });

    private def createGroupElements(): JPanel = {
        val nrGroups = 1;
        val panel = new JPanel(createBorderLayout());

        panel.add(westPanel, BorderLayout.WEST);
        panel.add(centerPanel, BorderLayout.CENTER);
        panel.add(eastPanel, BorderLayout.EAST);

        if (obj4Criterion == null)
            obj4Criterion = new HashMap[Int, Map[JComboBox[_], JTextField]]();

        distanceField = new JTextField(10);
        distanceField.addActionListener(new ActionListener() {
            override def actionPerformed(e: ActionEvent): Unit = {
                optionPane.setValue(btnString1);
            }
        });

        val wordGroupButton = new JButton("<< Add");
        wordGroupButton.addActionListener(this);
        wordGroupButton.setName(wgbName);

        for (i <- 0 until nrGroups)
            createWordGroup();

        addInnerPanel(new Label("Distance:"), distanceField, new JLabel(""),
                wordGroupButton);

        return panel;
    }

    private def createWordGroup(): Unit = {
        val nr = obj4Criterion.keySet().size() + 1;
        val key = "Word " + nr;
        val label = new JLabel(key);
        val list = new JComboBox(fields.toArray());
        val text = new JTextField();
        text.setName(textName);
        text.addActionListener(this);

        addInnerPanel(label, list, text, new JLabel(""), nr - 1);

        var value = obj4Criterion.get(key);
        if (value == null) {
            value = new HashMap[JComboBox[_], JTextField]();
            obj4Criterion.put(nr, value);
        }
        value.put(list, text);

        distanceField.setText("" + countContentFields());
    }

    private def countContentFields(): Int = {
        var nr = 0;
        val keys = new ArrayList[Int](obj4Criterion.keySet());
        for (i <- 0 until keys.size()) {
            val j = keys.get(i);
            val h = obj4Criterion.get(j);
            val hKeys = h.keySet();
            for (cb <- hKeys) {
                val field: String = cb.getSelectedItem().asInstanceOf[String];
                if (field.startsWith(FieldUtils.PREFIX_ATTRIBUTE)
                        || field.equals("text"))
                    nr = nr + 1;
            }
        }
        return nr;
    }

    private def addInnerPanel(o1: Component, o2: Component , o3: Component, o4: Component): Unit = {
        // o1 se duce pe westPanel
        westPanel.add(o1);

        // o2, o3 se duc pe centerPanel
        centerPanel.add(o2);
        centerPanel.add(o3);

        // 04 se duce pe eastPanel
        eastPanel.add(o4);
    }

    private def addInnerPanel(o1: Component , o2: Component, o3: Component, o4: Component, poz: Int): Unit = {
        // o1 se duce pe westPanel
        westPanel.add(o1, poz);

        // o2, o3 se duc pe centerPanel
        centerPanel.add(o2, 2 * poz);
        centerPanel.add(o3, 2 * poz + 1);

        // 04 se duce pe eastPanel
        eastPanel.add(o4, poz);
    }

    /* Creeaza un layout tip GridLayout folosit general in acest dialog */
    private def createGridLayout(r: Int, c: Int): GridLayout = {
        val layout = new GridLayout(r, c);
        layout.setVgap(5);
        layout.setHgap(5);
        return layout;
    }

    /* Creeaza un layout tip BorderLayout folosit general in acest dialog */
    private def createBorderLayout(): BorderLayout = {
        return new BorderLayout();
    }

    override def actionPerformed(e: ActionEvent): Unit = {
        val source = e.getSource().asInstanceOf[Component];
        val sourceName = source.getName();
        if (sourceName.compareTo(wgbName) == 0) {
            createWordGroup();
            this.pack();
            optionPane.validate();
        } else if (sourceName.compareTo(textName) == 0) {
            optionPane.setValue(btnString1);
        }

    }

    /*
     * private String transformToMatchIcircAcirc(String val) { String expr = new
     * String(val); Pattern p = Pattern.compile("[\\xe2\\xc2\\xee\\xce]",
     * Pattern.UNICODE_CASE); Matcher m = p.matcher(val); expr =
     * m.replaceAll("[\u00e2\u00c2\u00ee\u00ce]"); return expr; }
     */

    private def startSearch(): Unit = {
        val keys = new ArrayList[Int](obj4Criterion.keySet()).sorted;
        val regexTerms = new ArrayList[String]();
        BooleanQuery.setMaxClauseCount(Integer.MAX_VALUE);
        for (i <- 0 until keys.size()) {
            val j = keys.get(i);
            val h = obj4Criterion.get(j);
            val hKeys = h.keySet();
            for (cb <- hKeys) {
                val tf = h.get(cb);
                val field = cb.getSelectedItem().asInstanceOf[String];
                val v = tf.getText();
                if (field.startsWith(FieldUtils.PREFIX_ATTRIBUTE)
                        || field.equals("text")) {
                    val term =
                        if (field.startsWith(FieldUtils.PREFIX_ATTRIBUTE))
                            new String(field + "=\"" + v + "\";FULL")
                                .toLowerCase();
                        else
                            v.toLowerCase();
                    regexTerms.add(term);
                }
            }
        }

        val distance = Integer.parseInt(distanceField.getText());
        val cv2 = new ConcViewer2(configurator, concordance,
                regexTerms.asScala.toList, distance);
        frame.desktop.add(cv2);
        cv2.addInternalFrameListener(frame);
        cv2.setVisible(true);
    }
}

object AdvancedSearchDialog {
    val btnString1 = "Search";
    val btnString2 = "Cancel";
    val wgbName = "wordGroupButton";
    val textName = "text";
    val logger = Logger.getLogger(AdvancedSearchDialog.getClass);
}
