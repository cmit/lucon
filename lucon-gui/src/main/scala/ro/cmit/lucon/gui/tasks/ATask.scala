package ro.cmit.lucon.gui.tasks

import foxtrot.Task
import javax.swing.JProgressBar
import javax.swing.SwingUtilities
import ro.cmit.lucon.configuration.IndexConfigurator

abstract class ATask(val configurator: IndexConfigurator) extends Task {
  protected var progressBar: JProgressBar = null

  protected var running: Boolean = false

  protected var taskInterrupted: Boolean = false

  protected var finished: Boolean = false

  def updateProgressbar(index: Int, max: Int, message: String) = {
    SwingUtilities.invokeLater(new Runnable() {
      override def run() = {
        if (progressBar != null) {
          progressBar.setMaximum(max)
          progressBar.setValue(index)
          progressBar.setString(message)
        }
      }
    })
  }

  protected def useIndeterminateProgressbar =
    if (progressBar != null)
      progressBar.setIndeterminate(true)

  protected def useDeterminateProgressbar =
    if (progressBar != null)
      progressBar.setIndeterminate(false)

  def isTaskInterrupted: Boolean = synchronized { taskInterrupted }

  def setTaskInterrupted(value: Boolean) = synchronized {
    taskInterrupted = value
    if (value)
      rollback
  }

  def getProgressBar = progressBar

  def setProgressBar(progressBar: JProgressBar) = this.progressBar = progressBar

  def isRunning(): Boolean = synchronized { running }

  def setRunning(running: Boolean) = synchronized {
    this.running = running
  }

  def isFinished = synchronized { finished }

  def setFinished(finished: Boolean) = synchronized {
    this.finished = finished
  }

  protected def rollback

}
