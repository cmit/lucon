package ro.cmit.lucon.gui.wizard

import ro.cmit.libs.formatters.Formatter

/**
 * Concrete formatter for Float.
 *
 */
class FloatFormatter extends Formatter[Float] {

  override def apply(value: Float): String = {
    if (value.isNaN())
      return null
    else
      value.toString()
  }

  override def unapply(value: String): Float = {
    if (value == null)
      return Float.NaN
    else value.toFloat
  }

}