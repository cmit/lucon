package ro.cmit.lucon.gui;

import java.util.ArrayList

import javax.swing.{JLabel, JSpinner, SpinnerNumberModel}
import javax.swing.event.{ChangeEvent, ChangeListener, TreeModelEvent, TreeModelListener}
import javax.swing.tree.{DefaultMutableTreeNode, TreeModel, TreePath}
import ro.cmit.lucon.engine.TermConcordance
import ro.cmit.lucon.gui.exception.LuconException
import ro.cmit.lucon.gui.viewers.ALuconViewer

import scala.collection.JavaConversions._

class TopTreeModel extends TreeModel with ChangeListener {

    private var root: DefaultMutableTreeNode = null;

    private var viewer: ALuconViewer = null;
    private val errorObservable = new ErrorObservable();
    private val treeModelListeners = new ArrayList[TreeModelListener]();
    private var termConcordance: TermConcordance = null;

    var nrRecordsOnPage = 200;
    var nrCurrentPage = 0;
    var nrRecordsTotal = 0;
    var nrPagesTotal = 0;
    var spinnerModel: SpinnerNumberModel = null;
    var label: JLabel = null;

    init();

    private def init() = {
        root = new DefaultMutableTreeNode(0 + " contexts");
        recalculateParameters();
    }

    private def recalculateParameters() = {
        nrRecordsTotal = if (termConcordance == null) 0 else termConcordance.contexts.size();
        nrPagesTotal =
          if (nrRecordsTotal == 0)
              1
          else
              nrRecordsTotal / nrRecordsOnPage + (if (nrRecordsTotal % nrRecordsOnPage == 0) 0 else 1)
        if (spinnerModel != null) {
            spinnerModel.setMaximum(nrPagesTotal);
            if (nrCurrentPage > spinnerModel.getMaximum().asInstanceOf[Int])
                nrCurrentPage = spinnerModel.getMaximum().asInstanceOf[Int];
            else if (nrCurrentPage < spinnerModel.getMinimum().asInstanceOf[Int])
                nrCurrentPage = spinnerModel.getMinimum().asInstanceOf[Int];
            spinnerModel.setValue(nrCurrentPage);
        }
        if (label != null)
            label.setText(" of " + nrPagesTotal);
    }

    def updateModel(termConcordance: TermConcordance) = {
        try {
            this.termConcordance = termConcordance;
            recalculateParameters();
            createNodes();
        } catch {
          case e1: RuntimeException =>
            // catch any runtime exception as OutOfMemory and notify for
            // error
            init();
            if (viewer != null)
                viewer.freeMouseAndKeyboard();
            errorObservable
                    .reportError(new LuconException(e1),
                            "Sorry, an error occured. Not all contexts may have been loaded.");
          case e2: Throwable =>
            // catch any runtime exception as OutOfMemory and notify for
            // error
            init();
            if (viewer != null)
                viewer.freeMouseAndKeyboard();
            errorObservable
                    .reportError(new LuconException(e2),
                            "Sorry, an error occured. Not all contexts may have been loaded");
        }
    }

    private def createNodes(): Unit = {
        if (termConcordance == null) {
            return;
        }
        try {
            if (viewer != null)
                viewer.blockMouseAndKeyboard();
            val contexts = termConcordance.getConcordanceContexts(
                    (nrCurrentPage - 1) * nrRecordsOnPage, nrCurrentPage * nrRecordsOnPage);
            val nrContexts = contexts.size();
            root = new DefaultMutableTreeNode(nrContexts + " contexts");
            root.removeAllChildren();

            contexts.zipWithIndex
              .groupBy(_._1.source.filePath).toList
              .sortBy(_._2.head._2).foreach {
              case (filePath, crtContexts) =>
                val nodeFile = new DefaultMutableTreeNode(s"(${crtContexts.size}) $filePath");
                root.add(nodeFile)
                crtContexts.foreach {
                  crtContext => nodeFile.add(new DefaultMutableTreeNode(crtContext._1))
                }
            }

            fireTreeStructureChanged(root);
            if (viewer != null)
                viewer.freeMouseAndKeyboard();
        } catch {
          case e1: RuntimeException =>
            // catch any runtime exception as OutOfMemory and notify for
            // error
            init();
            if (viewer != null)
                viewer.freeMouseAndKeyboard();
            errorObservable
                    .reportError(new LuconException(e1),
                            "Sorry, an error occured. Not all contexts may have been loaded.");
          case e2: Throwable =>
            // catch any runtime exception as OutOfMemory and notify for
            // error
            init();
            if (viewer != null)
                viewer.freeMouseAndKeyboard();
            errorObservable
                    .reportError(new LuconException(e2),
                            "Sorry, an error occured. Not all contexts may have been loaded.");
        }
    }

    def getContexts() = termConcordance.contexts

    def setViewer(viewer: ALuconViewer) = this.viewer = viewer

    override def addTreeModelListener(l: TreeModelListener) = treeModelListeners.add(l);

    override def getChild(parent: Object, index: Int): Object = parent.asInstanceOf[DefaultMutableTreeNode].getChildAt(index)

    override def getChildCount(parent: Object): Int = parent.asInstanceOf[DefaultMutableTreeNode].getChildCount()

    override def getIndexOfChild(parent: Object, child: Object): Int = 
      parent.asInstanceOf[DefaultMutableTreeNode].getIndex(child.asInstanceOf[DefaultMutableTreeNode])

    override def getRoot() = root

    override def isLeaf(node: Object): Boolean = node.asInstanceOf[DefaultMutableTreeNode].isLeaf();

    override def removeTreeModelListener(l: TreeModelListener) = treeModelListeners.remove(l);

    override def valueForPathChanged(arg0: TreePath , arg: Object) = {}

    /**
     * The only event raised by this model is TreeStructureChanged with the root
     * as path, i.e. the whole tree has changed.
     */
    def fireTreeStructureChanged(root: Object) = {
        val e = new TreeModelEvent(this, Array[Object](root));
        val len = treeModelListeners.size();
        for (tml <- treeModelListeners) {
            tml.treeStructureChanged(e);
        }
    }

    def getNrPagesTotal() = nrPagesTotal;

    def setSpinnerModel(spinnerModel: SpinnerNumberModel) = this.spinnerModel = spinnerModel

    def setLabel(label: JLabel) = this.label = label

    override def stateChanged(e: ChangeEvent) = {
        val source = e.getSource();
        if (source.isInstanceOf[JSpinner]) {
            val m = source.asInstanceOf[JSpinner];
            nrCurrentPage = m.getValue().asInstanceOf[Int];
            recalculateParameters();
            createNodes();
        }
    }

}
