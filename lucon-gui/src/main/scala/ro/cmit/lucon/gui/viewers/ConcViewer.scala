package ro.cmit.lucon.gui.viewers;

import java.awt._
import java.awt.event.{ActionEvent, ActionListener}
import java.io._
import java.util.{Calendar, Properties}

import javax.swing._
import org.apache.log4j.Logger
import org.apache.lucene.index.IndexReader
import ro.cmit.lucon.configuration.{IndexConfigurator, IndexKey}
import ro.cmit.lucon.engine.Concordance
import ro.cmit.lucon.gui.{ImageUtils, LeftTable, TopTree, TopTreeModel}
import ro.cmit.lucon.gui.viewers.ALuconViewer.{viewerCount, xOffset, yOffset}
import ro.cmit.lucon.gui.viewers.ConcViewer.{crtDir, logger, recentsDirectory}
import ro.cmit.lucon.gui.wizard.GuiKey;

/* Used by InternalFrameDemo.java. */
class ConcViewer(configurator: IndexConfigurator) extends ALuconViewer(configurator) with ActionListener {

  private var concordance: Concordance = null;

  private var crtTextField: JTextField = null;

  private var leftTable: LeftTable = null;

  private var topTree: TopTree = null;

  try {
    val imageData = ImageUtils.getIconData("/icons/lc2.jpg");
    setFrameIcon(new ImageIcon(imageData));
  } catch {
    case e: IOException =>
      ConcViewer.logger.error("Error initializating ConcViewer", e);
  }
  initialize()

  def openIndex(indexReader: IndexReader) = {
    concordance = new Concordance(indexReader);

    populateComponents();

    addPropertyFileInRecentDirectory(configurator.get(IndexKey.INDEX_FILE).getAbsolutePath());
  }

  private def populateComponents() = {
    leftTable.updateModel(concordance);
    // packRows(leftTable, 0);

    // alert("Index opened.");

    // TODO: trebuie populate controalele din acest viewer
  }

  private def addPropertyFileInRecentDirectory(absolutePath: String) = {
    val recentsDirectoryFullPath = crtDir + File.separator + recentsDirectory;
    val dir = new File(recentsDirectoryFullPath);
    var success = true;
    if (!dir.exists())
      success = success && dir.mkdirs();
    val menuFiles = dir.listFiles();
    var found = false;
    if (menuFiles != null && menuFiles.length > 0) {
      for (file <- menuFiles) {
        val props = new Properties();
        try {
          val fis = new FileInputStream(file);
          props.load(fis);
          fis.close();

          val indexPath = props.getProperty("index.path");
          if (absolutePath.equals(indexPath)) {
            found = true;
            //break;
          }
        } catch {
          case e: FileNotFoundException =>
            logger.error("Error occurred.", e);
          case e: IOException =>
            logger.error("Error occurred.", e);
        }
      }
    }
    if (!found) {
      val propFileName = recentsDirectoryFullPath + File.separator + createNewPropFileName();

      try {
        val fos = new FileOutputStream(propFileName);
        fos.write(new String("index.path=" + absolutePath.replaceAll("\\\\", "\\\\u005c") + "\n")
          .getBytes());
        fos.write(new String("left.context.size="
          + configurator.getDefault(IndexKey.LEFT_CONTEXT) + "\n").getBytes());
        fos
          .write(new String("right.context.size="
            + configurator.getDefault(IndexKey.RIGHT_CONTEXT) + "\n")
            .getBytes());
        fos.close();
        configurator.get(GuiKey.MAIN_FRAME).updateMenuFileOpenRecent;
      } catch {
        case e: FileNotFoundException =>
          logger.error("Error occurred.", e);
        case e: IOException =>
          logger.error("Error occurred.", e);
      }
    }
  }

  private def createNewPropFileName(): String = {
    val calendar = Calendar.getInstance();
    val year = calendar.get(Calendar.YEAR);
    val month = calendar.get(Calendar.MONTH) + 1;
    val day = calendar.get(Calendar.DAY_OF_MONTH);
    val hour = calendar.get(Calendar.HOUR_OF_DAY);
    val minute = calendar.get(Calendar.MINUTE);
    val second = calendar.get(Calendar.SECOND);
    return "recent-" + year +
      (if (month < 10) "0" + month else "" + month) +
      (if (day < 10) "0" + day else "" + day) +
      (if (hour < 10) "0" + hour else "" + hour) +
      (if (minute < 10) "0" + minute else "" + minute) +
      (if (second < 10) "0" + second else "" + second);
  }

  @Override
  def initialize(): Unit = {
    val propFile = new File(crtDir, ALuconViewer.propFileName);
    val props = new Properties();
    try {
      props.load(new FileInputStream(propFile));
    } catch {
      case e: FileNotFoundException =>
        logger.error("Error occurred.", e);
      case e: IOException =>
        logger.error("Error occurred.", e);
    }

    // ...Then set the window size or call pack...
    setSize(600, 400);

    // Set the window's location.
    setLocation(xOffset * (viewerCount - 1), yOffset * (viewerCount - 1));
    setLayout(new BorderLayout());
    val contentPane = getContentPane();
    contentPane.add(createHorizSplitPane(), BorderLayout.CENTER);

//    val statusBar = createStatusBar();
//    add(statusBar, BorderLayout.SOUTH);

    // conexiuni intre componente
    leftTable.setTopTree(topTree);
    leftTable.setViewer(this);

    crtTextField.getDocument().addDocumentListener(leftTable.getFilter());
    crtTextField.addActionListener(leftTable.getFilter());

    leftTable.getFilter().setInternalFrame(this);

  }

  protected def createStatusBar(): JPanel = {
    val statusBar = new JPanel();
    statusBar.setSize(200, 60);
    statusBar.setLayout(new BorderLayout());
    val statusLabel = new JLabel(s"Small context: left - " +
      configurator.getDefault(IndexKey.LEFT_CONTEXT) +
      ", right - " + configurator.getDefault(IndexKey.RIGHT_CONTEXT) +
      "; large context: left - 200, right - 200");
    statusBar.add(statusLabel, BorderLayout.EAST);
    statusBar
  }

  private def createHorizSplitPane(): JSplitPane = {
    val horizSplitPane = new JSplitPane(
      JSplitPane.HORIZONTAL_SPLIT,
      createLeftComponent(), createVertSplitPane());
    horizSplitPane.setOneTouchExpandable(true);
    val mainFrame = configurator.get(GuiKey.MAIN_FRAME);
    val l = (mainFrame.getWidth() / 4).toInt;
    horizSplitPane.setDividerLocation(l + horizSplitPane.getInsets().left);

    return horizSplitPane;
  }

  private def createLeftComponent(): Component = {
    if (crtTextField == null)
      crtTextField = new JTextField(5);

    val p = new JPanel(new BorderLayout());
    p.add(crtTextField, BorderLayout.NORTH);
    if (leftTable == null)
      leftTable = new LeftTable(concordance);
    leftTable.addErrorObserver(this);
    /*
		 * leftTable.setFont(leftTable.getFont().deriveFont((float)defaultFont));
		 */
    p.add(new JScrollPane(leftTable), BorderLayout.CENTER);

    val southPanel = new JPanel(new GridLayout(0, 1));
    val pagePanel = createTablePaginatorPanel();
    southPanel.add(pagePanel);
    p.add(southPanel, BorderLayout.SOUTH);

    return p;
  }

  private def createVertSplitPane(): JSplitPane = {
    val vertSplitPane = new JSplitPane(
      JSplitPane.VERTICAL_SPLIT,
      createTopComponent(), createBottomComponent());
    vertSplitPane.setOneTouchExpandable(true);
    vertSplitPane.setDividerLocation(130);

    return vertSplitPane;
  }

  private def createTopComponent(): Component = {
    /*
		 * if (topTable == null) topTable = new TopTable();
		 * topTable.setFont(topTable.getFont().deriveFont((float)defaultFont));
		 *
		 * TopTableModel model = topTable.getModel(); int startValue =
		 * model.getLeftContextSize(); JSpinner spin = new JSpinner(new
		 * SpinnerNumberModel(new Integer(startValue), new Integer(0), null, new
		 * Integer(1))); spin.addChangeListener(model);
		 */

    val northPanel = new JPanel(new BorderLayout());
    val expandCollapsePanel = createExpandCollapsePanel();
    northPanel.add(expandCollapsePanel, BorderLayout.WEST);
    if (topTree == null)
      topTree = new TopTree(this);
    topTree.addErrorObserver(this);
    topTree.addTreeSelectionListener(bottomArea)
    topTree.addTreeSelectionListener(bottomLabel)
    val pagePanel = createTreePaginatorPanel();
    northPanel.add(pagePanel, BorderLayout.EAST);
    // northPanel.add(spin);

    val topPanel = new JPanel(new BorderLayout());
    topPanel.add(northPanel, BorderLayout.NORTH);
    topPanel.add(new JScrollPane(topTree), BorderLayout.CENTER);

    return topPanel;
  }

  private def createExpandCollapsePanel(): JPanel = {
    val panel = new JPanel();
    panel.setLayout(new GridLayout(1, 2, 5, 5));
    val expandAllButton = new JButton("Expand all");
    expandAllButton.setName("expandAll");
    expandAllButton.setMargin(new Insets(1, 1, 1, 1));
    val expandFont = expandAllButton.getFont().deriveFont(Font.BOLD, 9.0f);
    expandAllButton.setFont(expandFont);
    expandAllButton.setPreferredSize(new Dimension(80, 20));
    expandAllButton.addActionListener(this);
    panel.add(expandAllButton);

    val collapseAllButton = new JButton("Colapse all");
    collapseAllButton.setName("colapseAll");
    collapseAllButton.setMargin(new Insets(1, 1, 1, 1));
    val collapseFont = collapseAllButton.getFont().deriveFont(Font.BOLD, 9.0f);
    collapseAllButton.setFont(collapseFont);
    collapseAllButton.setPreferredSize(new Dimension(80, 20));
    collapseAllButton.addActionListener(this);
    panel.add(collapseAllButton);

    return panel;
  }

  private def createBottomComponent(): Component = {
    val p = new JPanel(new BorderLayout());
    val labelPane = new JScrollPane(bottomLabel);
    p.add(labelPane, BorderLayout.SOUTH);
    p.add(new JScrollPane(bottomArea), BorderLayout.CENTER);
    return p;
  }

  private def createTablePaginatorPanel(): JPanel = {
    val pagePanel = new JPanel(new GridLayout(1, 3, 5, 5));
    pagePanel.add(new JLabel("Page: "));
    val tablePaginator = leftTable.getTablePaginator();
    val maxSpinner = if (tablePaginator.getNrPagesTotal() == 1) 1
    else tablePaginator.getNrPagesTotal();
    val spinnerModel = new SpinnerNumberModel(1, 1, maxSpinner, 1);
    val pageSpinner = new JSpinner(spinnerModel);
    tablePaginator.setSpinnerModel(spinnerModel);
    pageSpinner.addChangeListener(tablePaginator);
    pagePanel.add(pageSpinner);
    val label = new JLabel(" of " + maxSpinner);
    pagePanel.add(label);
    tablePaginator.setLabel(label);

    return pagePanel;
  }

  private def createTreePaginatorPanel(): JPanel = {
    val pagePanel = new JPanel(new GridLayout());
    pagePanel.add(new JLabel("Page: "));
    val treeModel = topTree.getModel().asInstanceOf[TopTreeModel];
    val maxSpinner =
      if (treeModel.getNrPagesTotal() == 1) 1
      else treeModel.getNrPagesTotal();
    val spinnerModel = new SpinnerNumberModel(1, 1, maxSpinner, 1);
    val pageSpinner = new JSpinner(spinnerModel);
    treeModel.setSpinnerModel(spinnerModel);
    pageSpinner.addChangeListener(treeModel);
    pagePanel.add(pageSpinner);
    val label = new JLabel(" of " + maxSpinner);
    pagePanel.add(label);
    treeModel.setLabel(label);

    return pagePanel;
  }

  override def alert(msg: String) = {
    JOptionPane.showMessageDialog(this, msg);
  }

  def getTopTree() = topTree

  override def actionPerformed(e: ActionEvent) = {
    val src = e.getSource();
    if (src.isInstanceOf[JButton]) {
      val button = src.asInstanceOf[JButton];
      val cmd = button.getName();
      if ("expandAll".equals(cmd)) {
        topTree.expandAll(true);
      } else if ("colapseAll".equals(cmd)) {
        topTree.expandAll(false);
      }

    }
  }

  def getConcordance() = concordance;

  @throws[Throwable]
  override def finalize() = {
    super.finalize();
    if (concordance.indexReader != null) {
      concordance.indexReader.close();
      concordance = null;
    }
  }

}

object ConcViewer {
  val homeIndexDirPropName = "home.indexes.dir";
  val crtDir = System.getProperty("user.dir");
  val recentsDirectory = "recents";
  val logger = Logger.getLogger(ConcViewer.getClass);

  // The height of each row is set to the preferred height of the
  // tallest cell in that row.
  def packRows(table: JTable, margin: Int): Unit = {
    packRows(table, 0, table.getRowCount(), margin);
  }

  // For each row >= start and < end, the height of a
  // row is set to the preferred height of the tallest cell
  // in that row.
  private def packRows(table: JTable, start: Int, end: Int, margin: Int) = {
    // Get the preferred height
    val h = getPreferredRowHeight(table, 0, margin);

    for (r <- 0 until table.getRowCount()) {

      // Now set the row height using the preferred height
      if (table.getRowHeight(r) != h) {
        table.setRowHeight(r, h);
      }
    }
  }

  // Returns the preferred height of a row.
  // The result is equal to the tallest cell in the row.
  private def getPreferredRowHeight(table: JTable, rowIndex: Int,
                                    margin: Int): Int = {
    // Get the current default height for all rows
    var height = table.getRowHeight();

    // Determine highest cell in the row
    for (c <- 0 until table.getColumnCount()) {
      val renderer = table.getCellRenderer(rowIndex, c);
      val comp = table.prepareRenderer(renderer, rowIndex, c);
      val h = comp.getPreferredSize().height + 2 * margin;
      height = Math.max(height, h);
    }
    return height;
  }

}