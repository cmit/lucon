package ro.cmit.lucon.gui.viewers;

import java.awt.{BorderLayout, Component, Dimension, Font, GridLayout, Insets}
import java.awt.event.{ActionEvent, ActionListener}
import java.io.{File, FileInputStream, FileNotFoundException, IOException}
import java.util.Properties

import javax.swing._
import org.apache.log4j.Logger
import ro.cmit.lucon.configuration.{IndexConfigurator, IndexKey}
import ro.cmit.lucon.engine.Concordance
import ro.cmit.lucon.gui.{ErrorObservable, TopTree, TopTreeModel}
import ro.cmit.lucon.gui.exception.LuconException
import ro.cmit.lucon.gui.viewers.ALuconViewer.{propFileName, viewerCount, xOffset, yOffset}
import ro.cmit.lucon.gui.viewers.ConcViewer2.logger

/* Used by InternalFrameDemo.java. */
class ConcViewer2(
  configurator: IndexConfigurator,
  concordance:  Concordance, regexTerms: List[String], distance: Int) extends ALuconViewer(configurator) with ActionListener {
  private var topTree: TopTree = null;

  private val errorObservable = new ErrorObservable();
  errorObservable.addObserver(this);
  initialize()
  blockMouseAndKeyboard();
  try {
    val termConcordance = concordance.contextsWithDistance(
      regexTerms,
      distance, configurator.getDefault(IndexKey.LEFT_CONTEXT),
      configurator.getDefault(IndexKey.RIGHT_CONTEXT));
    topTree.updateModel(termConcordance);
  } catch {
    case e1: RuntimeException =>
      // catch any runtime exception as OutOfMemory and notify for
      // error
      errorObservable
        .reportError(
          new LuconException(e1),
          "Sorry, an error occured. Maybe, not all contexts are loaded.");
      logger.error(e1)
    case e2: Throwable =>
      // catch any runtime exception as OutOfMemory and notify for
      // error
      errorObservable
        .reportError(
          new LuconException(e2),
          "Sorry, an error occured. Maybe, not all contexts are loaded.");
      logger.error(e2)
  } finally {
    freeMouseAndKeyboard();
  }

  override def initialize(): Unit = {
    val crtDir = System.getProperty("user.dir");
    val propFile = new File(crtDir, propFileName);
    val props = new Properties();
    try {
      props.load(new FileInputStream(propFile));
    } catch {
      case e: FileNotFoundException =>
        logger.error("Error occurred.", e);
      case e: IOException =>
        logger.error("Error occurred.", e);
    }

    // ...Then set the window size or call pack...
    setSize(600, 400);

    // Set the window's location.
    setLocation(xOffset * (viewerCount - 1), yOffset * (viewerCount - 1));
    setLayout(new BorderLayout());
    val contentPane = getContentPane();
    contentPane.add(createVertSplitPane(), BorderLayout.CENTER);

//    val statusBar = createStatusBar();
//    add(statusBar, BorderLayout.SOUTH);

  }

  protected def createStatusBar(): JPanel = {
    val statusBar = new JPanel();
    statusBar.setSize(200, 60);
    statusBar.setLayout(new BorderLayout());
    val statusLabel = new JLabel("Small context: left - " +
      configurator.getDefault(IndexKey.LEFT_CONTEXT) +
      ", right - " + configurator.getDefault(IndexKey.RIGHT_CONTEXT) +
      "; large context: left - 200, right - 200");
    statusBar.add(statusLabel, BorderLayout.EAST);
    return statusBar;
  }

  private def createVertSplitPane(): JSplitPane = {
    val vertSplitPane = new JSplitPane(
      JSplitPane.VERTICAL_SPLIT,
      createTopComponent(), createBottomComponent());
    vertSplitPane.setOneTouchExpandable(true);
    vertSplitPane.setDividerLocation(130);

    return vertSplitPane;
  }

  private def createTopComponent(): Component = {
    val northPanel = new JPanel(new BorderLayout());
    val expandCollapsePanel = createExpandCollapsePanel();
    northPanel.add(expandCollapsePanel, BorderLayout.WEST);
    if (topTree == null)
      topTree = new TopTree(this);
    topTree.addErrorObserver(this);
    topTree.addTreeSelectionListener(bottomArea)
    topTree.addTreeSelectionListener(bottomLabel)
    val pagePanel = createTreePaginatorPanel();
    northPanel.add(pagePanel, BorderLayout.EAST);
    // northPanel.add(spin);

    val topPanel = new JPanel(new BorderLayout());
    topPanel.add(northPanel, BorderLayout.NORTH);
    topPanel.add(new JScrollPane(topTree), BorderLayout.CENTER);

    return topPanel;
  }

  private def createExpandCollapsePanel(): JPanel = {
    val panel = new JPanel();
    panel.setLayout(new GridLayout(1, 2, 5, 5));
    val expandAllButton = new JButton("Expand all");
    expandAllButton.setName("expandAll");
    expandAllButton.setMargin(new Insets(1, 1, 1, 1));
    val expandFont = expandAllButton.getFont().deriveFont(Font.BOLD, 9.0f);
    expandAllButton.setFont(expandFont);
    expandAllButton.setPreferredSize(new Dimension(80, 20));
    expandAllButton.addActionListener(this);
    panel.add(expandAllButton);

    val collapseAllButton = new JButton("Colapse all");
    collapseAllButton.setName("colapseAll");
    collapseAllButton.setMargin(new Insets(1, 1, 1, 1));
    val collapseFont = collapseAllButton.getFont().deriveFont(Font.BOLD, 9.0f);
    collapseAllButton.setFont(collapseFont);
    collapseAllButton.setPreferredSize(new Dimension(80, 20));
    collapseAllButton.addActionListener(this);
    panel.add(collapseAllButton);

    return panel;
  }

  private def createTreePaginatorPanel(): JPanel = {
    val pagePanel = new JPanel(new GridLayout());
    pagePanel.add(new JLabel("Page: "));
    val treeModel = topTree.getModel().asInstanceOf[TopTreeModel];
    val maxSpinner = if (treeModel.getNrPagesTotal() == 1) 1
    else treeModel.getNrPagesTotal();
    val spinnerModel = new SpinnerNumberModel(1, 1, maxSpinner, 1);
    val pageSpinner = new JSpinner(spinnerModel);
    treeModel.setSpinnerModel(spinnerModel);
    pageSpinner.addChangeListener(treeModel);
    pagePanel.add(pageSpinner);
    val label = new JLabel(" of " + maxSpinner);
    pagePanel.add(label);
    treeModel.setLabel(label);

    return pagePanel;
  }

  private def createBottomComponent(): Component = {
    val p = new JPanel(new BorderLayout());
    p.add(new JScrollPane(bottomLabel), BorderLayout.NORTH);
    p.add(new JScrollPane(bottomArea), BorderLayout.CENTER);
    return p;
  }

  def updateTopTree(tt: TopTree): Unit = topTree = tt

  def getTopTree(): TopTree = topTree

  override def actionPerformed(e: ActionEvent): Unit = {
    val src = e.getSource();
    if (src.isInstanceOf[JButton]) {
      val button = src.asInstanceOf[JButton];
      val cmd = button.getName();
      if (cmd.equals("expandAll")) {
        topTree.expandAll(true);
      } else if (cmd.equals("colapseAll")) {
        topTree.expandAll(false);
      }

    }
  }

  def getConcordance(): Concordance = concordance
}

object ConcViewer2 {
  val logger = Logger.getLogger(ConcViewer2.getClass);
}