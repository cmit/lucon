package ro.cmit.lucon.gui.tasks;

/** Uses a SwingWorker to perform a time-consuming (and utterly fake) task. */

import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.IOException
import java.io.InputStreamReader
import java.io.StringReader
import java.io.UnsupportedEncodingException

import scala.collection.JavaConversions.collectionAsScalaIterable

import org.apache.log4j.Logger
import org.apache.lucene.document.Document
import org.apache.lucene.document.Field
import org.apache.lucene.index.IndexWriter
import org.apache.lucene.store.FSDirectory

import javax.swing.Timer
import ro.cmit.lucon.analyzers.ConcAnalyzer
import ro.cmit.lucon.configuration.IndexConfigurator
import ro.cmit.lucon.configuration.IndexKey

class IndexingTask(override val configurator: IndexConfigurator) extends ATask(configurator) with ActionListener {

  private var contentAnalyser: ConcAnalyzer = null

  private var metaInfoAnalyser: ConcAnalyzer = null

  private var current = 0;

  private var taskLength = 0;

  private val logger = Logger.getLogger(this.getClass);

  @throws[InterruptedException]
  override def run(): Object = {

    try {
      prepareIndexDirectory

      val created = createIndexes
      if (!created)
        return null;

      writePropertyFile();
    } catch {
      case e: UnsupportedEncodingException => {
        rollback();
        logger.error("Error occurred.", e);
      }
      case e: FileNotFoundException => {
        rollback();
        logger.error("Error occurred.", e);
      }
      case e: IOException => {
        rollback();
        logger.error("Error occurred.", e);
      }
    }

    return null;
  }

  @throws[IOException]
  @throws[FileNotFoundException]
  private def writePropertyFile() = {
    logger.info("Writing index property file.");
    configurator.save(configurator.get(IndexKey.INDEX_FILE));
  }

  @throws[IOException]
  @throws[UnsupportedEncodingException]
  @throws[FileNotFoundException]
  private def createIndexes: Boolean = {
    logger.info("Creating indexes.");

    val timer = new Timer(1000, this);
    timer.start();

    contentAnalyser = new ConcAnalyzer(configurator);
    val indexContentDir = configurator.get(IndexKey.INDEX_CONTENT_DIR);
    val contentWriter = new IndexWriter(
      FSDirectory.getDirectory(indexContentDir), true,
      contentAnalyser, true);
    contentWriter.setMaxFieldLength(Integer.MAX_VALUE);
    // logger.debug("Created content analyser.");

    metaInfoAnalyser = new ConcAnalyzer(configurator);
    // logger.debug("meta-info dir: " + indexMetaInfoDir);
    val indexMetaInfoDir = configurator.get(IndexKey.INDEX_METAINFO_DIR);
    val metaInfoWriter = new IndexWriter(
      FSDirectory.getDirectory(indexMetaInfoDir), metaInfoAnalyser,
      true);
    metaInfoWriter.setMaxFieldLength(Integer.MAX_VALUE);
    // logger.debug("Created meta-info analyser.");

    current = 0;
    taskLength = configurator.getSizeOfFiles.toInt;
    val metaFieldsList = configurator.getXmlMetaElements();
    val files = configurator.getFiles
    var i = 0
    while (i < files.size && !isTaskInterrupted) {
      val crtFile = files(i);
      // logger.debug("Indexing file: " + crtFile);

      // aici se face indexarea de continut
      val isr = new InputStreamReader(new FileInputStream(
        crtFile), "UTF-8");
      // logger.debug("Content done 1.");
      contentAnalyser.forFile(crtFile);
      // logger.debug("Content done 2.");
      var document = new Document();
      document.add(new Field("path", crtFile.getAbsolutePath(),
        Field.Store.YES, Field.Index.UN_TOKENIZED,
        Field.TermVector.NO));
      document.add(new Field("text", isr,
        Field.TermVector.WITH_POSITIONS_OFFSETS));
      // logger.debug("Content done 3.");
      contentWriter.addDocument(document);
      // logger.debug("Content done.");

      // adaug si fieldurile cu metainfo separat
      document = new Document();
      document.add(new Field("path", crtFile.getAbsolutePath(),
        Field.Store.YES, Field.Index.UN_TOKENIZED,
        Field.TermVector.NO));
      val crtMetaFieldsMap = configurator.getMetaInfoFields
      if (crtMetaFieldsMap != null) {
        for (fieldName <- metaFieldsList) {
          var fieldContent = crtMetaFieldsMap.get(fieldName);
          if (fieldContent == null)
            fieldContent = "";
          val r = new StringReader("<metainfo>" + fieldContent
            + "</metainfo>");
          document.add(new Field(fieldName, r,
            Field.TermVector.WITH_POSITIONS_OFFSETS));
        }
      }
      metaInfoWriter.addDocument(document);
      // logger.debug("metainfo done.");

      current = current + crtFile.length().toInt;
      if (isTaskInterrupted) {
        rollback();
        contentWriter.close();
        metaInfoWriter.close();
        timer.stop();
        updateProgressbar(current, taskLength, "Stopped");
        setRunning(false);
        return false;
      }

      i = i + 1
    }
    contentWriter.close();
    metaInfoWriter.close();
    timer.stop();
    updateProgressbar(taskLength, taskLength, "Finished");
    setRunning(false);

    // logger.info("Created indexes.");
    return true;
  }

  private def prepareIndexDirectory: Boolean = {
    var success = true;

    val indexRootDir = configurator.get(IndexKey.INDEX_ROOT_DIR);

    if (!indexRootDir.exists())
      success = success && indexRootDir.mkdirs();
    if (!success) {
      setTaskInterrupted(true);
      logger.error("Failed to create: " + indexRootDir);
      return false;
    }

    val indexContentDir = configurator.get(IndexKey.INDEX_CONTENT_DIR);
    if (!indexContentDir.exists())
      success = success && indexContentDir.mkdirs();
    if (!success) {
      setTaskInterrupted(true);
      logger.error("Failed to create: " + indexContentDir);
      return false;
    }

    val indexMetaInfoDir = configurator.get(IndexKey.INDEX_METAINFO_DIR);
    if (!indexMetaInfoDir.exists())
      success = success && indexMetaInfoDir.mkdirs();
    if (!success) {
      setTaskInterrupted(true);
      logger.error("Failed to create: " + indexMetaInfoDir);
      return false;
    }

    return success;
  }

  override def rollback() = {
    val indexRootDir = configurator.get(IndexKey.INDEX_ROOT_DIR);
    if (indexRootDir != null) {
      try {
        new IndexDeletingTask(configurator).run();
      } catch {
        case e: Exception => logger.error("Error occurred.", e);
      }
    }
  }

  override def actionPerformed(e: ActionEvent) = {
    if (contentAnalyser != null) {
      updateProgressbar(
        current + contentAnalyser.getReaderPosition.toInt,
        taskLength, "Indexing...");
    }

  }

}
