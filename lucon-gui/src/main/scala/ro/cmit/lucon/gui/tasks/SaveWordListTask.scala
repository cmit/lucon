package ro.cmit.lucon.gui.tasks

/** Uses a SwingWorker to perform a time-consuming (and utterly fake) task. */

import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStreamWriter

import scala.util.control.Breaks.break
import scala.util.control.Breaks.breakable

import org.apache.log4j.Logger
import org.apache.lucene.index.IndexReader

import javax.swing.Timer
import ro.cmit.lucon.configuration.IndexConfigurator
import ro.cmit.lucon.configuration.IndexKey
import ro.cmit.lucon.engine.FieldUtils

class SaveWordListTask(override val configurator: IndexConfigurator) extends ATask(configurator) with ActionListener {

  private var location: File = null

  private var current = 0

  private var taskLength = 0

  private val logger = Logger.getLogger(this.getClass)

  def setLocation(location: File) = this.location = location

  @throws[InterruptedException]
  override def run(): Object = {
    if (location != null) {
      val timer = new Timer(1000, this)
      timer.start()

      useIndeterminateProgressbar
      updateProgressbar(0, 100, "Calculating...")
      taskLength = 0
      try {
        val index = IndexReader.open(configurator.get(IndexKey.INDEX_CONTENT_DIR))
        var te = index.terms()
        while (te.next()) {
          val t = te.term()
          taskLength = taskLength + 1
        }

        useDeterminateProgressbar
        updateProgressbar(0, taskLength, "Saving...")
        val fos = new FileOutputStream(location)
        val osw = new OutputStreamWriter(fos, "UTF-8")

        te = index.terms()
        while (te.next()) {
          breakable {
            val t = te.term()
            if (t.text().startsWith(FieldUtils.PREFIX_ATTRIBUTE)
              || t.text().startsWith(FieldUtils.PREFIX_XPATH))
              break
            var fr = 0
            val td = index.termDocs(t)
            while (td.next()) {
              fr += td.freq()
            }
            osw.write(t.text() + "\t" + fr + "\r\n")
            current = current + 1
          }
        }

        osw.close()
        fos.close()
      } catch {
        case e: IOException => logger.error("Error occurred.", e)
      }

      timer.stop()
      updateProgressbar(taskLength, taskLength, "Finished")
    }

    return null
  }

  override def rollback() = {}

  override def actionPerformed(e: ActionEvent) = updateProgressbar(current, taskLength, "Saving...")

}
