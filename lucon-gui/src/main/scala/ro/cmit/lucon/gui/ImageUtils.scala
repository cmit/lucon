package ro.cmit.lucon.gui;

import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream;

object ImageUtils {

  @throws(classOf[IOException])
  def getIconData(filePath: String): Array[Byte] = {
    val in: InputStream = ImageUtils.getClass.getResourceAsStream(filePath);
    val bos = new ByteArrayOutputStream();
    val buffer = new Array[Byte](1024);
    var n = in.read(buffer);
    while (n > 0) {
      bos.write(buffer, 0, n);
      n = in.read(buffer);
    }
    bos.flush();
    return bos.toByteArray();
  }

}
