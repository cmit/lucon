package ro.cmit.lucon.gui.wizard.step;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;

import org.apache.log4j.Logger;
import org.pietschy.wizard.InvalidStateException;
import org.pietschy.wizard.WizardModel;

import ro.cmit.lucon.gui.tasks.ATask;
import ro.cmit.lucon.gui.tasks.IndexOpeningTask;
import ro.cmit.lucon.configuration.IndexConfigurator;
import ro.cmit.lucon.configuration.IndexKey;
import foxtrot.Worker;

import ChooseIndexStep._

class ChooseIndexStep(configurator: IndexConfigurator) extends AIndexStep("Open created index.", "", configurator) 
with ActionListener
with DocumentListener
with ChangeListener {
	private var task: ATask = null;
	// lcs = leftContextSize
	private var lcsSpinner: JSpinner = null;

	// rcs = rightContextSize
	private var rcsSpinner: JSpinner = null;


	private var pathToIndexField: JTextField = null;

	private var browseButton: JButton = null;


	private val fileChooser = new JFileChooser(new File(System.getProperty("user.dir")));
	fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
	fileChooser.setMultiSelectionEnabled(false);
	fileChooser.setFileFilter(new IdxFileFilter());
	setLayout(new BorderLayout(3, 3));
	add(createPanel(), BorderLayout.NORTH);
	private val progressBar = createProgressBar();
	add(progressBar, BorderLayout.SOUTH);

	private def createPanel(): Component = {
		val panel = new JPanel(new BorderLayout(3, 3));
		panel.add(createContextSizePanel(), BorderLayout.NORTH);
		panel.add(createPathToIndexPanel(), BorderLayout.SOUTH);
		return panel;
	}

	private def createContextSizePanel(): Component = {
		val panel = new JPanel(new GridLayout(2, 1));
		panel.add(createCSPanel("Left context size", true));
		panel.add(createCSPanel("Right context size", false));
		return panel;
	}
	
	private def createCSPanel(labelStr: String, isLeft: Boolean): Component = {
		val panel = new JPanel(new BorderLayout());
		val label = new JLabel(labelStr);
		panel.add(label, BorderLayout.WEST);
		var spinnerModel: SpinnerNumberModel = null;
		var spinner: JSpinner = null;
		if(isLeft) {
			spinnerModel = new SpinnerNumberModel(
					configurator.getDefault(IndexKey.LEFT_CONTEXT).intValue(), 0, 200, 1);
			spinner = new JSpinner(spinnerModel);
			lcsSpinner = spinner;
		}
		else {
			spinnerModel = new SpinnerNumberModel(
			        configurator.getDefault(IndexKey.RIGHT_CONTEXT).intValue(), 0, 200, 1);
			spinner = new JSpinner(spinnerModel);
			rcsSpinner = spinner;
		}
		spinnerModel.addChangeListener(this);
		panel.add(spinner, BorderLayout.EAST);
		
		return panel;
	}

	private def createPathToIndexPanel():Component = {
		val panel = new JPanel(new BorderLayout());
		pathToIndexField = new JTextField();
		pathToIndexField.getDocument().addDocumentListener(this);
		browseButton = new JButton("Browse...");
		panel.add(pathToIndexField, BorderLayout.CENTER);
		panel.add(browseButton, BorderLayout.EAST);
		browseButton.addActionListener(this);
		return panel;
	}

	private def createProgressBar(): JProgressBar = {
		val progressBar = new JProgressBar(0, 100);
		progressBar.setStringPainted(true);
		progressBar.setAlignmentX(Component.LEFT_ALIGNMENT);
		return progressBar;
	}

	override def actionPerformed(e: ActionEvent): Unit = {
		val eventSource = e.getSource();
		if (eventSource.isInstanceOf[JButton]) {
			val returnVal: Int = fileChooser.showOpenDialog(this);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				val file = fileChooser.getSelectedFile();
				val str = file.getAbsolutePath();
				pathToIndexField.setText(str);
				setComplete(true);
			}
		}
	}

	override def isComplete(): Boolean = {
		if (pathToIndexField.getText().trim().length() > 0) {
			val f = new File(pathToIndexField.getText());
			return f.exists() && f.isFile();
		}
		return false;
	}

	@throws[InvalidStateException]
	override def applyState(): Unit = {
	  configurator.init();
		setBusy(true);
		try {
			// do some work on another thread.. see Foxtrot
			/*
			 * task = new ComplexAnalizeTask();
			 * task.setProgressBar(progressBar); task.setStartingFiles(files);
			 * task.setConfiguration(getConfiguration()); Worker.post(task);
			 */
		    logger.info("Open index from: " + pathToIndexField.getText());
		    configurator.setIndexRootDir(new File(pathToIndexField.getText()).getParentFile());
			task = new IndexOpeningTask(configurator);
			task.setProgressBar(progressBar);
			Worker.post(task);
		} catch {
		  case e: Exception =>
			  logger.error("Error occurred.", e);
		} finally {
			setBusy(false);
		}
	}

	override def abortBusy(): Unit = {
		super.abortBusy();
	}

	override def init(model: WizardModel): Unit = {
		// TODO: de implementat
	}

	override def prepare(): Unit = { // read the model and configure the panel
		// TODO: de implementat
	}

	override def changedUpdate(e: DocumentEvent): Unit = {
		if (isComplete())
			this.setComplete(true);
		else
			this.setComplete(false);
	}

	override def insertUpdate(e: DocumentEvent): Unit = {
		if (isComplete())
			this.setComplete(true);
		else
			this.setComplete(false);
	}

	override def removeUpdate(e: DocumentEvent ): Unit = {
		if (isComplete())
			this.setComplete(true);
		else
			this.setComplete(false);
	}

	private class IdxFileFilter extends FileFilter {
		override def accept(f: File): Boolean = {
			if (f.isFile()) {
				return f.getAbsolutePath().toLowerCase().endsWith(".idx");
			}
			return true;
		}

		override def getDescription() = "Idx files"
	}

	override def stateChanged(e: ChangeEvent): Unit = {
		val eventSource = e.getSource();
		if (eventSource.isInstanceOf[SpinnerNumberModel]) {
			if (eventSource == lcsSpinner.getModel()) {
			    configurator.set(IndexKey.LEFT_CONTEXT, eventSource.asInstanceOf[SpinnerNumberModel].getValue().asInstanceOf[Int]);
			} else if (eventSource == rcsSpinner.getModel()) {
			    configurator.set(IndexKey.RIGHT_CONTEXT, eventSource.asInstanceOf[SpinnerNumberModel].getValue().asInstanceOf[Int]);
			}
		}
	}

}

object ChooseIndexStep {
	val logger = Logger.getLogger(ChooseIndexStep.getClass);
  
}