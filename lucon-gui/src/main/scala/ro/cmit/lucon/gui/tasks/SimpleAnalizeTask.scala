package ro.cmit.lucon.gui.tasks

import java.io.File
import java.util.LinkedList
import java.util.Queue
import java.util.Vector

import scala.collection.JavaConversions.seqAsJavaList

import ro.cmit.lucon.configuration.IndexConfigurator

/** Uses a SwingWorker to perform a time-consuming (and utterly fake) task. */

class SimpleAnalizeTask(override val configurator: IndexConfigurator) extends ATask(configurator) {
  private var startingFiles: Array[File] = null

  @throws[InterruptedException]
  override def run(): Object = {
    val queue: Queue[File] = new LinkedList[File]
    queue.addAll(startingFiles.toSeq)

    var files = new Vector[File](1, 1)
    var taskLength = files.size() + queue.size()
    updateProgressbar(0, taskLength, "0/" + taskLength)
    var j = 0
    while (!queue.isEmpty()) {
      val f = queue.poll()
      if (f.isFile()) {
        files.add(f)
        configurator.addFile(f)
        j = j + 1
      } else {
        val fileList = f.listFiles()
        queue.addAll(fileList.toSeq)
      }
      taskLength = files.size() + queue.size()
      updateProgressbar(j, taskLength, j + "/" + taskLength)
      if (isTaskInterrupted) {
        updateProgressbar(j, taskLength, j + "Stopped")
        return null
      }
    }
    updateProgressbar(taskLength, taskLength, "100%")
    return null
  }

  def getStartingFiles = startingFiles

  def setStartingFiles(startingFiles: Array[File]) = this.startingFiles = startingFiles

  override def rollback() = {}

}
