package ro.cmit.lucon.gui.wizard

import ro.cmit.libs.tsc.DefaultKey
import ro.cmit.libs.tsc.TypeSafeKey
import ro.cmit.lucon.gui.AbstractMainFrame
import ro.cmit.libs.tsc.key.property.PropertyKey
import ro.cmit.libs.tsc.key.property.PropertyKeyImpl

object GuiKey {
  val INDEX_OPENER: DefaultKey[Boolean] = new DefaultKey[Boolean](true);
  val MAIN_FRAME: TypeSafeKey[AbstractMainFrame] = new TypeSafeKey[AbstractMainFrame]() {};
  val FONT_SCALE: PropertyKey[Float] = PropertyKeyImpl.create("font.scale", new FloatFormatter)
}