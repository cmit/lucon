package ro.cmit.lucon.gui.viewers

import java.awt.{Cursor, GridLayout}
import java.awt.event.{KeyAdapter, MouseAdapter, MouseMotionAdapter}
import java.util.{Observable, Observer}

import javax.swing.event.{TreeSelectionEvent, TreeSelectionListener}
import javax.swing.tree.DefaultMutableTreeNode
import javax.swing._
import ro.cmit.lucon.configuration.IndexConfigurator
import ro.cmit.lucon.engine.Context
import ro.cmit.lucon.gui.BottomTextArea
import ro.cmit.lucon.gui.exception.LuconException

abstract class ALuconViewer(val configurator: IndexConfigurator)
  extends JInternalFrame("ConcViewer #" + (ALuconViewer.viewerCount + 1), true, // resizable
    true, // closable
    true, // maximizable
    true) with Observer {
  ALuconViewer.viewerCount = ALuconViewer.viewerCount + 1

  protected val bottomLabel = ALuconViewer.createBottomLabel

  protected val bottomArea = new BottomTextArea()

  private val glass = new JPanel(new GridLayout(0, 1))
  // add a label to help trap focus while the glass pane is active
  private val padding = new JLabel()
  
  initialize()

  initializeGlassPane()

  private def initializeGlassPane() = {
    // Set up the glass pane with a little message and a progress bar...
    val controlPane = new JPanel(new GridLayout(2, 1))
    controlPane.setOpaque(false)
    controlPane.add(new JLabel())
    glass.setOpaque(false)
    glass.add(padding)
    glass.add(new JLabel())
    glass.add(controlPane)
    glass.add(new JLabel())
    glass.add(new JLabel())

    // trap both mouse and key events. Could provide a smarter
    // key handler if you wanted to allow things like a keystroke
    // that would cancel the long-running operation.
    glass.addMouseListener(new MouseAdapter() {})
    glass.addMouseMotionListener(new MouseMotionAdapter() {})
    glass.addKeyListener(new KeyAdapter() {})

    // make sure the focus won't leave the glass pane
    // glass.setFocusCycleRoot(true) // 1.4
    padding.setNextFocusableComponent(padding) // 1.3
    setGlassPane(glass)
  }

  def initialize()

  def alert(msg: String) = JOptionPane.showMessageDialog(this, msg)

  def blockMouseAndKeyboard() = {
    setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR))
    glass.setVisible(true)
    padding.requestFocus() // required to trap key events
  }

  def freeMouseAndKeyboard() = {
    setCursor(Cursor.getDefaultCursor)
    glass.setVisible(false)
  }

  def update(observable: Observable, o: Any) = {
    freeMouseAndKeyboard()
    if (o.isInstanceOf[String])
      alert(o.asInstanceOf[String])
    else if (o.isInstanceOf[LuconException])
      alert(o.asInstanceOf[LuconException].getMessage())
  }
  
}

object ALuconViewer {
  var viewerCount = 0
  val xOffset = 30
  val yOffset = 30
  val propFileName = "lucon.properties"
  val ONE_SECOND = 1000

  def createBottomLabel: JTextArea with TreeSelectionListener = {
    val bottomLabel = new JTextArea("") with TreeSelectionListener {
      override def valueChanged(e: TreeSelectionEvent) = {
        setText("")
        val component = e.getPath.getLastPathComponent
        if (component != null) {
          val node = component.asInstanceOf[DefaultMutableTreeNode]
          if (node.isLeaf) {
            node.getUserObject match {
              case context: Context =>
                setText(context.source.filePath + ":" + context.xpath)
            }
          }
        }
      }
    }
    bottomLabel.setLineWrap(true)
    bottomLabel.setWrapStyleWord(true)
    bottomLabel.setEditable(false)

    bottomLabel
  }

}
