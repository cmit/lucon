package ro.cmit.lucon.gui.tasks;

/** Uses a SwingWorker to perform a time-consuming (and utterly fake) task. */

import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStreamWriter
import java.util.List

import scala.collection.JavaConversions.asScalaBuffer

import org.apache.log4j.Logger

import javax.swing.Timer
import ro.cmit.lucon.configuration.IndexConfigurator
import ro.cmit.lucon.engine.Context

class SaveContextsTask(val contexts: List[Context], val location: File, override val configurator: IndexConfigurator) extends ATask(configurator) with ActionListener {

  private var current = 0;

  private var taskLength = 0;

  private val logger = Logger.getLogger(this.getClass);

  @throws[InterruptedException]
  def run(): Object = {

    if (location == null)
      return null;
    if (contexts == null)
      return null;

    val timer = new Timer(1000, this);
    timer.start();

    useIndeterminateProgressbar
    updateProgressbar(0, 100, "Calculating...");
    taskLength = contexts.size();
    if (taskLength == 0)
      return null;
    try {

      useDeterminateProgressbar
      updateProgressbar(0, taskLength, "Saving...");
      val fos = new FileOutputStream(location);
      val osw = new OutputStreamWriter(fos, "UTF-8");

      for (context <- contexts) {
        osw.write(context.smallContext);
        osw.write('\n');
        current = current + 1;
      }

      osw.close();
      fos.close();

      timer.stop();
      updateProgressbar(taskLength, taskLength, "Finished");
    } catch {
      case e: IOException => logger.error("Error occurred.", e);
    }

    return null;
  }

  override def rollback() = {}

  def actionPerformed(e: ActionEvent) {
    updateProgressbar(current, taskLength, "Saving...");
  }

}
