package ro.cmit.lucon.gui;

import javax.swing.SwingUtilities;

/**
 * This is the 3rd version of SwingWorker (also known as
 * SwingWorker 3), an abstract class that you subclass to
 * perform GUI-related work in a dedicated thread.  For
 * instructions on using this class, see:
 * 
 * http://java.sun.com/docs/books/tutorial/uiswing/misc/threads.html
 *
 * Note that the API changed slightly in the 3rd version:
 * You must now invoke start() on the SwingWorker after
 * creating it.
 */
abstract class SwingWorker {
    private var value: Object = null;  // see getValue(), setValue()
    //private Thread thread;
    private var threadVar: ThreadVar = null;


    /**
     * Start a thread that will call the <code>construct</code> method
     * and then exit.
     */
        val doFinished = new Runnable() {
           override def run(): Unit = { finished(); }
        };

        val doConstruct = new Runnable() { 
            override def run(): Unit = {
                try {
                    setValue(construct());
                }
                finally {
                    threadVar.clear();
                }

                SwingUtilities.invokeLater(doFinished);
            }
        };

        val t = new Thread(doConstruct);
        threadVar = new ThreadVar(t);

        /** 
     * Class to maintain reference to current worker thread
     * under separate synchronization control.
     */
    class ThreadVar(t: Thread ) {
        private var thread: Thread = t;
        def get(): Thread = synchronized { thread};
        def clear(): Unit = synchronized { thread = null };
    }

    /** 
     * Get the value produced by the worker thread, or null if it 
     * hasn't been constructed yet.
     */
    protected def getValue(): Object = synchronized { value }

    /** 
     * Set the value produced by worker thread 
     */
    private def setValue(x: Object): Unit = synchronized { 
        value = x
    }

    /** 
     * Compute the value to be returned by the <code>get</code> method. 
     */
    def construct(): Object;

    /**
     * Called on the event dispatching thread (not on the worker thread)
     * after the <code>construct</code> method has returned.
     */
    def finished(): Unit = {}

    /**
     * A new method that interrupts the worker thread.  Call this method
     * to force the worker to stop what it's doing.
     */
    def interrupt(): Unit = {
        val t: Thread = threadVar.get();
        if (t != null) {
            t.interrupt();
        }
        threadVar.clear();
    }

    /**
     * Return the value created by the <code>construct</code> method.  
     * Returns null if either the constructing thread or the current
     * thread was interrupted before a value was produced.
     * 
     * @return the value created by the <code>construct</code> method
     */
    def get(): Object = {
        while (true) {  
            val t: Thread = threadVar.get();
            if (t == null) {
                return getValue();
            }
            try {
                t.join();
            }
            catch {
              case e: InterruptedException =>
                Thread.currentThread().interrupt(); // propagate
                return null;
            }
        }
        null
    }



    /**
     * Start the worker thread.
     */
    def start(): Unit = {
        val t = threadVar.get();
        if (t != null) {
            t.start();
        }
    }
}
