package ro.cmit.lucon.gui.tasks

/** Uses a SwingWorker to perform a time-consuming (and utterly fake) task. */

import java.io.File

import scala.util.Try

import org.apache.commons.io.FileUtils

import ro.cmit.lucon.configuration.IndexConfigurator
import ro.cmit.lucon.configuration.IndexKey

class IndexDeletingTask(override val configurator: IndexConfigurator) extends ATask(configurator) {

  @throws[InterruptedException]
  override def run = {

    if (progressBar != null) {
      progressBar.setIndeterminate(true)
      updateProgressbar(0, 100, "Deleting...")
    }
    val contentDir = configurator.get(IndexKey.INDEX_CONTENT_DIR)
    if (contentDir.exists) {
      deleteDir(contentDir)
    }
    val metainfoDir = configurator.get(IndexKey.INDEX_METAINFO_DIR)
    if (metainfoDir.exists) {
      deleteDir(metainfoDir)
    }
    val indexFile = configurator.get(IndexKey.INDEX_FILE)
    if (indexFile.exists) {
      indexFile.delete()
    }

    if (progressBar != null) {
      progressBar.setIndeterminate(false)
      updateProgressbar(100, 100, "Finished")
    }

    null
  }

  // Deletes all files and subdirectories under dir.
  // Returns true if all deletions were successful.
  // If a deletion fails, the method stops attempting to delete and returns
  // false.
  private def deleteDir(dir: File) = Try(FileUtils.deleteDirectory(dir)).isSuccess

  override def rollback = {}

}
