package ro.cmit.lucon.gui.wizard.step;

import org.pietschy.wizard.PanelWizardStep

import javax.swing.Icon
import ro.cmit.lucon.configuration.IndexConfigurator

abstract class AIndexStep(arg0: String, arg1: String, arg2: Icon, val configurator: IndexConfigurator)
    extends PanelWizardStep(arg0, arg1, arg2) {

  def this(configurator: IndexConfigurator) = this("", "", null, configurator)

  def this(arg0: String, arg1: String, configurator: IndexConfigurator) = this(arg0, arg1, null, configurator)
}
