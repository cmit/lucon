package ro.cmit.lucon

object LuconPackage {
  /* Return Lucon's package, including version information. */
  def get(): Package = LuconPackage.getClass.getPackage()
}