package ro.cmit.lucon.test

import org.scalatest.FlatSpec
import ro.cmit.lucon.gui.components.PickListModel
import org.scalatest.prop.PropertyChecks
import org.scalacheck.Gen

class PickListModelSpec extends FlatSpec with PropertyChecks {

  def listOfIndexesGen(listSize: Int): Gen[List[Int]] = {
    for (
      sz <- Gen.choose(0, listSize);
      list <- Gen.listOfN(sz, Gen.choose(0, listSize - 1))
    ) yield list
  }

  def listOfStringGen(listSize: Int): Gen[List[String]] = {
    for (
      sz <- Gen.choose(0, listSize);
      list <- Gen.listOfN(sz, Gen.alphaStr)
    ) yield list
  }

  def checkPickListAsEmpty(newPickList: PickListModel[Nothing]) = {
    assert(newPickList.leftList.size == 0)
    assert(newPickList.rightList.size == 0)
  }

  "An empty PickList" should "remain empty on moving left to right" in {
    val pickList = new PickListModel(Nil)
    forAll(listOfIndexesGen(2)) {
      list => checkPickListAsEmpty(pickList.moveLeftToRight(list));
    }
  }

  it should "remain empty on moving right to left" in {
    val pickList = new PickListModel(Nil)
    forAll(listOfIndexesGen(2)) {
      list => checkPickListAsEmpty(pickList.moveRightToLeft(list))
    }
  }

  it should "remain empty on moving all to right" in {
    val pickList = new PickListModel(Nil)
    val newPickList = pickList.moveAllToRight
    checkPickListAsEmpty(newPickList);
  }

  it should "remain empty on moving all to left" in {
    val pickList = new PickListModel(Nil)
    val newPickList = pickList.moveAllToLeft
    checkPickListAsEmpty(newPickList);
  }

  "A non-empty left and empty right PickList" should "preserve the same number of elements after left-to-right moving" in {
    forAll(listOfStringGen(5)) {
      list =>
        {
          val pickList = new PickListModel(list)
          forAll(listOfIndexesGen(list.size)) {
            indexes =>
              {
                val newPickList = pickList.moveLeftToRight(indexes)
                assert(pickList.leftList.size + pickList.rightList.size == newPickList.leftList.size + newPickList.rightList.size)
              }
          }
        }
    }
  }

  it should "preserve the same number of elements after move all to right" in {
    forAll(listOfStringGen(5)) {
      list =>
        {
          val pickList = new PickListModel(list)
          val newPickList = pickList.moveAllToRight

          assert(newPickList.leftList.isEmpty)
          assert(pickList.leftList.size == newPickList.rightList.size)
          assert(pickList.leftList == newPickList.rightList)
        }
    }
  }

}