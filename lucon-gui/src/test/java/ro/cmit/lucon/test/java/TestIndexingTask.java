package ro.cmit.lucon.test.java;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;

import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermDocs;
import org.apache.lucene.index.TermEnum;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import ro.cmit.lucon.gui.tasks.IndexDeletingTask;
import ro.cmit.lucon.gui.tasks.IndexingTask;
import ro.cmit.lucon.configuration.IndexConfigurator;
import ro.cmit.lucon.configuration.IndexKey;

@RunWith(JUnit4.class)
public class TestIndexingTask {
    private static final String indexRootDirName = "TestIndexingTask"
            + File.separator + "lucene-index";
    private static final String dirPath = "TestIndexingTask";
    private static final IndexConfigurator configurator = createConfigurator();

    private static IndexConfigurator createConfigurator() {
        final IndexConfigurator configurator = new IndexConfigurator();
        File indexRootDir = new File(indexRootDirName);
        indexRootDir.mkdirs();
        configurator.setIndexRootDir(indexRootDir);

        File dir = new File(dirPath);
        System.out.println("Files from: " + dir.getAbsolutePath());
        if (dir != null && dir.isDirectory()) {
            File[] listFiles = dir.listFiles();
            for (int i = 0; i < listFiles.length; i++) {
                File file = listFiles[i];
                if (file != null && !file.isDirectory()) {
                    String filePath = file.getAbsolutePath();
                    System.out.println("Added file: " + filePath);
                    configurator.addFile(new File(filePath));
                }
            }
        }

        return configurator;
    }

    @BeforeClass
    public static void createIndex() throws Exception {

        new IndexDeletingTask(configurator).run();

        System.out.print("Indexing to "
                + configurator.get(IndexKey.INDEX_FILE()).getAbsolutePath()
                + "... ");
        new IndexingTask(configurator).run();
        System.out.println("done");

    }

    @Test
    public void testNumDocs() throws CorruptIndexException, IOException {
        System.out.print("Opening from "
                + configurator.get(IndexKey.INDEX_CONTENT_DIR())
                        .getAbsolutePath() + "... ");
        IndexReader indexReader = IndexReader.open(configurator
                .get(IndexKey.INDEX_CONTENT_DIR()));
        System.out.println("done");

        assertEquals(2, indexReader.numDocs());
        indexReader.close();
    }

    @Test
    public void testNumTerms() {
        try {
            System.out.print("Opening from "
                    + configurator.get(IndexKey.INDEX_CONTENT_DIR())
                            .getAbsolutePath() + "... ");
            IndexReader indexReader = IndexReader.open(configurator
                    .get(IndexKey.INDEX_CONTENT_DIR()));
            System.out.println("done");
            int nrTerms = getNumTerms(indexReader);
            System.out.println("No of terms found: " + nrTerms);
            assertEquals(374, nrTerms);
            indexReader.close();
        } catch (IOException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
        assertTrue(true);
    }

    private int getNumTerms(IndexReader indexReader) throws IOException {
        int numTerm = 0;
        TermEnum te = indexReader.terms();
        while (te.next()) {
            Term t = te.term();
            TermDocs td = indexReader.termDocs(t);
            while (td.next()) {
                numTerm += td.freq();
            }
        }
        return numTerm;
    }

}
